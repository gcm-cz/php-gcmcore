<?php

require_once __DIR__."/i18n/L10N.php";
require_once __DIR__."/i18n/Promise.php";

function get_user_locale() {
    $cu = \gcm\kisscms\LoginManager::getCurrentUser();
    if (!is_null($cu)) {
        return $cu->settings->get("locale");
    } else {
        return NULL;
    }
}

function t($val, $amount=NULL) {
    return \gcm\i18n\L10N::getInstance(get_user_locale())->getDomain()->getLocalizedMessage($val, $amount);
}

function d($domain, $val, $amount=NULL) {
    return \gcm\i18n\L10N::getInstance(get_user_locale())->getDomain($domain)->getLocalizedMessage($val, $amount);
}

function t_p($val, $amount=NULL) {
    return new \gcm\i18n\Promise(\gcm\i18n\L10N::DEFAULT_DOMAIN, $val, $amount);
}

function d_p($domain, $val, $amount=NULL) {
    return new \gcm\i18n\Promise($domain, $val, $amount);
}