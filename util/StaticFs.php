<?php

namespace gcm\util;

class StaticFs {
    const DEFAULT_NAMESPACE = "__main__";

    protected static $paths = [self::DEFAULT_NAMESPACE => ROOT_PATH."/static"];

    protected static function verify_path(string $path) {
        if (preg_match("/^@([a-zA-Z][a-zA-Z0-9-_]*)\\/(.*)\$/", $path, $matches)) {
            return [$matches[1], $matches[2]];
        } elseif (preg_match("/^\\/(.+)$/", $path, $matches)) {
            return [self::DEFAULT_NAMESPACE, $matches[1]];
        } else {
            throw new \RuntimeException("Static path does not have correct format. Expected [@namespace]/path.");
        }
    }

    public static function get_path(string $path) {
        list($namespace, $path) = self::verify_path($path);

        if (isset(self::$paths[$namespace])) {
            return self::$paths[$namespace]."/".$path;
        } else {
            throw new \RuntimeException("Static namespace '".$namespace."' is not defined.");
        }
    }

    public static function add_ns(string $namespace, string $path) {
        if (!isset(self::$paths[$namespace])) {
            self::$paths[$namespace] = $path;
        } else {
            throw new \RuntimeException("Static namespace '".$namespace."' is already defined.");
        }
    }

    public static function list_ns() {
        return array_keys(self::$paths);
    }

    public static function has_ns(string $namespace) {
        return isset(self::$paths[$namespace]);
    }
}
