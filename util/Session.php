<?php

namespace gcm\util;

class Session {
    const COOKIE_NAME = "sessid";
    const DEFAULT_LIFETIME = 7200;

    public static $instance = NULL;

    protected $id = NULL;
    protected $data = [];
    protected $changed = [];
    protected $lifetime = [];

    /**
     * Force init session. If session cookie is not set, create new session.
     * @param \gcm\db\Transaction $db MySQL transaction. If no transaction is passed, new will be created.
     */
    public static function init(\gcm\db\Transaction $db=NULL) {
        if (is_null(self::$instance)) {
            inject_transaction(function($db){
                self::$instance = new self($db);
            });
        }
    }

    /**
     * Try to init session, but only if cookie is set.
     * @param \gcm\db\Transaction $db MySQL transaction. If no transaction is passed, new will be created.
     */
    public static function tryInit(\gcm\db\Transaction $db=NULL) {
        if (is_null(self::$instance) && isset($_COOKIE[self::COOKIE_NAME])) {
            try {
                self::$instance = new self($db);
                return true;
            } catch (\gcm\db\exceptions\ConnectError $e) {
                self::$instance = NULL;
                return false;
            }
        } elseif (!is_null(self::$instance)) {
            return true;
        }
    }

    public static function id(\gcm\db\Transaction $db=NULL) {
        self::tryInit($db);

        if (!is_null(self::$instance)) {
            return self::$instance->id;
        } else {
            return self::rand_id();
        }
    }

    public static function set($name, $value, $lifetime=NULL, \gcm\db\Transaction $db=NULL) {
        self::init($db);

        self::$instance->data[$name] = $value;
        self::$instance->lifetime[$name] = $lifetime;
        self::$instance->changed[$name] = true;
    }

    public static function get($name, $default=NULL, \gcm\db\Transaction $db=NULL) {
        self::tryInit($db);

        if (is_null(self::$instance)) {
            return $default;
        } else {
            return self::$instance->data[$name] ?? $default;
        }
    }

    public static function getLifetime($name) {
        self::tryInit();

        if (!is_null(self::$instance)) {
            return self::$instance->lifetime[$name] ?? NULL;
        } else {
            return NULL;
        }
    }

    public static function destroy() {
        if (isset($_COOKIE[self::COOKIE_NAME])) {
            $db = transaction();
            $db->query("DELETE FROM `session` WHERE `id` = ?", $_COOKIE[self::COOKIE_NAME]);
            $db->commit();

            setcookie(self::COOKIE_NAME, "");
        }
    }

    public static function rand_id($length = 32, $alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {
        $out = "";
        for ($i = 0; $i < $length; ++$i) {
            $out .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];
        }
        return $out;
    }

    protected function __construct(\gcm\db\Transaction $db=NULL) {
        if (!isset($_COOKIE[self::COOKIE_NAME])) {
            $rand = $this->rand_id();
            $_COOKIE[self::COOKIE_NAME] = $rand;

            $this->id = $rand;
        } else {
            $this->id = $_COOKIE[self::COOKIE_NAME];
        }

        // Remember lifetime. If there is session that has lifetime set, extend the cookie.
        $max_lifetime = 0;

        inject_transaction(function($db) use (&$max_lifetime){
            $q = $db->query("SELECT `name`, `value`, `lifetime` FROM `session` WHERE `id` = ? AND `timestamp` > DATE_ADD(NOW(), INTERVAL -COALESCE(`lifetime`, ?) SECOND)", $this->id, self::DEFAULT_LIFETIME);

            while ($a = $q->fetch_assoc()) {
                $this->data[$a["name"]] = json_decode($a["value"]);
                $this->lifetime[$a["name"]] = $a["lifetime"];
                if (!is_null($a["lifetime"]) && $max_lifetime < $a["lifetime"]) {
                    $max_lifetime = $a["lifetime"];
                }
            }
        });

        $host = $_SERVER["HTTP_HOST"];
        if (isset($_SERVER["HTTP_X_FORWARDED_HOST"])) {
            $host = $_SERVER["HTTP_X_FORWARDED_HOST"];
        }

        setcookie(self::COOKIE_NAME, $this->id, (($max_lifetime > 0)?(time() + $max_lifetime):0), "/", $host, isset($_SERVER["HTTPS"]));

        register_shutdown_function(array($this, "save"));
    }

    public function save() {
        $db = transaction();

        $to_insert = [];
        $to_delete = [];

        $insert_args = [];

        foreach ($this->changed as $key => $dummy) {
            if (is_null($this->data[$key])) {
                $to_delete[] = $key;
            } else {
                $to_insert[] = "(?, ?, ?, NOW(), ?)";
                $insert_args[] = $this->id;
                $insert_args[] = $key;
                $insert_args[] = json_encode($this->data[$key]);
                $insert_args[] = $this->lifetime[$key] ?? NULL;
            }
        }

        if (!empty($to_insert)) {
            $db->query("INSERT INTO `session` (`id`, `name`, `value`, `timestamp`, `lifetime`) VALUES ".implode(",", $to_insert)." ON DUPLICATE KEY UPDATE `value` = VALUES(`value`), `lifetime` = COALESCE(VALUES(`lifetime`), `lifetime`)", ...$insert_args);
        }

        if (!empty($to_delete)) {
            $db->query("DELETE FROM `session` WHERE `id` = ? AND `name` IN (?)", $this->id, $to_delete);
        }

        $db->query("UPDATE `session` SET `timestamp` = NOW() WHERE `id` = ?", $this->id);
        $db->commit();
    }

    public static function cleanup() {
        try {
            inject_transaction(function($db){
                if (!$db->is_commited()) {
                    $db->query("DELETE FROM `session` WHERE `timestamp` < DATE_ADD(NOW(), INTERVAL -COALESCE(`lifetime`, ?) SECOND)", self::DEFAULT_LIFETIME);
                }
            });
        } catch (\gcm\db\exceptions\ConnectError $e) {
        }
    }
}
