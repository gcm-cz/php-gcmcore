<?php

namespace gcm\util;

class Debugger {
    protected $other_error_handler = NULL;
    protected $other_exception_handler = NULL;

    protected static $text_mode = false;

    public function installErrorHandler() {
        $this->other_error_handler = \set_error_handler([$this, "error_handler"], \ini_get("error_reporting"));
    }

    public function installShutdownFunction() {
        \register_shutdown_function([$this, "check_for_fatal"]);
    }

    public function installExceptionHandler() {
        $this->other_exception_handler = \set_exception_handler([$this, "exception_handler"]);
    }

    public function install() {
        $this->installErrorHandler();
        $this->installShutdownFunction();
        $this->installExceptionHandler();
    }

    public static function setTextMode(bool $textMode=true) {
        self::$text_mode = $textMode;
    }

    public function error_handler(int $errno, string $errstr, string $errfile, int $errline) {
        if (!is_null($this->other_error_handler)) {
            call_user_func($this->other_error_handler, $errno, $errstr, $errfile, $errline);
        }

        if (!(error_reporting() & $errno) || (!@ini_get("display_errors") && headers_sent())) {
            // This error code is not included in error_reporting, so let it fall
            // through to the standard PHP error handler
            return false;
        }

        if (!ini_get("display_errors")) {
            return;
        }

        switch ($errno) {
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_USER_ERROR:
                $msg = "Error";
                break;

            case E_WARNING:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
            case E_USER_WARNING:
                $msg = "Warning";
                break;

            case E_PARSE:
                $msg = "Parse Error";
                break;

            case E_NOTICE:
            case E_USER_NOTICE:
                $msg = "Notice";
                break;

            case E_STRICT:
                $msg = "Strict Error";
                break;

            case E_RECOVERABLE_ERROR:
                $msg = "Recoverable Error";
                break;

            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                $msg = "Deprecated";
                break;
        }

        if (php_sapi_name() == "cli" || self::$text_mode) {
            echo "\n".$msg.": ".$errstr." in ".$errfile." on line ".$errline."\n";
            if (function_exists('debug_backtrace')){
                echo "Stack trace (most recent call first):\n";
                $backtrace = debug_backtrace();
                array_shift($backtrace);
                foreach ($backtrace as $index => $l) {
                    echo "  ".($index+1).". in function ".($l['class'] ?? "").($l['type'] ?? "").($l['function']);
                    if (isset($l['file']) && !empty($l['file'])) echo " called from {$l['file']}";
                    if (isset($l['line'])) echo ", line {$l['line']}";
                    echo "\n";
                }
            }
        } else {
            echo "\n<div style=\"border: solid 0.05em #cc0000; margin: 0.5em; border-radius: 0.3em; background: #ffe5e5; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);\">\n";
            echo "\t<div style=\"padding: 0.5em 1em; color: #A00000;\"><strong>".$msg.":</strong> ".$errstr." in <b>".$errfile."</b> on line <b>".$errline."</b></div>";

            if (function_exists('debug_backtrace')){
                echo "\t<div style=\"background: #eaeaea; padding: 0.5em 1em; border-bottom-left-radius: 0.3em; border-bottom-right-radius: 0.3em;\">\n";
                echo "\t\t<strong>Stack trace</strong> (most recent call first):<br />\n";
                $backtrace = debug_backtrace();
                array_shift($backtrace);
                echo "\t\t<ol style=\"padding-left: 1em; margin: 0; margin-top: 0.5em\">\n";
                foreach ($backtrace as $l) {
                    echo "\t\t\t<li>in function <b>".($l['class'] ?? "").($l['type'] ?? "").($l['function'])."</b>";
                    if (isset($l['file']) && !empty($l['file'])) print " called from <b>{$l['file']}</b>";
                    if (isset($l['line'])) print ", line <b>{$l['line']}</b>";
                    print "</li>\n";
                }
                echo "\t\t</ol>\n";
                echo "\t</div>\n";
            }

            echo "</div>\n";
        }
    }

    public function check_for_fatal() {
        $error = error_get_last();
        if ($error["type"] == E_ERROR) {
            $this->error_handler($error["type"], $error["message"], $error["file"] ?? "Unknown", $error["line"] ?? 0);
        }
    }

    public function exception_handler($exception) {
        if (!is_null($this->other_exception_handler)) {
            $this->other_exception_handler($exception);
        }

        // Do not output error if display_errors is false and headers already been sent (so anything has been returned
        // to the browser).
        if (!headers_sent()) {
            require_once __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."controllers".DIRECTORY_SEPARATOR."ErrorController.php";

            if (class_exists("\\gcm\\controllers\\ErrorController")) {
                $ec = new \gcm\controllers\ErrorController();

                if ($exception instanceof \gcm\util\exceptions\HTTPException) {
                    echo $ec->http_error($exception);
                } else {
                    echo $ec->error500($exception);
                }
            } else {
                throw $exception;
            }
        }
    }
}
