<?php

namespace gcm\util;

class Message {
    const SUCCESS = 1;
    const INFO = 2;
    const WARNING = 3;
    const ERROR = 4;

    protected $message;
    protected $severity;

    public function __construct(string $message, int $severity) {
        $this->message = $message;
        $this->severity = $severity;

        self::test_severity($severity);
    }

    public function __toString() {
        return $this->message;
    }

    public function getSeverity() {
        return $this->severity;
    }

    public function isSuccess() {
        return $this->severity == self::SUCCESS;
    }

    public function isInfo() {
        return $this->severity == self::INFO;
    }

    public function isWarning() {
        return $this->severity == self::WARNING;
    }

    public function isError() {
        return $this->severity == self::ERROR;
    }

    protected static function test_severity(int $severity) {
        if (!in_array($severity, [self::SUCCESS, self::INFO, self::WARNING, self::ERROR])) {
            throw new \InvalidArgumentException("Message severity must be one of ".Message::class."::SUCCESS, ".Message::class."::INFO, ".Message::class."::WARNING, ".Message::class."::ERROR.");
        }
    }

    public static function flash(string $message, int $severity) {
        $msgs = Session::get("messages", []);

        self::test_severity($severity);
        $msg = new \stdClass();
        $msg->message = $message;
        $msg->severity = $severity;

        $msgs[] = $msg;
        Session::set("messages", $msgs);
    }

    public static function flashed() {
        $msgs = Session::get("messages", []);
        if (!empty($msgs)) {
            Session::set("messages", []);
        }

        return array_map(function($item){
            return new self($item->message, $item->severity);
        }, $msgs);
    }
}
