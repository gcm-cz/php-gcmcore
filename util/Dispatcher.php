<?php

namespace gcm\util;

/**
 * Very simple event dispatcher.
 */
class Dispatcher {
    const EV_ROUTER_BIND = self::class."::EV_ROUTER_BIND";
    const EV_TWIG_INIT = self::class."::EV_TWIG_INIT";
    const EV_OVERRIDE_REQUEST = self::class."::EV_OVERRIDE_REQUEST";
    const EV_REQUEST_FAILED = self::class."::EV_REQUEST_FAILED";

    const PRIO_LOW = 0;
    const PRIO_DEFAULT = 1;
    const PRIO_HIGH = 2;

    protected static $events = [];

    protected static $level=0;

    public static function bind($event, $callback, $priority=self::PRIO_DEFAULT) {
        if (!isset(self::$events[$event])) {
            self::$events[$event] = [];
        }

        foreach (self::$events[$event] as $index => $cb) {
            if ($cb[0] > $priority) {
                array_splice(self::$events[$event], $index, 0, [$priority, $callback]);
                return;
            }
        }

        self::$events[$event][] = [$priority, $callback];
    }

    public static function dispatch($event, ...$params) {
        $out = [];

        foreach (self::gen($event, ...$params) as $result) {
            $out[] = $result;
        }

        return $out;
    }

    public static function gen($event, ...$params) {
        if (isset(self::$events[$event])) {
            foreach (self::$events[$event] as $callback) {
                yield call_user_func($callback[1], ...$params);
            }
        }
    }

    public static function satisfy($event, ...$params) {
        foreach (self::gen($event, ...$params) as $result) {
            if (!is_null($result)) {
                return $result;
            }
        }

        return NULL;
    }
}
