<?php

namespace gcm\util\cli;

use gcm\util\CLI;

class CommandWrapper extends CommandHandler {
    protected $_help;
    protected $_callable;

    public function __construct(string $name, string $help, callable $callable) {
        parent::__construct($name);
        $this->_help = $help;
        $this->_callable = $callable;
    }

    public function help(array $args, array $kwargs): string {
        return $this->_help;
    }

    public function __invoke(array $args, array $kwargs): int {
        try {
            $exitcode = call_user_func($this->_callable, $args, $kwargs);
            if (!is_int($exitcode)) {
                return CLI::EXIT_SUCCESS;
            } else {
                return $exitcode;
            }
        } catch (\ArgumentCountError $e) {
            echo $this->help($args, $kwargs);
            return CLI::EXIT_FAILURE;
        }
    }
}
