<?php

namespace gcm\util\cli;

class CommandClass extends SubCommandHandler {
    public function __construct(string $prefix) {
        parent::__construct($prefix);

        $refl = new \ReflectionClass($this);
        $parent = $refl->getParentClass();

        foreach ($refl->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            if ($method->isAbstract() || $method->isConstructor() || $method->isDestructor() || $parent->hasMethod($method->getName())) {
                continue;
            }

            $comment = $method->getDocComment();
            $comment = preg_replace('/^\/\*\*[[:blank:]]*([[:blank:]]*\n)?/', "", $comment); // Begining of comment
            $comment = preg_replace('/([[:cntrl:]]+[[:blank:]]*\\*\/\s*$)/', "", $comment); // End of comment
            $comment = preg_replace('/(?<=[[:cntrl:]]|^)[[:blank:]]*\*[[:blank:]]?/', "", $comment); // Middle of comment

            $this->registerCommand(new CommandWrapper($method->getName(), $comment, [$this, $method->getName()]));
        }
    }

    protected function combineArgs(array $pos, array $args=[], array $kwargs=[], array $default=[]): array {
        $out = $kwargs;

        foreach ($pos as $index => $arg) {
            if (isset($kwargs[$arg]) && !isset($args[$index])) {
                $out[$arg] = $kwargs[$arg];
            } elseif (!isset($kwargs[$arg]) && isset($args[$index])) {
                $out[$arg] = $args[$index];
            } elseif (isset($kwargs[$arg]) && isset($args[$index])) {
                // kwarg overwrites arg, as in Python.
                $out[$arg] = $kwargs[$arg];
            } elseif (!isset($kwargs[$arg]) && !isset($args[$index])) {
                $out[$arg] = $default[$arg] ?? NULL;
            }
        }

        return $out;
    }
}
