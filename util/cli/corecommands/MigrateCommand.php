<?php

namespace gcm\util\cli\corecommands;

use gcm\util\CLI;
use gcm\util\cli\CommandHandler;
use gcm\config\Config;

class MigrateCommand extends CommandHandler {
    const MIGRATIONS_SUBDIR = "migrations";

    public function __construct() {
        parent::__construct("migrate");
    }

    public function help(array $args, array $kwargs): string {
        return "Perform database migrations to be up-to-date with code.";
    }

    public function __invoke(array $argv, array $kwargs): int {
        $ml = \gcm\util\Dispatcher::dispatch(\gcm\ml\EV_GET_MODULE_LOADER)[0];

        $files_to_migrate = [];
        $modules = $ml->list();

        foreach ($modules as $module) {
            $path = $module->path.DIRECTORY_SEPARATOR.self::MIGRATIONS_SUBDIR;
            foreach (scandir($path) as $file) {
                if ($file[0] == ".") continue;
                $files_to_migrate[] = [$module, $file];
            }
        }

        // Select all already applied migrations from schema_migration table.
        $already_migrated = [];

        $db = transaction();
        try {
            $q = $db->query("SELECT `filename` FROM `schema_migration`");
            while ($a = $q->fetch_object()) {
                list($modname, $file) = explode("/", $a->filename, 2);
                if (isset($modules[$modname])) {
                    $already_migrated[] = [$modules[$modname], $file];
                }
            }
        } catch (\gcm\db\exceptions\QueryError $e) {
            // Ignore missing table schema_migration, for initial setup.
            if ($e->getCode() != 1146) {
                throw $e;
            }
        }
        $db->commit();

        usort($files_to_migrate, function($a, $b) {
            return strnatcmp($a[1], $b[1]);
        });

        $to_migrate = [];

        foreach ($files_to_migrate as $file) {
            if (!in_array($file, $already_migrated)) {
                $to_migrate[] = $file;
            }
        }

        echo "Pending migrations:\n".implode("\n", array_map(function($file) { return " - ".$file[0]->name."/".$file[1]; }, $to_migrate));
        if (count($to_migrate) == 0) {
            echo "None.";
        }
        echo "\n\n";

        foreach ($to_migrate as $file) {
            list($module, $file) = $file;
            $fullpath = $module->path.DIRECTORY_SEPARATOR.self::MIGRATIONS_SUBDIR.DIRECTORY_SEPARATOR.$file;

            echo "Migrating ".$module->name."/".$file."\n";
            $m = popen(implode(" ", [
                "mysql",
                "-B",
                "-h".escapeshellarg(Config::get("mysql", "host")),
                "-u".escapeshellarg(Config::get("mysql", "user")),
                "-p".escapeshellarg(Config::get("mysql", "password")),
                escapeshellarg(Config::get("mysql", "database")),
                "< ".escapeshellarg($fullpath),
            ]), "r");

            while (!feof($m)) {
                $line = fgets($m);
                echo $line;
            }

            $resp = pclose($m);
            if ($resp == 0) {
                $db = transaction();
                try {
                    $db->query("INSERT INTO `schema_migration` (`filename`, `applied`) VALUES (?, NOW())", $module->name."/".$file);
                    $db->commit();
                } finally {
                    if (!$db->is_commited()) {
                        $db->rollback();
                    }
                }
            } else {
                echo "Migration failed.\n";
                return CLI::EXIT_FAILURE;
            }
        }

        echo "Migration is done.\n";

        return CLI::EXIT_SUCCESS;
    }
}
