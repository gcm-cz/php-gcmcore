<?php

namespace gcm\util\cli\corecommands;

use gcm\util\cli\CommandHandler;

class QuitCommand extends CommandHandler {
    public function __construct() {
        parent::__construct("quit");
    }

    public function help(array $args, array $kwargs): string {
        return "Quit interactive shell.";
    }

    public function __invoke(array $argv, array $kwargs): int {
        echo "\n";
        echo "Bye.\n";
        exit(0);
    }
}
