<?php

namespace gcm\util\cli\corecommands;

use gcm\util\CLI;
use gcm\util\cli\CommandHandler;

class HelpCommand extends CommandHandler {
    public function __construct() {
        parent::__construct("help");
    }

    public function help(array $args, array $kwargs): string {
        return "Show help for given command.\n\n".
            "Usage:\n".
            "> help\n".
            "  Show list of available commands.\n\n".
            "> help <command>\n".
            "  Show help for given command.";
    }

    public function __invoke(array $argv, array $kwargs): int {
        if (empty($argv)) {
            echo "Available commands are:\n";

            $cmds = array_keys($this->cli->getCommands());
            natsort($cmds);
            echo implode("\n", array_map(function($cmd) { return " - ".$cmd; }, $cmds));

            echo "\n\nFor more info, run: help <command>\n";
        } else {
            $name = array_shift($argv);
            if ($this->cli->hasCommand($name)) {
                echo $this->cli->getCommand($name)->help($argv);
            } else {
                echo "Command ".$name." does not exists.";
            }
        }

        return CLI::EXIT_SUCCESS;
    }
}
