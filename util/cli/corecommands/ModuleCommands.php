<?php

namespace gcm\util\cli\corecommands;

use gcm\util\CLI;
use gcm\util\cli\CommandClass;
use gcm\util\Dispatcher;

use gcm\ml\ModuleLoader;

class ModuleCommands extends CommandClass {
    const MKDIR_MODE = 0755;

    public function __construct() {
        parent::__construct("module");
    }

    /**
     * List existing modules and their versions.
     */
    public function list(): int {
        $ml = Dispatcher::dispatch(\gcm\ml\EV_GET_MODULE_LOADER)[0];
        foreach ($ml->list() as $module) {
            echo " - ".$module->name." (".$module->version.")\n";
        }

        return CLI::EXIT_SUCCESS;
    }

    /**
     * Initialize new module.
     *
     * Usage: module init [<name> [<version>]]
     */
    public function init(array $args, array $kwargs): int {
        $kwargs = $this->combineArgs(["name", "version"], $args, $kwargs, ["version" => "1.0.0"]);

        if (empty($kwargs["name"])) {
            $kwargs["name"] = $this->cli->getTerminal()->readline("Enter module name: ");
        }

        if (empty($kwargs["name"])) {
            $this->cli->getTerminal()->writeError("You must enter module name.");
            return CLI::EXIT_FAILURE;
        }

        if (!preg_match('/^[a-zA-Z][a-zA-Z0-9-_]*$/', $kwargs["name"])) {
            $this->cli->getTerminal()->writeError("Module name must be valid identifier ([a-zA-Z][a-zA-Z0-9-_]*).");
            return CLI::EXIT_FAILURE;
        }

        $ml = Dispatcher::dispatch(\gcm\ml\EV_GET_MODULE_LOADER)[0];
        $modules = $ml->list();

        if (isset($modules[$kwargs["name"]])) {
            $this->cli->getTerminal()->writeError("Module '".$kwargs["name"]."' already exists.");
            return CLI::EXIT_FAILURE;
        }

        if (in_array($kwargs["name"], ModuleLoader::PROTECTED_MODULE_NAMES)) {
            $this->cli->getTerminal()->writeERror("Not allowed module name: '".$kwargs["name"]."'.");
            return CLI::EXIT_FAILURE;
        }

        // Create module directory.
        $module_dir = BASE_PATH.DIRECTORY_SEPARATOR."app".DIRECTORY_SEPARATOR.ModuleLoader::MODULES_DIR.DIRECTORY_SEPARATOR.$kwargs["name"];
        mkdir($module_dir, self::MKDIR_MODE, true);

        $meta_path = $module_dir.DIRECTORY_SEPARATOR.ModuleLoader::META_FILE;
        $init_path = $module_dir.DIRECTORY_SEPARATOR.ModuleLoader::INIT_FILE;

        // Write meta file
        $f = fopen($meta_path, "w");
        fwrite($f, <<<EOF
<?php

namespace $name;

return (new \gcm\ml\Module())
    ->setName("${kwargs["name"]}")
    ->setVersion("${kwargs["version"]}");
EOF
);
        fclose($f);

        // Write init file
        $f = fopen($init_path, "w");
        fwrite($f, <<<EOF
<?php

namespace ${kwargs["name"]};

// Place your module init code here. For example, bind to Dispatcher callbacks to
// interact with the system.

EOF
);
        fclose($f);

        $migrations_dir = $module_dir.DIRECTORY_SEPARATOR.MigrateCommand::MIGRATIONS_SUBDIR;
        mkdir($migrations_dir, self::MKDIR_MODE);

        $initial_migration = $migrations_dir.DIRECTORY_SEPARATOR.date("Y-m-d-H-i-s")."_initial.sql";
        $f = fopen($initial_migration, "w");
        fwrite($f, "-- Place your initial database migration here.\n");
        fclose($f);

        return CLI::EXIT_SUCCESS;
    }
}
