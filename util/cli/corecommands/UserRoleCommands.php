<?php

namespace gcm\util\cli\corecommands;

use \gcm\util\CLI;
use \gcm\util\cli\CommandClass;
use \gcm\util\cli\TableHelper;

use \gcm\db\exceptions\EntityNotFound;

use \gcm\kisscms\User;
use \gcm\kisscms\Role;

class UserRoleCommands extends CommandClass {
    public function __construct() {
        parent::__construct("role");
    }

    public function help(array $args, array $kwargs): string {
        if (empty($args)) {
            return "Work with user roles.\n".parent::help($args);
        } else {
            return parent::help($args);
        }
    }

    /**
     * List roles associated with this user account.
     *
     * Usage: user role list <username>
     */
    public function list(array $args, array $kwargs): int {
        $kwargs = $this->combineArgs(["username"], $args, $kwargs);

        if (empty($kwargs["username"])) {
            throw new \InvalidArgumentException("You must specify username argument.");
        }

        try {
            $db = transaction();

            $user = User::getByName($kwargs["username"], $db);

            $roles = $user->listRoles($db);

            $tbl = new TableHelper($this->cli->getTerminal(), ["id" => "ID", "name" => "Role"]);
            $tbl->addRows($roles);

            $this->cli->getTerminal()->writeln("Roles of user ".$user->username.":");
            $tbl->write();

            return CLI::EXIT_SUCCESS;
        } catch (EntityNotFound $e) {
            $this->cli->getTerminal()->writeError("User ".$kwargs["username"]." does not exists.");
            return CLI::EXIT_FAILURE;
        } finally {
            $db->commit();
        }
    }

    /**
     * Add role to user account
     *
     * Usage: user role add <username> <id>
     */
    public function add(array $args, array $kwargs): int {
        $kwargs = $this->combineArgs(["username", "id"], $args, $kwargs);

        if (empty($kwargs["username"])) {
            $this->cli->getTerminal()->writeError("Missing required argument username.");
            return CLI::EXIT_FAILURE;
        }

        if (empty($kwargs["id"])) {
            $this->cli->getTerminal()->writeError("Missing required argument id.");
            return CLI::EXIT_FAILURE;
        }

        try {
            $db = transaction();

            try {
                $user = User::getByName($kwargs["username"], $db);
            } catch (EntityNotFound $e) {
                $this->cli->getTerminal()->writeError("User ".$kwargs["username"]." does not exists.");
                return CLI::EXIT_FAILURE;
            }

            try {
                $role = Role::getById($db, $kwargs["id"]);
            } catch (EntityNotFound $e) {
                $this->cli->getTerminal()->writeError("Role ".$kwargs["id"]." does not exists.");
                return CLI::EXIT_FAILURE;
            }

            $existing_roles = $user->list_role_ids($db);
            $existing_roles[] = $role->id;
            $user->set_roles($db, $existing_roles);
            $db->commit();

            $this->cli->getTerminal()->writeln("User ".$user->username." now has role ".$role->name.".");
            return CLI::EXIT_SUCCESS;
        } finally {
            if (!$db->is_commited()) {
                $db->rollback();
            }
        }
    }

    /**
     * Remove role from user account.
     *
     * Usage: user role remove <username> <id>
     */
    public function remove(array $args, array $kwargs): int {
        $kwargs = $this->combineArgs(["username", "id"], $args, $kwargs);

        if (empty($kwargs["username"])) {
            $this->cli->getTerminal()->writeError("Missing required argument username.");
            return CLI::EXIT_FAILURE;
        }

        if (empty($kwargs["id"])) {
            $this->cli->getTerminal()->writeError("Missing required argument id.");
            return CLI::EXIT_FAILURE;
        }

        try {
            $db = transaction();

            try {
                $user = User::getByName($kwargs["username"], $db);
            } catch (EntityNotFound $e) {
                $this->cli->getTerminal()->writeError("User ".$kwargs["username"]." does not exists.");
                return CLI::EXIT_FAILURE;
            }

            try {
                $role = Role::getById($db, $kwargs["id"]);
            } catch (EntityNotFound $e) {
                $this->cli->getTerminal()->writeError("Role ".$kwargs["id"]." does not exists.");
                return CLI::EXIT_FAILURE;
            }

            $existing_roles = $user->list_role_ids($db);
            foreach ($existing_roles as $index => $role_id) {
                if ($role_id == $role->id) {
                    unset($existing_roles[$index]);
                    break;
                }
            }

            $user->set_roles($db, $existing_roles);
            $db->commit();

            $this->cli->getTerminal()->writeln("User ".$user->username." has been removed from role ".$role->name.".");
            return CLI::EXIT_SUCCESS;
        } finally {
            if (!$db->is_commited()) {
                $db->rollback();
            }
        }
    }
}
