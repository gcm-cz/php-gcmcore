<?php

namespace gcm\util\cli\corecommands;

use \gcm\util\cli\CommandClass;
use \gcm\util\cli\TableHelper;

use \gcm\kisscms\User;

class UserCommands extends CommandClass {
    public function __construct() {
        parent::__construct("user");

        $this->registerCommand(new UserRoleCommands());
    }

    /**
     * List users
     */
    public function list() {
        $db = transaction();
        try {
            $users = User::list($db);

            $tbl = new TableHelper($this->cli->getTerminal(), ["id" => "ID", "username" => "Name", "status" => "Status"]);
            $tbl->addRows(array_map(function($user) {
                $user->status = [
                    User::STATUS_NEW => "New",
                    User::STATUS_ACTIVE => "Active",
                    User::STATUS_SUSPENDED => "Suspended",
                    User::STATUS_MUST_CHANGE_PASSWORD => "Password change required",
                ][$user->status_id];

                return $user;
            }, $users));
            $tbl->write();
        } finally {
            $db->commit();
        }
    }

    /**
     * Add new user. The user is created as verified.
     *
     * Usage: user add [username] [password]
     */
    public function add(array $args, array $kwargs) {
        $kwargs = $this->combineArgs(["username", "password"], $args, $kwargs);

        if (empty($kwargs["username"])) {
            $kwargs["username"] = $this->cli->getTerminal()->readline("User name: ");
        }

        if (empty($kwargs["password"])) {
            $kwargs["password"] = $this->cli->getTerminal()->readline("Password: ", false);
        }

        $db = transaction();
        try {
            $u = User::register($db, $kwargs["username"], $kwargs["password"], "", User::STATUS_ACTIVE);
            $db->commit();

            $this->cli->getTerminal()->write("User has been successfully created. It's ID is ".$u->id.".\n");
        } finally {
            if (!$db->is_commited()) {
                $db->rollback();
            }
        }
    }
}
