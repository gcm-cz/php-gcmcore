<?php

namespace gcm\util\cli\corecommands;

use \gcm\util\cli\CommandClass;
use \gcm\util\cli\TableHelper;
use \gcm\util\CLI;

use \gcm\kisscms\Role;
use \gcm\kisscms\Action;

use \gcm\db\exceptions\EntityNotFound;

class RoleCommands extends CommandClass {
    public function __construct() {
        parent::__construct("role");
    }

    /**
     * List defined roles.
     *
     * Usage: role list [limit=<limit>] [offset=<offset>]
     */
    public function list(array $args, array $kwargs): int {
        $limit = $kwargs["limit"] ?? NULL;
        $offset = $kwargs["offset"] ?? NULL;

        $db = transaction();
        try {
            $roles = Role::list($db, $limit, $offset);

            $tbl = new TableHelper($this->cli->getTerminal(), ["id" => "ID", "name" => "Role"]);
            $tbl->addRows($roles);

            $this->cli->getTerminal()->writeln("System roles:");
            $tbl->write();

            return CLI::EXIT_SUCCESS;
        } finally {
            $db->commit();
        }
    }

    /**
     * Add new role
     *
     * Usage: role add <name>
     */
    public function add(array $args, array $kwargs): int {
        $kwargs = $this->combineArgs(["name"], $args, $kwargs);

        if (empty($kwargs["name"])) {
            $kwargs["name"] = $this->cli->getTerminal()->readline("Role name: ");
        }

        if (empty($kwargs["name"])) {
            $this->cli->getTerminal()->writeError("Name cannot be empty.");
            return CLI::EXIT_FAILURE;
        }

        $db = transaction();
        try {
            $role = Role::create($db, $kwargs["name"]);
            $db->commit();

            $this->cli->getTerminal()->writeln("Role ".$kwargs["name"]." has been created. ID: ".$role->id."\n");

            return CLI::EXIT_SUCCESS;
        } catch (\gcm\db\exceptions\DuplicateEntry $e) {
            $this->cli->getTerminal()->writeError("Role ".$kwargs["name"]." already exists.");
            return CLI::EXIT_FAILURE;
        } finally {
            if (!$db->is_commited()) {
                $db->rollback();
            }
        }
    }

    /**
     * Remove role
     *
     * Usage: role remove <id>
     */
    public function remove(array $args, array $kwargs) {
        $kwargs = $this->combineArgs(["id"], $args, $kwargs);

        if (empty($kwargs["id"])) {
            $this->cli->getTerminal()->writeError("Missing required argument id.");
            return CLI::EXIT_FAILURE;
        }

        $db = transaction();
        try {
            $role = Role::getById($db, $kwargs["id"]);
            $role->remove($db);
            $db->commit();
            $this->cli->getTerminal()->writeln("Role ".$role->name." has been removed.");
            return CLI::EXIT_SUCCESS;
        } catch (EntityNotFound $e) {
            $this->cli->getTerminal()->writeError("Role ".$kwargs["id"]." was not found.");
            return CLI::EXIT_FAILURE;
        } finally {
            if (!$db->is_commited()) {
                $db->rollback();
            }
        }
    }

    /**
     * Modify role
     *
     * Usage: role modify <id> [name=<name>]
     */
    public function modify(array $args, array $kwargs) {
        $kwargs = $this->combineArgs(["id"], $args, $kwargs);

        if (empty($kwargs["id"])) {
            $this->cli->getTerminal()->writeError("Missing required argument `id`.");
            return CLI::EXIT_FAILURE;
        }

        $db = transaction();
        try {
            $role = Role::getById($db, $kwargs["id"]);

            $opts = ["name"];
            $to_update = [];

            foreach ($opts as $opt) {
                if (isset($kwargs[$opt]) && !empty($kwargs[$opt])) {
                    $to_update[$opt] = $kwargs[$opt];
                }
            }

            if (!empty($to_update)) {
                $role->update($db, $to_update);
                $db->commit();
                $this->cli->getTerminal()->writeln("Role ".$role->name." has been modified.");
            } else {
                $this->cli->getTerminal()->writeError("Nothing to update.");
                return CLI::EXIT_FAILURE;
            }

            return CLI::EXIT_SUCCESS;
        } catch (EntityNotFound $e) {
            $this->cli->getTerminal()->writeError("Role ".$kwargs["id"]. " was not found.");
            return CLI::EXIT_FAILURE;
        } finally {
            if (!$db->is_commited()) {
                $db->rollback();
            }
        }
    }

    /**
     * List actions of given role.
     *
     * Usage: role actions <id>
     */
    public function actions(array $args, array $kwargs) {
        $kwargs = $this->combineArgs(["id"], $args, $kwargs);

        if (empty($kwargs["id"])) {
            $this->cli->getTerminal()->writeError("Missing required argument `id`.");
            return CLI::EXIT_FAILURE;
        }

        $db = transaction();
        try {
            $role = Role::getById($db, $kwargs["id"]);
            $role_action_ids = $role->list_action_ids($db);
            $role_actions = [];

            $actions = Action::list($db);
            foreach ($actions as $action) {
                if (in_array($action->id, $role_action_ids)) {
                    $role_actions[] = $action;
                }
            }

            $table = new TableHelper($this->cli->getTerminal(), ["name" => "Action", "description" => "Description"]);
            $table->addRows($role_actions);
            $this->cli->getTerminal()->writeln("Actions of role ".$role->name.":");
            $table->write();
            return CLI::EXIT_SUCCESS;
        } catch (EntityNotFound $e) {
            $this->cli->getTerminal()->writeError("Role ".$kwargs["id"]. " was not found.");
            return CLI::EXIT_FAILURE;
        } finally {
            $db->commit();
        }
    }
}
