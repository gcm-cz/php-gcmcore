<?php

namespace gcm\util\cli;

use gcm\util\CLI;

class Terminal {
    protected $cli;

    protected $fdRead;
    protected $fdWrite;
    protected $buffer;
    protected $cursorPos;

    protected $history = [];
    protected $historyPos = 0;
    protected $historyCurrentBuffer = NULL;

    protected $autocompleteCallback = NULL;

    protected $featureFlags = self::FEAT_ECHO;

    const ESCAPE = "\e[";
    const CURSOR_LEFT = "D";
    const CURSOR_RIGHT = "C";
    const CURSOR_UP = "A";
    const CURSOR_DOWN = "B";
    const HOME = "H";
    const END = "F";
    const DELETE = "~";

    const FEAT_AUTOCOMPLETE = 1 << 0;
    const FEAT_HISTORY = 1 << 1;
    const FEAT_EOF_IS_QUIT = 1 << 2;
    const FEAT_ECHO = 1 << 3;
    const FEAT_HELP_SHORTCUT = 1 << 4;

    public function __construct(CLI $cli, $fd=NULL) {
        $this->cli = $cli;

        if (is_null($fd)) {
            $this->fdRead = fopen("php://stdin", "r");
            $this->fdWrite = fopen("php://stdout", "r");
        } else {
            $this->fdRead = $fd;
            $this->fdWrite = $fd;
        }
    }

    public function setAutocompleteCallback(callable $callback=NULL) {
        $this->autocompleteCallback = $callback;
    }

    public function write(string $str) {
        fwrite($this->fdWrite, $str, strlen($str));
    }

    public function writeln(string $str) {
        if (mb_substr($str, -1) != "\n") {
            $str .= "\n";
        }

        $this->write($str);
    }

    public function writePrompt() {
        $this->write(self::ESCAPE."0m".strtr($this->cli->getPrompt(), [
            "{:prefix}" => "/".str_replace(" ", "/", $this->cli->getPrefix()),
            "{:name}" => \gcm\config\Config::get("site", "title"),
        ]));
    }

    protected function ufgetc() {
        // mask values for first byte's bit patterns
        static $mask = [
            192, // 110xxxxx
            224, // 1110xxxx
            240  // 11110xxx
        ];

        // read first byte
        $ch = fgetc($this->fdRead);
        if ($ch === false) {
            // return false on EOF
            return false;
        }

        // single-byte character
        if ((ord($ch) & $mask[0]) != $mask[0]) {
            return $ch;
        }

        // multi-byte character
        $buf = $ch;
        for ($i = 0; $i < count($mask); $i++) {
            if ((ord($ch) & $mask[$i]) != $mask[$i]) {
                break;
            }
            $buf .= fgetc($this->fdRead);
        }
        return $buf;
    }

    protected function readEscape(): array {
        $ch = fgetc($this->fdRead);
        if ($ch != "[") {
            $this->write(self::ESCAPE."[2mESC".$ch.self::ESCAPE."[0m");
            return [NULL, NULL, NULL];
        }

        $ch = fgetc($this->fdRead);

        $params = "";

        while (is_numeric($ch) || $ch == ";") {
            $params .= $ch;
            $ch = fgetc($this->fdRead);
        }

        if (strpos($params, ";") !== false) {
            list($p1, $p2) = explode(";", $params, 2);
        } elseif (!empty($params)) {
            $p1 = $params;
            $p2 = NULL;
        } else {
            $p1 = NULL;
            $p2 = NULL;
        }

        return [$ch, $p1, $p2];
    }

    protected function resetBuffer(string $content="") {
        if (!empty($this->buffer)) {
            $this->moveCursorLeft(mb_strlen($this->buffer));
        }

        $this->buffer = $content;
        $this->cursorPos = mb_strlen($content);

        if ($this->featureFlags & self::FEAT_ECHO) {
            $this->write(self::ESCAPE."K");
            if (!empty($this->buffer)) {
                $this->write($this->buffer);
            }
        }
    }

    protected function refreshToEOL() {
        if ($this->featureFlags & self::FEAT_ECHO) {
            $this->write(
                self::ESCAPE."K". // Clear to end of line
                mb_substr($this->buffer, $this->cursorPos) // Write rest of the buffer
            );

            $goback = mb_strlen($this->buffer) - $this->cursorPos;
            if ($goback > 0) {
                $this->write(self::ESCAPE.$goback.self::CURSOR_LEFT); // Move cursor back to it's place
            }
        }
    }

    protected function backspace() {
        if ($this->cursorPos <= 0) {
            return;
        }

        $this->buffer = mb_substr($this->buffer, 0, $this->cursorPos - 1).mb_substr($this->buffer, $this->cursorPos);
        --$this->cursorPos;

        if ($this->featureFlags & self::FEAT_ECHO) {
            $this->write(self::ESCAPE."D");
            $this->refreshToEOL();
        }
    }

    protected function delete() {
        if ($this->cursorPos >= mb_strlen($this->buffer) - 1) {
            return;
        }

        $this->buffer = mb_substr($this->buffer, 0, $this->cursorPos).mb_substr($this->buffer, $this->cursorPos + 1);
        $this->refreshToEOL();
    }

    protected function moveCursorLeft(int $p1=1) {
        $newPos = max(0, $this->cursorPos - $p1);
        $diff = $this->cursorPos - $newPos;
        $this->cursorPos = $newPos;

        if ($this->featureFlags & self::FEAT_ECHO) {
            if ($diff > 1) {
                $this->write(self::ESCAPE.$diff.self::CURSOR_LEFT);
            } elseif ($diff == 1) {
                $this->write(self::ESCAPE.self::CURSOR_LEFT);
            }
        }
    }

    protected function moveCursorRight(int $p1=1) {
        $newPos = min(mb_strlen($this->buffer), $this->cursorPos + $p1);
        $diff = $newPos - $this->cursorPos;
        $this->cursorPos = $newPos;

        if ($this->featureFlags & self::FEAT_ECHO) {
            if ($diff > 1) {
                $this->write(self::ESCAPE.$diff.self::CURSOR_RIGHT);
            } elseif ($diff == 1) {
                $this->write(self::ESCAPE.self::CURSOR_RIGHT);
            }
        }
    }

    protected function historyUp() {
        if (empty($this->history)) {
            return;
        }

        if (is_null($this->historyCurrentBuffer)) {
            $this->historyCurrentBuffer = $this->buffer;
        } else {
            $this->historyPos = max(0, $this->historyPos - 1);
        }

        $this->resetBuffer($this->history[$this->historyPos]);
    }

    protected function historyDown() {
        if (empty($this->history)) {
            return;
        }

        if ($this->historyPos < count($this->history) - 1) {
            $this->historyPos = min(count($this->history) - 1, $this->historyPos + 1);
            $this->resetBuffer($this->history[$this->historyPos]);
        } else {
            if (!is_null($this->historyCurrentBuffer)) {
                $this->resetBuffer($this->historyCurrentBuffer);
                $this->historyCurrentBuffer = NULL;
            }
        }
    }

    protected function processEscape(string $esc, int $p1=null, int $p2=null) {
        switch ($esc) {
            case self::CURSOR_LEFT:
                $this->moveCursorLeft();
                break;

            case self::CURSOR_RIGHT:
                $this->moveCursorRight();
                break;

            case self::HOME:
                $this->moveCursorLeft(mb_strlen($this->buffer));
                break;

            case self::END:
                $this->moveCursorRight(mb_strlen($this->buffer));
                break;

            case self::DELETE:
                $this->delete();
                break;

            case self::CURSOR_UP:
                if ($this->featureFlags & self::FEAT_HISTORY) {
                    $this->historyUp();
                }
                break;

            case self::CURSOR_DOWN:
                if ($this->featureFlags & self::FEAT_HISTORY) {
                    $this->historyDown();
                }
                break;

            default:
                $this->write(self::ESCAPE."2m"."ESC[".$esc.self::ESCAPE."0m");
        }
    }

    protected function autocomplete() {
        if (!is_null($this->autocompleteCallback)) {
            $choices = call_user_func($this->autocompleteCallback, mb_substr($this->buffer, 0, $this->cursorPos));
            $choices = array_filter($choices, function($choice) { return !empty($choice); });
            if (count($choices) == 1) {
                $this->resetBuffer($choices[0]." ".mb_substr($this->buffer, $this->cursorPos));
            } else if (count($choices) > 1) {
                $this->moveCursorRight(mb_strlen($this->buffer));
                $this->write("\nSuggestions: ".implode(" ", $choices)."\n");
                $this->writePrompt();
                $this->write($this->buffer);
                $this->resetBuffer($this->buffer);
            }
        }
    }

    protected function processChar(string $ch): bool {
        $out = false;

        switch ($ch) {
            // RETURN
            case "\n":
                $out = true;
                $this->write("\n");
                break;

            // Backspace
            case "\x7F":
                $this->backspace();
                break;

            // Escape sequence
            case "\x1B":
                list($esc, $p1, $p2) = $this->readEscape();
                if (!is_null($esc)) {
                    $this->processEscape($esc, $p1, $p2);
                }
                break;

            case "\t":
                if ($this->featureFlags & self::FEAT_AUTOCOMPLETE) {
                    $this->autocomplete();
                }
                break;

            // Ctrl+D, EOF
            case "\x04":
                $out = true;
                if ($this->featureFlags & self::FEAT_EOF_IS_QUIT) {
                    $this->resetBuffer("/quit");
                    $this->write("\n");
                } else {
                    $this->resetBuffer("");
                }
                break;

            default:
                if (ord($ch) >= 0x20) {
                    $this->buffer = mb_substr($this->buffer, 0, $this->cursorPos).$ch.mb_substr($this->buffer, $this->cursorPos);

                    if ($this->featureFlags & self::FEAT_ECHO) {
                        $this->write($ch);
                    }

                    ++$this->cursorPos;

                    $this->refreshToEOL();
                } else {
                    $this->write(self::ESCAPE."2m0x".dechex(ord($ch)).self::ESCAPE."0m");
                }
                break;
        }

        if (($this->featureFlags & self::FEAT_HELP_SHORTCUT) && $this->buffer == "?") {
            // TODO: Allow shortcut also for subcommands.
            $this->write("\n");
            $this->buffer = "help";
            $out = true;
        }

        //$this->cli->debug("buffer: '".$this->buffer."', len: ".mb_strlen($this->buffer).", pos: ".$this->cursorPos);

        return $out;
    }

    public function readCommand(): ?string {
        // this weird construct enables reading by chars from stdin.
        readline_callback_handler_install("", function(){});
        try {
            $this->resetBuffer();
            $this->writePrompt();

            $this->featureFlags =
                self::FEAT_AUTOCOMPLETE |
                self::FEAT_HISTORY |
                self::FEAT_EOF_IS_QUIT |
                self::FEAT_ECHO |
                self::FEAT_HELP_SHORTCUT;

            while (!feof($this->fdRead)) {
                $ch = $this->ufgetc();

                if ($this->processChar($ch)) {
                    break;
                }
            }

            if (empty($this->buffer) && feof($this->fdRead)) {
                return false;
            }

            if (!empty($this->buffer)) {
                $this->history[] = $this->buffer;
                $this->historyPos = count($this->history) - 1;
                $this->historyCurrentBuffer = NULL;
            }

            return $this->buffer;
        } finally {
            readline_callback_handler_remove();
        }
    }

    public function readline(string $prompt="", bool $echo=true) {
        // this weird construct enables reading by chars from stdin.
        readline_callback_handler_install("", function(){});
        try {
            $this->resetBuffer();
            $this->write($prompt);

            $this->featureFlags = (($echo)?self::FEAT_ECHO:0);

            while (!feof($this->fdRead)) {
                $ch = $this->ufgetc();
                if ($this->processChar($ch)) {
                    break;
                }
            }

            return $this->buffer;
        } finally {
            readline_callback_handler_remove();
        }
    }

    public function writeError(string $error) {
        $this->writeln(self::ESCAPE."31m".$error.self::ESCAPE."0m");
    }
}
