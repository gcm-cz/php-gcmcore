<?php

namespace gcm\util\cli;

use gcm\util\CLI;

abstract class CommandHandler {
    public function __construct(string $command) {
        $this->cli = NULL;
        $this->command = $command;
    }

    public function setCLI(CLI $cli) {
        $this->cli = $cli;
    }

    public function getCommand(): string {
        return $this->command;
    }

    public function autocomplete(array $args, array $kwargs): array {
        return [];
    }

    public abstract function help(array $args, array $kwargs): string;
    public abstract function __invoke(array $args, array $kwargs): int;
}
