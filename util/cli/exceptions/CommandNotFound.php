<?php

namespace gcm\util\cli\exceptions;

class CommandNotFound extends CLIException {
    public function __construct(string $command) {
        parent::__construct("Command ".$command." does not exists.");
    }
}
