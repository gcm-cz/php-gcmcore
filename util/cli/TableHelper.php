<?php

namespace gcm\util\cli;

class TableHelper {
    public function __construct(Terminal $terminal, array $columns) {
        $this->terminal = $terminal;
        $this->columns = $columns;
        $this->columnWidth = array_map(function($column) { return mb_strlen($column); }, $this->columns);

        $this->rows = [];
    }

    public function addRow($row) {
        foreach (array_keys($this->columns) as $column) {
            if (is_array($row)) {
                $this->columnWidth[$column] = max($this->columnWidth[$column], mb_strlen($row[$column]));
            } elseif (is_object($row)) {
                $this->columnWidth[$column] = max($this->columnWidth[$column], mb_strlen($row->{$column}));
            } else {
                throw new \InvalidArgumentException("Argument \$row must be array or object. It is ".gettype($user).".");
            }
        }

        $this->rows[] = $row;
    }

    public function addRows(array $rows) {
        foreach ($rows as $row) {
            $this->addRow($row);
        }
    }

    public function write() {
        $this->writeHeader();
        foreach ($this->rows as $row) {
            $this->writeRow($row);
        }
        $this->writeFooter();
    }

    protected function writeHeader() {
        $first = true;
        foreach ($this->columns as $column => $label) {
            if ($first) {
                $first = false;
                $this->terminal->write("\u{250C}"); // ┌
            } else {
                $this->terminal->write("\u{252C}"); // ┬
            }
            $this->terminal->write(str_repeat("\u{2500}", $this->columnWidth[$column] + 2)); // ─
        }
        $this->terminal->write("\u{2510}\n"); // ┐

        foreach ($this->columns as $column => $label) {
            // │ Label │
            $this->terminal->write("\u{2502} ".$label.str_repeat(" ", $this->columnWidth[$column] - mb_strlen($label) + 1));
        }

        $this->terminal->write("\u{2502}\n");

        $first = true;
        foreach ($this->columns as $column => $label) {
            if ($first) {
                $first = false;
                $this->terminal->write("\u{251C}"); // ├
            } else {
                $this->terminal->write("\u{253C}"); // ┼
            }
            $this->terminal->write(str_repeat("\u{2500}", $this->columnWidth[$column] + 2)); // ─
        }
        $this->terminal->write("\u{2524}\n"); // ┤
    }

    protected function writeRow($row) {
        foreach ($this->columns as $column => $label) {
            $value = "";
            if (is_array($row)) {
                $value = $row[$column];
            } elseif (is_object($row)) {
                $value = $row->{$column};
            }

            $this->terminal->write("\u{2502} ".$value.str_repeat(" ", $this->columnWidth[$column] - mb_strlen($value) + 1));
        }
        $this->terminal->write("\u{2502}\n");
    }

    protected function writeFooter() {
        $first = true;
        foreach ($this->columns as $column => $label) {
            if ($first) {
                $first = false;
                $this->terminal->write("\u{2514}"); // └
            } else {
                $this->terminal->write("\u{2534}"); // ┴
            }
            $this->terminal->write(str_repeat("\u{2500}", $this->columnWidth[$column] + 2)); // ─
        }
        $this->terminal->write("\u{2518}\n"); // ┘
    }
}
