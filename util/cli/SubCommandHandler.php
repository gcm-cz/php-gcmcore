<?php

namespace gcm\util\cli;

use gcm\util\CLI;
use gcm\util\cli\exceptions\CommandNotFound;

class SubCommandHandler extends CommandHandler {
    protected $commands;

    public function help(array $args, array $kwargs): string {
        if (empty($args)) {
            $cmds = array_keys($this->commands);
            natsort($cmds);

            return "\nAvailable sub commands:\n".implode("\n", array_map(function($cmd) { return " - ".$cmd; }, $cmds))."\n";
        } else {
            $subcmd = array_shift($args);
            if (isset($this->commands[$subcmd])) {
                $cmd = $this->commands[$subcmd];
                return $cmd->help($args);
            } else {
                throw new CommandNotFound($this->cli->getPrefix()." ".$subcmd);
            }
        }
    }

    public function setCLI(CLI $cli) {
        parent::setCLI($cli);

        foreach ($this->commands as $handler) {
            $handler->setCLI($cli);
        }
    }

    public function registerCommand(CommandHandler $cmd) {
        $name = $cmd->getCommand();
        $cmd->cli = $this->cli;

        if (!isset($this->commands[$name])) {
            $this->commands[$name] = $cmd;
        } else {
            trigger_error("Command ".$this->getCommand()." ".$name." already exists.");
        }
    }

    public function autocomplete(array $args, array $kwargs): array {
        $choices = [];

        //$this->cli->debug("autocomplete: ".$this->getCommand().": ".implode(", ", array_map(function($x) { return ">".$x."<"; }, $args)));

        if (empty($args)) {
            $choices = array_keys($this->commands);
        } elseif (count($args) == 1) {
            $name = $args[0];
            foreach (array_keys($this->commands) as $command) {
                if (substr($command, 0, strlen($name)) == $name) {
                    $choices[] = $command;
                }
            }
        } elseif (count($args) > 1) {
            $name = array_shift($args);
            if (isset($this->commands[$name])) {
                $choices = $this->commands[$name]->autocomplete($args);
            }
        }

        return array_map(function($choice) { return $this->getCommand()." ".$choice; }, $choices);
    }

    public function __invoke(array $args, array $kwargs): int {
        if (!empty($args)) {
            $subcmd = array_shift($args);
            if ($subcmd == "..") {
                $this->cli->popPrefix();
            } elseif ($subcmd == "/") {
                $this->cli->clearPrefix();
            } elseif ($subcmd == "help") {
                echo $this->help($args, $kwargs)."\n";
            } elseif (isset($this->commands[$subcmd])) {
                $cmd = $this->commands[$subcmd];
                $cmd->cli = $this->cli;
                return $cmd($args, $kwargs);
            } else {
                throw new CommandNotFound($this->cli->getPrefix()." ".$subcmd);
            }
        } else {
            $this->cli->pushPrefix($this->getCommand());
        }

        return CLI::EXIT_SUCCESS;
    }
}
