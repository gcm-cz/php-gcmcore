<?php

namespace gcm\util;

class Response {
    public $response;
    public $hints = [];

    public function __construct($response) {
        $this->response = $response;
    }

    public function hint(string $name, $value=NULL) {
        if (!is_null($value)) {
            $this->hints[$name] = $value;
        }

        if (is_null($value)) {
            return $this->hints[$name] ?? NULL;
        } else {
            return $this;
        }
    }

    public function __toString() {
        return $this->response;
    }
}
