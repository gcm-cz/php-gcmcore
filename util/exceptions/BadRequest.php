<?php

namespace gcm\util\exceptions;

class BadRequest extends HTTPException {
    public function __construct(string $message=NULL) {
        parent::__construct(self::HTTP_BAD_REQUEST, $message);
    }
}
