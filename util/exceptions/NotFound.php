<?php

namespace gcm\util\exceptions;

class NotFound extends HTTPException {
    public function __construct(string $message=NULL, \Throwable $parent=NULL) {
        parent::__construct(self::HTTP_NOT_FOUND, $message, $parent);
    }
}
