<?php

namespace gcm\util\exceptions;

class Unauthorized extends HTTPException {
    public function __construct(string $message="You are not authorized to access this resource.", \Throwable $parent=NULL) {
        parent::__construct(self::HTTP_UNAUTHORIZED, $message, $parent);
    }
}
