<?php

namespace gcm\util\exceptions;

class Redirect extends HTTPException {
    public $url;

    public function __construct($url, $code=self::HTTP_FOUND) {
        $this->url = $url;
        parent::__construct($code, "Redirecting to <a href=\"".htmlspecialchars($url)."\">".htmlspecialchars($url)."</a>");
    }
}
