<?php

namespace gcm\util\exceptions;

class HTTPException extends \RuntimeException {
    const HTTP_MULTIPLE_CHOICES = 300;
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_FOUND = 302;
    const HTTP_SEE_OTHER = 303;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_USE_PROXY = 305;
    const HTTP_TEMPORARY_REDIRECT = 307;
    const HTTP_PERMANENT_REDIRECT = 308;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
    const HTTP_REQUEST_TIMEOUT = 408;
    const HTTP_CONFLICT = 409;
    const HTTP_GONE = 410;
    const HTTP_LENGTH_REQUIRED = 411;
    const HTTP_PRECONDITION_FAILED = 412;
    const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;
    const HTTP_REQUEST_URI_TOO_LONG = 414;
    const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;
    const HTTP_REQUEST_RANGE_NOT_SATISFIABLE = 416;
    const HTTP_EXPECTATION_FAILED = 417;
    const HTTP_IM_A_TEAPOT = 418;
    const HTTP_UPGRADE_REQUIRED = 426;
    const HTTP_PRECONDITION_REQUIRED = 428;
    const HTTP_TOO_MANY_REQUESTS = 429;
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;
    const HTTP_UNAVAILABLE_FOR_LEGAL_REASONS = 451;
    const HTTP_INTERNAL_SERVER_ERROR = 500;
    const HTTP_NOT_IMPLEMENTED = 501;
    const HTTP_BAD_GATEWAY = 502;
    const HTTP_SERVICE_UNAVAILABLE = 503;
    const HTTP_GATEWAY_TIMEOUT = 504;
    const HTTP_VERSION_NOT_SUPPORTED = 505;
    const HTTP_VARIANT_ALSO_NEGOTIATES = 506;
    const HTTP_NOT_EXTENDED = 510;
    const HTTP_NETWORK_AUTHENTICATION_REQUIRED = 511;
    const HTTP_NETWORK_READ_TIMEOUT_ERROR = 598;
    const HTTP_NETWORK_CONNECTION_TIMEOUT_ERROR = 599;

    protected static $codes = [
        self::HTTP_MULTIPLE_CHOICES => "Multiple Choices",
        self::HTTP_MOVED_PERMANENTLY => "Moved Permanently",
        self::HTTP_FOUND => "Found",
        self::HTTP_SEE_OTHER => "See Other",
        self::HTTP_NOT_MODIFIED => "Not Modified",
        self::HTTP_USE_PROXY => "Use Proxy",
        self::HTTP_TEMPORARY_REDIRECT => "Temporary Redirect",
        self::HTTP_PERMANENT_REDIRECT => "Permanent Redirect",
        self::HTTP_BAD_REQUEST => "Bad Request",
        self::HTTP_UNAUTHORIZED => "Unauthorized",
        self::HTTP_PAYMENT_REQUIRED => "Payment Required",
        self::HTTP_FORBIDDEN => "Forbidden",
        self::HTTP_NOT_FOUND => "Not Found",
        self::HTTP_METHOD_NOT_ALLOWED => "Method Not Allowed",
        self::HTTP_NOT_ACCEPTABLE => "Not Acceptable",
        self::HTTP_PROXY_AUTHENTICATION_REQUIRED => "Proxy Authentication Required",
        self::HTTP_REQUEST_TIMEOUT => "Request Timeout",
        self::HTTP_CONFLICT => "Conflict",
        self::HTTP_GONE => "Gone",
        self::HTTP_LENGTH_REQUIRED => "Length Required",
        self::HTTP_PRECONDITION_FAILED => "Precondition Failed",
        self::HTTP_REQUEST_ENTITY_TOO_LARGE => "Request Entity Too Large",
        self::HTTP_REQUEST_URI_TOO_LONG => "Request-URI Too Long",
        self::HTTP_UNSUPPORTED_MEDIA_TYPE => "Unsupported Media Type",
        self::HTTP_REQUEST_RANGE_NOT_SATISFIABLE => "Requested Range Not Satisfiable",
        self::HTTP_EXPECTATION_FAILED => "Expectation Failed",
        self::HTTP_IM_A_TEAPOT => "I'm a teapot",
        self::HTTP_UPGRADE_REQUIRED => "Upgrade Required",
        self::HTTP_PRECONDITION_REQUIRED => "Precondition Required",
        self::HTTP_TOO_MANY_REQUESTS => "Too Many Requests",
        self::HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE => "Request Header Fields Too Large",
        self::HTTP_UNAVAILABLE_FOR_LEGAL_REASONS => "Unavailable For Legal Reasons",
        self::HTTP_INTERNAL_SERVER_ERROR => "Internal Server Error",
        self::HTTP_NOT_IMPLEMENTED => "Not Implemented",
        self::HTTP_BAD_GATEWAY => "Bad Gateway",
        self::HTTP_SERVICE_UNAVAILABLE => "Service Unavailable",
        self::HTTP_GATEWAY_TIMEOUT => "Gateway Timeout",
        self::HTTP_VERSION_NOT_SUPPORTED => "HTTP Version Not Supported",
        self::HTTP_VARIANT_ALSO_NEGOTIATES => "Variant Also Negotiates",
        self::HTTP_NOT_EXTENDED => "Not Extended",
        self::HTTP_NETWORK_AUTHENTICATION_REQUIRED => "Network Authentication Required",
        self::HTTP_NETWORK_READ_TIMEOUT_ERROR => "Network read timeout error",
        self::HTTP_NETWORK_CONNECTION_TIMEOUT_ERROR => "Network connect timeout error",
    ];

    public function __construct(int $code, string $message=NULL, \Throwable $parent=NULL) {
        parent::__construct((!is_null($message))?$message:self::$codes[$code], $code, $parent);
    }

    public function getCodeDescription() {
        return self::$codes[$this->getCode()] ?? "";
    }
}
