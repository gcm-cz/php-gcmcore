<?php

namespace gcm\util\exceptions;

class Forbidden extends HTTPException {
    public function __construct(string $message="You don't have permission to access this page.", \Throwable $parent=NULL) {
        parent::__construct(self::HTTP_FORBIDDEN, $message, $parent);
    }
}
