<?php

namespace gcm\util;

use \Twig\TwigFilter;
use \Twig\TwigFunction;
use \Twig\TwigTest;
use \gcm\config\Config;

class TwigEnv {
    public $twig;

    public function __construct() {
        $loader = new \Twig\Loader\FilesystemLoader([__DIR__."/../templ/"]);

        $this->twig = new \Twig\Environment($loader, ["debug" => true]);
        $this->twig->addGlobal("self", get_class($this));

        self::initEnv($this->twig);
    }

    public static function initEnv(\Twig\Environment $env) {
        Dispatcher::dispatch(Dispatcher::EV_TWIG_INIT, $env);

        $env->addExtension(new \Twig\Extension\StringLoaderExtension());
        $env->addExtension(new \Twig\Extra\Intl\IntlExtension());

        $env->addFilter(new TwigFilter("sorted", __CLASS__."::twig_sorted", ["is_safe" => ["html"]]));

        $env->addGlobal("server", $_SERVER);
        $env->addGlobal("request", $_REQUEST);
        $env->addGlobal("get", $_GET);
        $env->addGlobal("post", $_POST);
        $env->addGlobal("tm", $GLOBALS["tm"]);
        $env->addGlobal("base_url", BASE_URL);

        $env->addFilter(new TwigFilter("datetime", __CLASS__."::twig_filter_datetime"));
        $env->addFilter(new TwigFilter("date", __CLASS__."::twig_filter_date"));
        $env->addFilter(new TwigFilter("time", __CLASS__."::twig_filter_time"));
        $env->addFilter(new TwigFilter("timestamp", __CLASS__."::twig_filter_timestamp"));
        $env->addFilter(new TwigFilter("duration", __CLASS__."::twig_format_duration"));
        $env->addFilter(new TwigFilter("dtinterval", __CLASS__."::twig_format_dtinterval"));
        $env->addFilter(new TwigFilter("filesize", __CLASS__."::twig_filter_filesize"));
        $env->addFilter(new TwigFilter("get_class", "get_class"));
        $env->addFilter(new TwigFilter("var_dump", function($var, $pretty=true) {
            if ($pretty) {
                return "<pre>".json_encode($var, JSON_PRETTY_PRINT)."</pre>";
            } else {
                return json_encode($var);
            }
        }, ["is_safe" => ["html"]]));

        $env->addFilter(new TwigFilter("t", function($context, $str, $num=NULL){
            if (isset($context["i18n_domain"])) {
                return d($context["i18n_domain"], $str, $num);
            } else {
                return t($str, $num);
            }
        }, ["needs_context" => true]));

        $env->addFilter(new TwigFilter("d", function($str, $domain, $num=NULL) {
            return d($domain, $str, $num);
        }));

        $env->addFunction(new TwigFunction("query_stats", function(){ return \gcm\db\MySQL\Connection::$statistics; }));
        $env->addFunction(new TwigFunction("microtime", __CLASS__."::twig_microtime"));
        $env->addFunction(new TwigFunction("url_for", "\\url_for"));
        $env->addFunction(new TwigFunction("has_url_for", "\\has_url_for"));
        $env->addFunction(new TwigFunction("has_action","\\gcm\\kisscms\\has_action"));

        $env->addTest(new TwigTest("bool", function($value) { return is_bool($value); }));
        $env->addTest(new TwigTest("int", function($value) { return is_int($value); }));
        $env->addTest(new TwigTest("double", function($value) { return is_double($value); }));
        $env->addTest(new TwigTest("string", function($value) { return is_string($value); }));
        $env->addTest(new TwigTest("object", function($value) { return is_object($value); }));
        $env->addTest(new TwigTest("array", function($value) { return is_array($value); }));
    }

    public static function twig_dtinterval($input) {
        return new \DateInterval($input);
    }

    public static function twig_format_duration($seconds, $type = "long") {
        $force_days = false;
        $force_hours = false;
        $force_minutes = false;
        $force_seconds = false;

        switch ($type) {
            case "long":
                $dict = [
                    "d" => ["%d days", "%d day"],
                    "h" => ["%d hours", "%d hour"],
                    "m" => ["%d minutes", "%d minute"],
                    "s" => ["%d seconds", "%d second"]
                ];
                $join = ", ";
                $na = "N/A";
                break;

            case "short":
                $dict = [
                    "d" => ["%dd", "%dd"],
                    "h" => ["%dh", "%dh"],
                    "m" => ["%dm", "%dm"],
                    "s" => ["%ds", "%ds"]
                ];
                $join = "";
                $na = "";
                break;

            case "time":
                $dict = [
                    "h" => ["%d", "%d"],
                    "m" => ["%02d", "%02d"],
                    "s" => ["%02d", "%02d"]
                ];
                $join = ":";
                $na = "";
                $force_hours = true;
                $force_minutes = true;
                $force_seconds = true;
                break;

            default:
                throw new \UnexpectedValueException("Duration type can be either 'long' or 'short' or 'time'.");
        }

        if ($seconds <= 0 || is_null($seconds)) {
            return $na;
        }

        $out = [];

        if ($seconds >= 86400 && isset($dict["d"])) {
            $days = floor($seconds / 86400);
            if ($days != 1) {
                $out[] = sprintf($dict["d"][0], $days);
            } else {
                $out[] = sprintf($dict["d"][1], $days);
            }
            $seconds -= $days * 86400;
        }

        if ($seconds >= 3600 || $force_hours) {
            $hours = floor($seconds / 3600);
            if ($hours != 1) {
                $out[] = sprintf($dict["h"][0], $hours);
            } else {
                $out[] = sprintf($dict["h"][1], $hours);
            }
            $seconds -= $hours * 3600;
        }

        if ($seconds >= 60 || $force_minutes) {
            $minutes = floor($seconds / 60);
            if ($minutes != 1) {
                $out[] = sprintf($dict["m"][0], $minutes);
            } else {
                $out[] = sprintf($dict["m"][1], $minutes);
            }
            $seconds -= $minutes * 60;
        }

        if ($seconds > 0 || $force_seconds) {
            if ($seconds != 1) {
                $out[] = sprintf($dict["s"][0], $seconds);
            } else {
                $out[] = sprintf($dict["s"][1], $seconds);
            }
        }

        return implode($join, $out);
    }

    public static function twig_filter_date($date, $format=NULL) {
        if (is_null($format)) {
            $format = Config::get("site", "date_format", "Y-m-d");
        }

        if (!is_null($date)) {
            if (!is_object($date)) {
                $date = new \DateTime($date);
            }

            return $date->format($format);
        } else {
            return "N/A";
        }
    }

    public static function twig_filter_datetime($datetime, $format=NULL) {
        if (is_null($format)) {
            $format = Config::get("site", "datetime_format", "Y-m-d G:i:s");
        }

        if (!is_null($datetime)) {
            if (!is_object($datetime)) {
                $datetime = new \DateTime($datetime);
            }

            return $datetime->format($format);
        } else {
            return "N/A";
        }
    }

    public static function twig_filter_time($time, $format=NULL) {
        if (is_null($format)) {
            $format = Config::get("site", "time_format", "G:i:s");
        }

        if (!is_null($time)) {
            if (!is_object($time)) {
                $time = new \DateTime($time);
            }

            return $time->format($format);
        } else {
            return "N/A";
        }
    }

    public static function twig_filter_timestamp($datetime) {
        if (is_null($datetime)) {
            return time();
        }

        if (!is_object($datetime)) {
            $datetime = new \DateTime($datetime);
        }

        return $datetime->getTimestamp();
    }

    public static function twig_sorted($val, $key, $default=false, $namespace="") {
        $url = $_SERVER["REQUEST_URI"];
        if ($_SERVER["QUERY_STRING"]) {
            $url = substr($url, 0, -strlen($_SERVER["QUERY_STRING"]) - 1);
        }

        if (!empty($namespace)) {
            $namespace = $namespace.".";
        }

        $direction = "ASC";
        if (($_GET[$namespace."order"] ?? (($default)?$key:"")) == $key && ($_GET[$namespace."direction"] ?? "ASC") == "ASC") {
            $direction = "DESC";
        }

        $url .= "?".http_build_query(array_merge($_GET, [
            $namespace."order" => $key,
            $namespace."direction" => $direction
        ]));

        if (($_GET[$namespace."order"] ?? (($default)?$key:"")) == $key) {
            $direction = (($_GET[$namespace."direction"] ?? "ASC") == "ASC");
            return "<a href=\"".$url."\" class=\"sorted sort\">".$val." <span class=\"fa ".(($direction)?"fa-sort-asc":"fa-sort-desc")."\"></span></a>";
        } else {
            return "<a href=\"".$url."\" class=\"sorted\">".$val." <span class=\"fa fa-sort hidden-print\"></span></a>";
        }
    }

    public static function twig_microtime() {
        return microtime(true);
    }

    public static function twig_filter_filesize($size, int $precision=2) {
        $units = ["B", "kB", "MB", "GB", "TB", "PB"];
        $i = 0;

        while ($size >= 1024 && $i < count($units)) {
            $size /= 1024.;
            ++$i;
        }

        if ($i > 0) {
            return sprintf("%1.".$precision."f %s", $size, $units[$i]);
        } else {
            return sprintf("%d %s", $size, $units[$i]);
        }
    }
}
