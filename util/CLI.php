<?php

namespace gcm\util;

use gcm\util\cli\CommandHandler;
use gcm\util\cli\exceptions\CLIException;
use gcm\util\cli\exceptions\CommandNotFound;
use gcm\util\cli\Terminal;

class CLI {
    const EV_REGISTER_COMMAND = __CLASS__."::EV_REGISTER_COMMAND";

    const EXIT_SUCCESS = 0;
    const EXIT_FAILURE = 1;

    protected $prompt;
    protected $commands = [];
    protected $prefix = [];

    protected $history = [];
    protected $historyIndex = 0;

    protected $term = NULL;

    public function __construct(string $prompt="\e[1;30m[\e[1;32m{:name}\e[1;30m] \e[1;34m{:prefix}\e[1;37m> \e[0m") {
        $this->prompt = $prompt;

        Dispatcher::dispatch(CLI::EV_REGISTER_COMMAND, $this);
    }

    public function registerCommand(CommandHandler $handler) {
        $name = $handler->getCommand();

        if (!isset($this->commands[$name])) {
            $this->commands[$name] = $handler;
            $handler->setCLI($this);
        } else {
            trigger_error("Command ".$name." already exists.", E_USER_ERROR);
        }
    }

    public function getPrompt(): string {
        return $this->prompt;
    }

    public function pushPrefix(string $prefix) {
        $this->prefix[] = $prefix;
    }

    public function popPrefix(): string {
        return array_pop($this->prefix);
    }

    public function getPrefix(): string {
        return implode(" ", $this->prefix);
    }

    public function clearPrefix() {
        $this->prefix = [];
    }

    public function getCommands(): array {
        return $this->commands;
    }

    public function hasCommand(string $command): bool {
        return isset($this->commands[$command]);
    }

    public function getCommand(string $name): CommandHandler {
        if ($this->hasCommand($name)) {
            return $this->commands[$name];
        } else {
            throw new CommandNotFound($name);
        }
    }

    public function getTerminal(): Terminal {
        return $this->term;
    }

    protected function parseCommand(string $buffer): array {
        if (!preg_match_all('/(?<=^|\s)(([\'"])(.+?)(?<!\\\\)\2|(?<![\"\'])([^=]+?)|(([a-zA-Z][a-zA-Z0-9_-]*)=([\'"])(.*?)(?<!\\\\)\7)|([a-zA-Z][a-zA-Z0-9_-]*)=([^\s]*))(?=$|\s)/',
            $buffer, $ms, PREG_SET_ORDER)) {
            throw new \InvalidArgumentException("Command syntax error.");
        }

        $args = [];
        $kwargs = [];

        // Command does not start with command but (probably) kwarg, that is syntax error.
        if (!empty($buffer) && empty($ms[0][3]) && empty($ms[0][4])) {
            throw new \InvalidArgumentException("Command syntax error.");
        }

        foreach ($ms as $match) {
            if (!empty($match[3])) {
                $args[] = $match[3];
            } elseif (!empty($match[4])) {
                $args[] = $match[4];
            } elseif (!empty($match[6])) {
                $kwargs[$match[6]] = $match[8];
            } elseif (!empty($match[9])) {
                $kwargs[$match[9]] = $match[10];
            }
        }

        // Apply prefix
        if (!empty($args) && substr($args[0], 0, 1) == "/") {
            // No prefix, toplevel command.
            $args[0] = substr($args[0], 1);
            $this->clearPrefix();
        } elseif (!empty($this->prefix)) {
            array_unshift($args, ...$this->prefix);
        }

        while (empty($args[0]) && !empty($args)) {
            array_shift($args);
        }

        return [$args, $kwargs];
    }

    public function autocomplete(string $buffer): array {
        // We need to store prefix, because parseCommand clears it if it encounters toplevel command.
        $prefixSave = $this->prefix;
        list($args, $kwargs) = $this->parseCommand($buffer);
        $this->prefix = $prefixSave;

        //$this->debug("autocomplete cmd: ".implode(", ", array_map(function($x) { return ">".$x."<"; }, $cmd)));
        //$this->debug("prefix: ".implode(" ", $this->prefix).", cmd: ".implode(" ", $cmd));

        $choices = [];

        if (empty($args)) {
            $choices = array_keys($this->commands);
        } elseif (count($args) == 1) {
            // Try to search for command suggestions.
            foreach (array_keys($this->commands) as $command) {
                if (substr($command, 0, strlen($args[0])) == $args[0]) {
                    $choices[] = $command;
                }
            }
        } elseif (count($args) > 1) {
            // Autocomplete command arguments must handle command itself.
            $name = array_shift($args);
            if (isset($this->commands[$name])) {
                $choices = $this->commands[$name]->autocomplete($args, $kwargs);
            }
        }

        $fullprefix = implode(" ", $this->prefix);
        return array_map(function($choice) use ($fullprefix) { return trim(substr($choice, strlen($fullprefix))); }, $choices);
    }

    public function run() {
        set_time_limit(0);

        $argv = $_SERVER["argv"];
        array_shift($argv);

        $this->term = new Terminal($this);
        $this->term->setAutocompleteCallback([$this, "autocomplete"]);

        if (!empty($argv)) {
            try {
                $args = [];
                $kwargs = [];

                foreach ($argv as $arg) {
                    $arg = explode("=", $arg, 2);
                    if (count($arg) == 2) {
                        $kwargs[$arg[0]] = $arg[1];
                    } else {
                        $args[] = $arg[0];
                    }
                }

                exit($this->execute($args, $kwargs));
            } catch (\Throwable $e) {
                $this->term->writeError($e->getMessage());
            }
        } else {
            while (($cmd = $this->term->readCommand()) !== false) {
                if (empty($cmd)) {
                    continue;
                }

                // Parse command line to arguments.
                list($args, $kwargs) = $this->parseCommand($cmd);

                if (empty($args) && empty($kwargs)) {
                    continue;
                }

                try {
                    $this->execute($args, $kwargs);
                } catch (\Throwable $e) {
                    $this->term->writeError($e->getMessage());
                }
            }
        }
    }

    public function execute(array $args, array $kwargs=[]): int {
        set_time_limit(0);

        $exitcode = self::EXIT_SUCCESS;

        $lastOutput = NULL;
        ob_start(function($output, $phase) use (&$lastOutput) {
            if (!empty($output)) {
                $this->term->write($output);
                $lastOutput = $output;
            }
        }, 1);

        $cmd = array_shift($args);
        if (isset($this->commands[$cmd])) {
            $handler = $this->commands[$cmd];
            $exitcode = $handler($args, $kwargs);
        } else {
            throw new CommandNotFound($cmd);
        }

        ob_end_clean();

        if (!is_null($lastOutput) && substr($lastOutput, -1) != "\n") {
            $this->term->write("\n");
        }

        return $exitcode;
    }

    public function debug(string $msg) {
        echo "\e7\e[H\e[K\e[2m".$msg."\e8";
    }
}
