<?php

namespace gcm\fastrouter;

abstract class Decorator implements DecoratorInterface {
    protected $callable;

    public function __construct($callable) {
        $this->callable = $callable;
    }

    public function __invoke(...$args) {
        $callable = $this->callable;
        if (is_array($callable) && !empty($callable) && !\is_object($callable[0])) {
            $callable[0] = new $callable[0]();
        }

        return \call_user_func_array($callable, $args);
    }

    public function getCallable() {
        return $this->callable;
    }
}
