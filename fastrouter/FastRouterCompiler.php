<?php

namespace gcm\fastrouter;

class FastRouterCompiler {
    const ROUTER_INSTANCE_ONLY_INTERFACE = 'gcm\\fastrouter\\InstanceOnlyController';
    const ROUTER_INTERFACE = 'gcm\\fastrouter\\Controller';

    private function __construct() {}

    /**
     * Compile pattern into rule.
     */
    public static function compileRule($pattern, $callable, $customName = NULL) {
        $pattern = self::normalizeUrl($pattern);
        $compiledPattern = self::compilePattern($pattern);

        if ($callable instanceof DecoratorInterface) {
            do {
                $originalCallable = $callable->getCallable();
            } while ($originalCallable instanceof DecoratorInterface);
            list($instance, $controller, $action) = self::verifyCallable($originalCallable);

            return new FastRouterRule($compiledPattern, $pattern, $instance, $controller, $action, $callable, $customName);
        } else {
            list($instance, $controller, $action) = self::verifyCallable($callable);
            if (is_null($instance) && is_null($controller) && is_callable($action)) {
                $callable = $action;
            } else {
                $callable = NULL;
            }

            return new FastRouterRule($compiledPattern, $pattern, $instance, $controller, $action, $callable, $customName);
        }
    }

    public static function normalizeUrl($pattern) {
        if (substr($pattern, 0, 1) != "/") {
            $pattern = "/".$pattern;
        }

        if (substr($pattern, -1) != "/") {
            $pattern = $pattern."/";
        }

        // If path ends with something that looks like file with extension, do not include trailing slash.
        if (preg_match('/\\.[^\\/]+\\/$/', $pattern)) {
            $pattern = substr($pattern, 0, -1);
        }

        $pattern = preg_replace('|/+|', '/', $pattern);
        return $pattern;
    }

    private static function compilePattern($pattern) {
        $escaped = preg_quote($pattern, '#');
        $ruleRe = "#^".preg_replace('#\\\<([a-zA-Z_][a-zA-Z0-9_-]*)\\\>#', '(?P<\\1>.*?)', $escaped)."#";

        return $ruleRe;
    }

    /**
     * Test whether method in given class exists, and if it does, that it can be called from outside environment.
     */
    public static function methodIsAccessible($class, $method) {
        if (!method_exists($class, $method)) {
            return false;
        }

        $ref = new \ReflectionMethod($class, $method);
        return $ref->isPublic() && !$ref->isStatic() && !$ref->isAbstract() && !$ref->isConstructor() && !$ref->isDestructor();
    }

    /**
     * Verify callable.
     * @param mixed $callable Callable
     * @return array $instance, $controller, $action exported from callable.
     */
    public static function verifyCallable($callable) {
        $instance = NULL;
        $controller = NULL;
        $action = NULL;

        if (is_array($callable) && count($callable) == 2) {
            if (is_object($callable[0])) {
                if (is_subclass_of($callable[0], self::ROUTER_INSTANCE_ONLY_INTERFACE)) {
                    $instance = $callable[0];
                } else {
                    throw new RoutingException("Instance of '".get_class($callable[0])."' must implement the '".self::ROUTER_INSTANCE_ONLY_INTERFACE."' interface.");
                }
            } elseif (class_exists($callable[0])) {
                if (is_subclass_of($callable[0], self::ROUTER_INTERFACE)) {
                    $controller = $callable[0];
                } else {
                    throw new RoutingException("Class '".$callable[0]."' must implement the '".self::ROUTER_INTERFACE."' interface.");
                }
            } else {
                throw new RoutingException("Class '".$callable[0]."' does not exists.");
            }

            if (self::methodIsAccessible($callable[0], $callable[1])) {
                $action = $callable[1];
            } else {
                throw new RoutingException("Method '".$callable[1]."' does not exists in class '".((is_object($callable[0]))?get_class($callable[0]):$callable[0])."'");
            }

        // If callable is class, bind that class to the controller.
        } elseif (is_callable($callable)) {
            $action = $callable;

        // All other nonempty callables
        } elseif (!is_null($callable)) {
            throw new RoutingException("Bad callable format. It's not instance, it's not a valid class and it's not class.method pair.");
        }

        // If callable is empty, it means that the rule itself must define controller and action.
        return array($instance, $controller, $action);
    }
}