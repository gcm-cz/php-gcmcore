<?php

namespace gcm\fastrouter;

/**
 * Controller can be bound to patterns based on class name, but it must
 * define empty constructor.
 */
interface Controller extends InstanceOnlyController {
    public function __construct();
}
