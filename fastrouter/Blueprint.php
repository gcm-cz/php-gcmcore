<?php

namespace gcm\fastrouter;

use \gcm\util\Dispatcher;

class Blueprint implements Router {
    protected $rules = [];

    public function bind($pattern, $callable = NULL, $customName = NULL) {
        $rule = FastRouterCompiler::compileRule($pattern, $callable, $customName);
        $rule->blueprint = $this;
        $this->rules[] = $rule;

        return $rule;
    }

    /**
     * Bind blueprint to URL prefix.
     */
    public function bindRouter(string $prefix, Router $router) {
        $prefix = rtrim($prefix, "/");

        foreach ($router->getRulesList() as $rule) {
            $rule->blueprint_prefix = rtrim($prefix.$rule->blueprint_prefix, "/");
            $this->rules[] = $rule;
        }
    }

    public function getRulesList() {
        return $this->rules;
    }

    public function execute(FastRoute $route) {
        $called = false;
        $output = NULL;

        if ($route->getBlueprint() && $route->getBlueprint() != $this) {
            $output = $route->getBlueprint()->execute($route);
        } else {
            $callable = $route->getMethodCallable();
            if (is_array($callable) && is_string($callable[0])) {
                $callable[0] = new $callable[0]();
            }

            $cb = function() use ($route, &$called, &$output, $callable) {
                $output = \call_user_func_array($callable, $route->getArgs());
                $called = true;
                return $output;
            };

            Dispatcher::dispatch(Router::EV_BEFORE_EXECUTE, $callable, $route->getArgs());
            foreach (Dispatcher::gen(Router::EV_WRAP_EXECUTE, $cb, $callable, $route->getArgs()) as $output) {
                $output = $output;
            }

            if (!$called && is_null($output)) {
                $output = $cb();
                $called = true;
            }
        }

        return $output;
    }
}
