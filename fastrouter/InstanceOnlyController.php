<?php

namespace gcm\fastrouter;

/**
 * Instance only controller can be bound to patterns only by it's instance.
 */
interface InstanceOnlyController {

}
