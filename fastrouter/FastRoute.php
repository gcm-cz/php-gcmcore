<?php

namespace gcm\fastrouter;

class FastRoute {
    private $method;
    private $kwargs;
    private $args;
    private $weight;
    private $blueprint;
    public $throw_exc;

    public function __construct($method, $args, $weight=0, Router $blueprint=NULL) {
        $this->method = $method;
        $this->args = $args;
        $this->weight = $weight;
        $this->blueprint = $blueprint;
    }

    public function __toString() {
        return "FastRoute for ".implode("::", $this->method)."(".implode(",", $this->args).") with weight ".$this->weight;
    }

    public function getMethodCallable() {
        return $this->method;
    }

    public function getMethodReflection(): \ReflectionFunctionAbstract {
        if (is_array($this->method)) {
            return new \ReflectionMethod($this->method[0], $this->method[1]);
        } else {
            return new \ReflectionFunction($this->method);
        }
    }

    public function &getArgs() {
        return $this->args;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function getBlueprint() {
        return $this->blueprint;
    }
}
