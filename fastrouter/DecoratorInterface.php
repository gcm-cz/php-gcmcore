<?php

namespace gcm\fastrouter;

interface DecoratorInterface {
    public function __invoke(...$args);
    public function getCallable();
}
