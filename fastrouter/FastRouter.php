<?php

namespace gcm\fastrouter;

/**
 * Fast URL router that does not do any magic, and expects more strict URL scheme.
 */
class FastRouter extends Blueprint {
    private $urlCache;

    public function resolve($url) {
        $url = FastRouterCompiler::normalizeUrl($url);

        $longest_match = 0;
        $longest_resp = NULL;

        foreach ($this->rules as $rule) {
            $resp = $rule->test($url);
            if ($resp instanceof FastRoute) {
                // Same rule without exception from Constraint wins (the OR part).
                if ($longest_match < $resp->getWeight() || ($longest_match <= $resp->getWeight() && is_null($resp->throw_exc))) {
                    $longest_resp = $resp;
                    $longest_match = $resp->getWeight();
                }
            }
        }

        if (!is_null($longest_resp)) {
            if ($longest_resp->throw_exc) {
                throw $longest_resp->throw_exc;
            } else {
                return $longest_resp;
            }
        } else {
            return false;
        }
    }

    public function urlFor($callback, array $params = array()) {
        $instance = NULL;
        $controller = NULL;
        $action = NULL;

        // Decode controller and action from callback.
        if (is_array($callback) && count($callback) == 2) {
            if (is_object($callback[0])) {
                $instance = $callback[0];
            } elseif (is_string($callback[0])) {
                $controller = $callback[0];
            } else {
                throw new RoutingException("Invalid callback specified for urlFor.");
            }

            if (is_string($callback[1])) {
                $action = $callback[1];
            } else {
                throw new RoutingException("Invalid callback specified for urlFor.");
            }
        } elseif (is_callable($callback)) {
            $action = $callback;
        } else {
            throw new RoutingException("Invalid callback specified for urlFor.");
        }

        $obj = (!is_null($instance))?spl_object_hash($instance):$controller;

        $sortedKeys = array_keys($params);
        sort($sortedKeys);

        $cacheKey = sha1($obj.":".$action.":".implode(":", $sortedKeys));

        $args = array();
        $kwargs = array();

        foreach ($params as $key => $val) {
            if (is_numeric($key)) {
                $args[] = rawurlencode($val);
            } else {
                $kwargs[$key] = rawurlencode($val);
            }
        }

        if (isset($this->urlCache[$cacheKey])) {
            return $this->urlCache[$cacheKey]->urlFor($instance, $controller, $action, $args, $kwargs);
        }

        $maxError = NULL;
        $maxErrorRule = NULL;

        foreach ($this->rules as $rule) {
            $status = $rule->hasUrlFor($instance, $controller, $action, $args, $kwargs);
            //error_log("Rule ".$rule." ended with ".$status);

            if ($status === true) {
                $this->urlCache[$cacheKey] = $rule;
                return $rule->urlFor($instance, $controller, $action, $args, $kwargs);
            } else {
                if (is_null($maxError) || $status >= $maxError) {
                    $maxError = $status;
                    $maxErrorRule = $rule;
                }
            }
        }

        throw new RoutingException("Don't have any binding for callback ".implode(".", $callback).". Can't generate url. Best match was ".$maxErrorRule." with error ".$this->errnoToString($maxError).": ".((!is_null($maxErrorRule))?$maxErrorRule->last_error:"No rule has been defined."));
    }

    protected function errnoToString($error) {
        switch ($error) {
            case FastRouterRule::URL_INSTANCE_UNKNOWN:
                return "URL_INSTANCE_UNKNOWN";

            case FastRouterRule::URL_CONTROLLER_NOT_BOUND:
                return "URL_CONTROLLER_NOT_BOUND";

            case FastRouterRule::URL_OTHER_CONTROLLER:
                return "URL_OTHER_CONTROLLER";

            case FastRouterRule::URL_OTHER_ACTION:
                return "URL_OTHER_ACTION";

            case FastRouterRule::URL_METHOD_NOT_CALLABLE:
                return "URL_METHOD_NOT_CALLABLE";

            case FastRouterRule::URL_MISSING_REQUIRED_PARAM:
                return "URL_MISSING_REQUIRED_PARAM";

            case FastRouterRule::URL_MISSING_PARAMS_FOR_METHOD:
                return "URL_MISSING_PARAMS_FOR_METHOD";

            case FastRouterRule::URL_UNKNOWN_KWARG:
                return "URL_UNKNOWN_KWARG";

            case FastRouterRule::URL_TOO_MANY_PARAMS:
                return "URL_TOO_MANY_PARAMS";

        }
    }
}
