<?php

namespace gcm\fastrouter;

interface Router {
    const EV_BEFORE_EXECUTE = Router::class."::EV_BEFORE_EXECUTE";
    const EV_WRAP_EXECUTE = Router::class."::EV_WRAP_EXECUTE";

    public function bind($pattern, $callable = NULL, $customName = NULL);
    public function bindRouter(string $prefix, Router $router);
    public function getRulesList();
    public function execute(FastRoute $route);
}
