<?php

namespace gcm\fastrouter;

interface Constraint {
    public function test(): bool;
}
