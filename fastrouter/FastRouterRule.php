<?php

namespace gcm\fastrouter;

/**
 * Rule for matching.
 */
class FastRouterRule {
    private $pattern;
    private $originalPattern;

    private $controller;
    private $action;
    private $instance;
    private $callable;

    private $requiredArgs = [];

    public $blueprint_prefix = "";
    public $blueprint = NULL;

    public $constraints = [];

    public $last_error;
    public $throw_exc = NULL;

    const URL_INSTANCE_UNKNOWN = 1;
    const URL_CONTROLLER_NOT_BOUND = 2;
    const URL_OTHER_CONTROLLER = 3;
    const URL_OTHER_ACTION = 4;
    const URL_METHOD_NOT_CALLABLE = 5;
    const URL_MISSING_REQUIRED_PARAM = 6;
    const URL_MISSING_PARAMS_FOR_METHOD = 7;
    const URL_UNKNOWN_KWARG = 8;
    const URL_TOO_MANY_PARAMS = 9;

    const ROUTE_BAD_PREFIX = 1;
    const ROUTE_BAD_PATTERN = 2;
    const ROUTE_BAD_ARGUMENT = 3;
    const ROUTE_CONSTRAINT_RETURN_FALSE = 4;
    const ROUTE_CONSTRAINT_FAIL = 5;
    const ROUTE_MISSING_ARGUMENT = 6;

    public function __construct($pattern, $originalPattern, $instance, $controller, $action, $callable, $customName = NULL) {
        $this->pattern = $pattern;
        $this->originalPattern = $originalPattern;

        if (preg_match_all("/\\<([a-zA-Z_][a-zA-Z_0-9-]*)\\>/", $this->originalPattern, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                $this->requiredArgs[$match[1]] = true;
            }
        }

        $this->instance = $instance;
        $this->controller = $controller;
        $this->action = $action;
        $this->callable = $callable;

        $this->customName = $customName;
    }

    public function __toString() {
        $str = "FastRouterRule(".$this->blueprint_prefix.$this->originalPattern." for ";

        if (!is_null($this->instance)) {
            $str .= " instance of ".get_class($this->instance);
        }

        if (!is_null($this->controller)) {
            $str .= " ".$this->controller;
        }

        if (!is_null($this->action)) {
            $str .= ".".$this->action;
        }

        $str .= ")";

        return $str;
    }

    public function test($url) {
        $debug = false;

        if ($debug) {
            echo "Test ".htmlspecialchars($this->blueprint_prefix.$this->originalPattern)." for ".$url."<br />";
        }

        $this->throw_exc = false;

        if (!empty($this->blueprint_prefix)) {
            if (substr($url, 0, strlen($this->blueprint_prefix)) == $this->blueprint_prefix) {
                $url = substr($url, strlen($this->blueprint_prefix));
            } else {
                if ($debug) {
                    echo "FAIL: ROUTE_BAD_PREFIX<br/>";
                }
                return self::ROUTE_BAD_PREFIX;
            }
        }

        $resp = $this->testPattern($url);
        if (!$resp) {
            if ($debug) {
                echo "FAIL: ROUTE_BAD_PATTERN<br />";
            }

            return self::ROUTE_BAD_PATTERN;
        }

        list($rule, $params) = $resp;

        // Make copy of variables, that can be altered.
        $instance = $this->instance;
        $controller = $this->controller;
        $action = $this->action;

        // Get number of non-default arguments for a function, and test whether it is possible to call this method.
        $compiledParams = array();
        $paramIndex = 0;

        if (!is_null($instance) || !is_null($controller)) {
            $refl = new \ReflectionMethod((!is_null($instance))?$instance:$controller, $action);
        } elseif (is_string($action)) {
            $refl = new \ReflectionFunction($action);
        } elseif (is_object($action)) {
            $refl = new \ReflectionMethod($action, "__invoke");
        } else {
            throw new RuntimeException("Something unspecified passed as route callback.");
        }

        foreach ($refl->getParameters() as $param) {
            if (isset($rule[$param->getName()])) {
                $compiledParams[] = $rule[$param->getName()];
            } elseif ($param->isVariadic()) {
                // Variadic parameter eats all.
                while (isset($params[$paramIndex])) {
                    $compiledParams[] = urldecode($params[$paramIndex]);
                    $paramIndex++;
                }
            } elseif (isset($params[$paramIndex])) {
                $compiledParams[] = urldecode($params[$paramIndex]);
                ++$paramIndex;
            } elseif (!$param->isOptional()) {
                // No kwarg, no regular parameter, we cannot match this method.
                if ($debug) {
                    echo "FAIL: ROUTE_BAD_ARGUMENT<br />";
                }

                return self::ROUTE_BAD_ARGUMENT;
            } else {
                // Optional param, use it's default value, because we need to
                // provide support for kwargs.
                $compiledParams[] = $param->getDefaultValue();
            }
        }

        // Too many arguments for method.
        if ($paramIndex < count($params)) {
            if ($debug) {
                echo "FAIL: ROUTE_MISSING_ARGUMENT<br />";
            }

            return self::ROUTE_MISSING_ARGUMENT;
        }

        if (!is_null($this->callable)) {
            $route = new FastRoute($this->callable, $compiledParams, $this->getWeight(), $this->blueprint);
        } else {
            $route = new FastRoute(array((!is_null($instance))?$instance:$controller, $action), $compiledParams, $this->getWeight(), $this->blueprint);
        }

        // Test constraints.
        foreach ($this->constraints as $constraint) {
            try {
                if (!$constraint->test()) {
                    if ($debug) {
                        echo "FAIL: ROUTE_CONSTRAINT_RETURN_FALSE<br />";
                    }

                    // If constraint returns false, it means skip the route.
                    return self::ROUTE_CONSTRAINT_RETURN_FALSE;
                }
            } catch (\Throwable $t) {
                // If constraint throws an exception, it means that this exception should be thrown when route wins.
                if ($debug) {
                    echo "FAIL: ROUTE_CONSTRAINT_FAIL<br />";
                }
                $route->throw_exc = $t;
            }
        }

        return $route;
    }

    /**
     * Test whether the rule matches specified URL.
     */
    protected function testPattern($url) {
        if (preg_match($this->pattern, $url, $matches)) {
            $matched_part = $matches[0];

            $rule = array();

            foreach ($matches as $key => $value) {
                if (is_numeric($key)) continue;

                // If there is an empty match, the URL is not matched at all.
                if (empty($value)) {
                    return false;
                }

                $rule[$key] = $value;
            }

            $rest = substr($url, strlen($matched_part));
            $rest = preg_replace('#^/|/$#', '', $rest);

            if (empty($rest)) {
                $params = array();
            } else {
                $params = preg_split('#[/]+#', $rest);
            }

            return array($rule, $params);
        } else {
            return false;
        }
    }

    /**
     * This method tests, whether this rule can generate URL for given arguments. It does not generate the URL
     * itself, only does all the checks.
     */
    public function hasUrlFor($instance, $controller, $action, $args, $kwargs) {
        // Custom name for controller is substitued with actual controller for this rule.
        if (!is_null($this->customName) && $controller == $this->customName) {
            $controller = NULL;

            if (!is_null($this->controller)) {
                $controller = $this->controller;
            } elseif (!is_null($this->instance)) {
                $instance = $this->instance;
            }
        }

        // Try to match controller part.
        if (!is_null($instance)) {
            if (is_null($this->instance) || $this->instance !== $instance) {
                // Given instance is different.
                $this->last_error = "Object instance differs.";
                return self::URL_INSTANCE_UNKNOWN;
            }

            // For further examination, we need to handle uniformly both instance and controller,
            // so it is safe to overwrite controller here.
            $controller = $instance;
        } elseif (!is_null($controller)) {
            if (!is_null($this->controller) && $this->controller != $controller) {
                // Controller is bound, but not the one that was requested.
                $this->last_error = "Controller differs. Specified ".$this->controller.", got ".$controller.".";
                return self::URL_OTHER_CONTROLLER;
            }
        }

        if (!is_null($this->action) && $action != $this->action) {
            // Requested other action than specified in the rule.
            $thisact = $this->action;
            if (is_object($thisact)) {
                $thisact = "instance of ".get_class($thisact);
            }
            $this->last_error = "Action differs. Specified ".$thisact.", got ".$action.".";
            return self::URL_OTHER_ACTION;
        }

        // Try to match method part.
        if (!is_null($controller) && !FastRouterCompiler::methodIsAccessible($controller, $action)) {
            // Requested method is not callable.
            $this->last_error = "Method ".((is_object($controller))?get_class($controller):$controller)."::".$action." is not callable publically.";
            return self::URL_METHOD_NOT_CALLABLE;
        } elseif (is_null($controller) && !is_callable($action)) {
            $this->last_error = "Function ".$action." is not callable.";
            return self::URL_METHOD_NOT_CALLABLE;
        }

        // Test number of arguments.
        if (!is_null($controller)) {
            $refl = new \ReflectionMethod($controller, $action);
        } else {
            $refl = new \ReflectionFunction($action);
        }

        $hasVariadicArgs = false;
        $kwArgsForParams = $kwargs;

        foreach ($kwArgsForParams as $key=>$val) {
            if (!isset($this->requiredArgs[$key])) {
                $this->last_error = "Missing required parameter ".$key.".";
                return self::URL_MISSING_REQUIRED_PARAM;
            }
        }

        reset($args);
        foreach ($refl->getParameters() as $param) {
            if ($param->isVariadic()) {
                $hasVariadicArgs = true;
            }

            if (!isset($kwArgsForParams[$param->getName()]) && !next($args)) {
                if (!$param->isOptional()) {
                    // Requested method has too much parameters.
                    $this->last_error = "Not enough arguments.";
                    return self::URL_MISSING_PARAMS_FOR_METHOD;
                }
            } elseif (isset($kwArgsForParams[$param->getName()])) {
                unset($kwArgsForParams[$param->getName()]);
            }
        }

        if (!empty($kwArgsForParams)) {
            // Unknown keyword argument that was not specified for given method.
            $this->last_error = "Unknown keyword arguments: ".implode(", ", $kwArgsForParams).".";
            return self::URL_UNKNOWN_KWARG;
        }

        if (next($args) !== false && !$hasVariadicArgs) {
            // Method does not have enough parameters.
            $this->last_error = "Too many arguments.";
            return self::URL_TOO_MANY_PARAMS;
        }

        // Here we know, the URL can be generated. So we only need to generate the URL from original pattern.
        return true;
    }

    /**
     * This method only generates URL, and does not do any checking. It is complement to the
     * hasUrlFor method, which only does checking and does not generate anything.
     * The url resolution is splitted to two parts because of possible caching of resolved rules for given arguments.
     */
    public function urlFor($instance, $controller, $action, $args, $kwargs) {
        $url = $this->originalPattern;
        $url = "/".$this->blueprint_prefix.$url;

        foreach ($kwargs as $key => $val) {
            $url = str_replace("<".$key.">", $val, $url);
        }

        $url .= "/".implode("/", $args);

        $url = FastRouterCompiler::normalizeUrl($url);
        return $url;
    }

    public function getInstance() {
        return $this->instance;
    }

    public function getController() {
        return $this->controller;
    }

    public function getAction() {
        return $this->action;
    }

    public function getCallable() {
        return $this->callable;
    }

    public function getRule() {
        return $this->blueprint_prefix."/".$this->originalPattern;
    }

    public function getWeight() {
        return strlen($this->blueprint_prefix."/".preg_replace("/\\<([a-zA-Z_][a-zA-Z_0-9-]*)\\>/", "a", $this->originalPattern));
    }

    /**
     * Set constraint of this rule. Constraints allows to do verifications while selecting route. If constraint returns
     * false, it means skip the rule. If constraint throws an exception, it means match the rule and throw this
     * exception when rule wins.
     */
    public function setConstraint(Constraint $constraint) {
        $this->constraints[] = $constraint;
    }
}
