<?php

@ini_set("error_reporting", E_ALL);
@ini_set("display_errors", true);

function url_for(...$args) {
    global $router;
    if (is_null($router)) {
        return ROOT_PATH;
    }

    if (($args[0] ?? NULL) == "static") {
        return \gcm\util\StaticFs::get_path($args[1] ?? "");
    } else {
        if (!is_null($router)) {
            return ROOT_PATH.$router->urlFor(...$args);
        } else {
            throw new \gcm\fastrouter\RoutingException("Router not instantiated.");
        }
    }
}

function has_url_for(...$args) {
    try {
        url_for(...$args);
        return true;
    } catch (\gcm\fastrouter\RoutingException $e) {
        return false;
    }
}

try {
    require __DIR__."/app/Timer.php";
    $tm = new Timer();

    define('ROOT_PATH', rtrim(dirname($_SERVER["SCRIPT_NAME"]), "/"));
    define('BASE_URL', ($_SERVER["HTTP_X_FORWARDED_PROTO"] ?? $_SERVER["REQUEST_SCHEME"])."://".($_SERVER["HTTP_X_FORWARDED_HOST"] ?? $_SERVER["HTTP_HOST"]).ROOT_PATH);
    define('BASE_PATH', __DIR__);

    function web_root_path($path) {
        $self = __DIR__;
        if (substr($path, 0, strlen($self)) == $self) {
            return ROOT_PATH.substr($path, strlen($self));
        } else {
            throw new \RuntimeException("Path '".$path."' is outside web root.");
        }
    }

    if (file_exists(__DIR__."/app/init.php")) {
        require __DIR__."/app/init.php";
    }

    $overriden = false;
    foreach (\gcm\util\Dispatcher::gen(\gcm\util\Dispatcher::EV_OVERRIDE_REQUEST) as $result) {
        $overriden |= $result;
    }

    if ($overriden) {
        exit;
    }

    $router = new \gcm\fastrouter\FastRouter();

    $url = substr($_SERVER["REQUEST_URI"], strlen(dirname($_SERVER["SCRIPT_NAME"])));
    $qsl = strlen($_SERVER["QUERY_STRING"]);
    if ($qsl > 0) {
        $url = substr($url, 0, -$qsl - 1);
    }
    $url = urldecode($url);

    \gcm\util\Dispatcher::dispatch(\gcm\util\Dispatcher::EV_ROUTER_BIND, $router);
    $route = $router->resolve($url);

    if ($route) {
        try {
            echo $router->execute($route);
        } catch (\gcm\util\exceptions\Redirect $e) {
            header("Location: ".$e->url);
            echo $e->getMessage();
        } catch (\gcm\db\exceptions\EntityNotFound $e) {
            throw new \gcm\util\exceptions\NotFound($e->getMessage(), $e);
        }
    } else {
        throw new \gcm\util\exceptions\NotFound("Resource /".$url." was not found.");
    }
} catch (\Throwable $t) {
    if (class_exists("\\gcm\\util\\Dispatcher")) {
        \gcm\util\Dispatcher::dispatch(\gcm\util\Dispatcher::EV_REQUEST_FAILED, $t);
    }
    throw $t;
} finally {
    if (class_exists("gcm\\util\\Session")) {
        \gcm\util\Session::cleanup();
    }
}
