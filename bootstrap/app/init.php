<?php

namespace gcm\kisscms;

function classNameToFile($className) {
    return strtr($className, ["\\" => DIRECTORY_SEPARATOR]).".php";
}

spl_autoload_register(function($className) {
    $fileNames = [
        classNameToFile($className)
    ];

    // gcm namespace is in core module.
    if (strpos($className, "gcm\\") === 0) {
        $fileNames[] = classNameToFile("core\\".substr($className, strlen("gcm\\")));
    }

    // gcm\kisscms as prefix for KISS-CMS modules.
    if (strpos($className, "gcm\\kisscms\\") === 0) {
        $fileNames[] = classNameToFile(substr($className, strlen("gcm\\kisscms\\")));
    }

    $root = __DIR__.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR;

    foreach ($fileNames as $fileName) {
        $fn = $root.$fileName;

        // Try file in directory.
        if (file_exists($fn)) {
            require($fn);
            break;
        }
    }
});

$ml = new \gcm\ml\ModuleLoader(__DIR__.DIRECTORY_SEPARATOR.\gcm\ml\ModuleLoader::MODULES_DIR);
$ml->require("core"); // Need this to initialize database to be able to reload configuration of disabled modules.
$ml->reload_config(); // Reload disabled modules configuration.
$ml->load(); // Load all non-disabled modules.

\gcm\util\Dispatcher::bind(\gcm\ml\EV_GET_MODULE_LOADER, function() use ($ml) {
    return $ml;
});
