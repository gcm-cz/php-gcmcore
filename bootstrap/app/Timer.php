<?php

class SavePoint {
    protected $name;
    protected $ts;
    protected $absolute;
    protected $relative;

    public function __construct(string $name, float $ts, float $absolute, float $relative) {
        $this->name = $name;
        $this->ts = $ts;
        $this->absolute = $time;
        $this->relative = $relative;
    }

    public function __toString(): string {
        return $this->name.": ".sprintf(Timer::TM_FORMAT, $this->absolute);
    }

    public function getName(): string {
        return $this->name;
    }

    public function fromStart(): float {
        return $this->absolute;
    }

    public function fromStartStr(): string {
        return sprintf(Timer::TM_FORMAT, $this->fromStart());
    }

    public function fromLast(): float {
        return $this->relative;
    }

    public function fromLastStr(): string {
        return sprintf(Timer::TM_FORMAT, $this->fromLast());
    }
}

class Timer {
    const TM_FORMAT = "%1.3f s";

    protected $start;
    protected $savepoints = [];
    protected $last;

    public function __construct() {
        $this->start = microtime(true);
        $this->last = $this->start;
    }

    public function savepoint(string $name): SavePoint {
        $now = microtime(true);
        $sp = new SavePoint(
            $name,
            $now,
            $now - $this->start,
            $now - (!empty($this->savepoints)
                ? $this->savepoints[count($this->savepoints) - 1]->ts
                : $this->start)
        );

        $this->savepoints[] = $sp;
        return $sp;
    }

    public function __toString(): string {
        return $this->fromStartStr()."\n".implode("\n", $this->savepoints);
    }

    public function fromStart(): float {
        $now = microtime(true);
        return $now - $this->start;
    }

    public function fromStartStr(): string {
        return sprintf(self::TM_FORMAT, $this->fromStart());
    }

    public function fromLast(): float {
        $now = microtime(true);
        $out = $now - $this->last;
        $this->last = $now;
        return $out;
    }

    public function fromLastStr(): string {
        return sprintf(self::TM_FORMAT, $this->fromLast());
    }
}
