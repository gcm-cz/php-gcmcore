<?php

if (php_sapi_name() !== "cli") {
    echo "Command line interface cannot be invoked from browser.";
    exit(1);
}

define('BASE_PATH', __DIR__);

if (file_exists(__DIR__."/app/init.php")) {
    require __DIR__."/app/init.php";
}

$cli = new \gcm\util\CLI();
$cli->run();
