<?php

if (!isset($_ENV) || !isset($_ENV["MYSQL_HOST"])) {
    echo "Environment variables are not propagated to PHP. Fix your PHP setup.\n";
    echo "Hint: variables_order=EGPCS\n";
    exit(1);
}

$m = new mysqli($_ENV["MYSQL_HOST"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"], $_ENV["MYSQL_DATABASE"]);

if ($m->connect_errno == 0) {
    $q = $m->query("SELECT 1");
    if ($a = $q->fetch_array()) {
        assert($a["1"] == "1");
    } else {
        exit(1);
    }
} else {
    exit(1);
}

echo "passed.\n";
