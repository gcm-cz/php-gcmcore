#!/bin/bash

# Executable configurations
PHP=${PHP:-php}
GIT=${GIT:-git}
COMPOSER=${COMPOSE:-composer}

CURDIR=$(pwd)

teardown() {
    cd $CURDIR
}

trap teardown EXIT

cat << EOF
==============================================================================
  GCM Core installer
  ------------------

  Prerequisites:
  - GIT (will look for: ${GIT})
  - MariaDB server (10.3+)
  - PHP (7.3+) (will look for: ${PHP})
  - PHP Composer (will look for: ${COMPOSER})
  - Shell

  Will install and bootstrap GCM Core based website in current directory:
  ${CURDIR}
==============================================================================
EOF

INTERACTIVE=0

if [ -t 0 ]; then
    read -p "Is this information correct? (yN): " cont
    if [ -z "$cont" ]; then
        cont="n"
    fi

    if [ "$cont" != "y" ]; then
        echo "Setup will now exit."
        exit 1
    fi

    INTERACTIVE=1
else
    echo "Continuing in non-interactive mode."
fi

# --- Check GIT is working --------------------------------------------------------------------------------------------
echo
echo "Step 1: Check GIT is working."
if ! command -v "$GIT" > /dev/null; then
    echo "Unable to find GIT executable. Specify environment variable GIT with correct location of GIT executable."
    exit 1
fi

$GIT status > /dev/null 2> /dev/null
RESULT=$?

if [ $RESULT -ne 127 ]; then
    echo "passed."
fi

# --- Initialize GIT repository ---------------------------------------------------------------------------------------
echo
echo "Step 2: Initialize GIT repository."
if [ $RESULT -eq 128 ]; then
    $GIT init .
elif [ $RESULT -eq 0 ]; then
    echo "GIT already initialized. Skipping."
else
    $GIT status
    echo "GIT not working properly."
    exit 1
fi

set -e

# --- Setup basic directory structure ---------------------------------------------------------------------------------
echo
echo "Step 3: Setup basic directory structure."
mkdir -p app/modules
echo "done."

# --- Setup GCM Core submodule ----------------------------------------------------------------------------------------
echo
echo "Step 4: Setup GCM Core submodule."
if [ ! -d app/modules/core/ ]; then
    $GIT submodule add git@gitlab.com:gcm-cz/php-gcmcore app/modules/core
else
    echo "Submodule already initialized."
fi

cd app/modules/core/bootstrap
echo "done."

# --- Test PHP executable ---------------------------------------------------------------------------------------------
echo
echo "Step 5: Test PHP executable..."

if ! command -v $PHP > /dev/null; then
    echo "PHP binary executable not found. Specify env variable PHP with correct location of PHP binary."
    exit 1
fi

PHP_VERSION=$(php <<< "<?php echo PHP_VERSION;" 2>/dev/null)
PHP_VERSION_MAJOR=$(echo $PHP_VERSION | cut -d'.' -f1)
PHP_VERSION_MINOR=$(echo $PHP_VERSION | cut -d'.' -f2)

if [ "$PHP_VERSION_MAJOR" -lt "7" ] || [ "$PHP_VERSION_MAJOR" -eq "7" -a "$PHP_VERSION_MINOR" -lt "2" ]; then
    echo "Detected PHP version $PHP_VERSION. Minimum required version is PHP 7.2."
    exit 1
fi

echo "passed. Detected PHP ${PHP_VERSION}."

# --- Setup MySQL -----------------------------------------------------------------------------------------------------
echo
echo "Step 6: Setup MySQL connection..."
if [ ! -f ../../../config.php ]; then
    if [ -z "$MYSQL_HOST" ]; then
        if [ "$INTERACTIVE" -eq "1" ]; then
            read -p 'Enter MySQL server host: ' MYSQL_HOST
        else
            echo "Missing environment variable MYSQL_HOST."
        fi
    fi

    if [ -z "$MYSQL_USER" ]; then
        if [ "$INTERACTIVE" -eq "1" ]; then
            read -p 'Enter MySQL server username: ' MYSQL_USER
        else
            echo "Missing environment variable MYSQL_USER."
        fi
    fi

    if [ -z "$MYSQL_PASSWORD" ]; then
        if [ "$INTERACTIVE" -eq "1" ]; then
            read -sp 'Enter MySQL server password: ' MYSQL_PASSWORD
            echo
        else
            echo "Missing environment variable MYSQL_PASSWORD."
        fi
    fi

    if [ -z "$MYSQL_DATABASE" ]; then
        if [ "$INTERACTIVE" -eq "1" ]; then
            read -p 'Enter MySQL database name: ' MYSQL_DATABASE
        else
            echo "Missing environment variable MYSQL_DATABASE."
        fi
    fi

    export MYSQL_HOST MYSQL_USER MYSQL_PASSWORD MYSQL_DATABASE

    # Test MySQL connection
    php -f .test_mysql.php

    # --- Create config file ----------------------------------------------------------------------------------------------
    echo
    echo "Step 7: Writing config file..."
    cat << EOF > ../../../config.php
<?php

use \gcm\config\Config;

function getenv_d(\$env, \$default=null) {
    \$out = \$_ENV[\$env] ?? \$default;
    if (empty(\$out)) {
        return \$default;
    } else {
        return \$out;
    }
}

Config::set("mysql", "host", getenv_d("MYSQL_HOST", "${MYSQL_HOST}"));
Config::set("mysql", "user", getenv_d("MYSQL_USER", "${MYSQL_USER}"));
Config::set("mysql", "password", getenv_d("MYSQL_PASSWORD", "${MYSQL_PASSWORD}"));
Config::set("mysql", "database", getenv_d("MYSQL_DATABASE", "${MYSQL_DATABASE}"));

EOF
    echo "done."

else
    echo "Skipping, as config.php already exists."
    echo
    echo "Step 7: Writing config file..."
    echo "skipping, already exists."
fi

# --- Install basic structure -----------------------------------------------------------------------------------------
echo
echo "Step 8: Installing basic scripts."

# Just copy. Maybe create symlinks would be better, but it could cause problems with web servers not configured
# to follow symlinks.
cp -rav * $CURDIR
cp -av .gitignore $CURDIR

echo "done."

# --- Install dependencies --------------------------------------------------------------------------------------------
echo
echo "Step 9: Install dependencies"

if ! command -v $COMPOSER; then
    echo "PHP Composer not found."
    exit 1
fi

cd $CURDIR/app/modules/core/
$COMPOSER install

# --- Setup database --------------------------------------------------------------------------------------------------
echo
echo "Step 10: Applying database migrations..."
cd $CURDIR
php -f cli.php migrate

# --- Finish ----------------------------------------------------------------------------------------------------------
echo
echo "Setup is done. The core now should be functional. You can now remove install.sh and proceed with development."
echo
