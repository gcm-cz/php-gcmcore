<?php

namespace gcm;

require_once __DIR__."/i18n_helpers.php";

return (new \gcm\ml\Module())
    ->setName("core")
    ->setTitle(d_p("admin", "Core"))
    ->setDescription(d_p("admin", "Core site code."))
    ->setVersion("1.0.0");