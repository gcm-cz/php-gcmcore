<?php

namespace gcm\kisscms;

use gcm\util\exceptions\Forbidden;

function has_action(string $action) {
    $result = !is_null(LoginManager::getCurrentUser()) && LoginManager::getCurrentUser()->has_action($action);
    return $result;
}

function ensure_action(string $action) {
    if (!has_action($action)) {
        throw new Forbidden();
    }
}

function ensure_any_action(string $action, string ...$actions) {
    $result = has_action($action);
    foreach ($actions as $action) {
        $result |= has_action($action);
    }
    return $result;
}
