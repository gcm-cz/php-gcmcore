<?php

namespace gcm\controllers;

use \gcm\util\Dispatcher;
use \gcm\fastrouter\Controller;
use \gcm\fastrouter\Router;

/**
 * Generic ApiController that just provides method _execute() than can wrap execution of subsequent methods
 * and for example transform input/output, or provide custom error handling.
 */
class ApiController implements Controller {
    public function __construct() {
        Dispatcher::bind(Router::EV_WRAP_EXECUTE, function($cb, $method, array $args) {
            return $this->_execute($cb, $method, $args);
        });
    }

    protected function _execute(callable $callback, callable $method, array $args) {
        return $callback();
    }
}
