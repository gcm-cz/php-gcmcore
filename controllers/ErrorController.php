<?php

namespace gcm\controllers;

require_once __DIR__."/TemplatedController.php";

use \gcm\util\exceptions\HTTPException;
use \gcm\util\exceptions\Forbidden;
use \gcm\util\exceptions\NotFound;

if (class_exists("\\gcm\\controllers\\TemplatedController")) {
    class ErrorController extends TemplatedController {
        public function http_error(HTTPException $error) {
            $this->twig->addFunction(new \Twig\TwigFunction("get_error_lines", function($file, $line) {
                if (!file_exists($file)) {
                    return [];
                }

                $f = file($file);

                $out = [];
                foreach (array_slice($f, $line - 2, 3, true) as $line=>$content) {
                    $out[] = [$line + 1, $content];
                }

                return $out;
            }));
            $this->twig->addGlobal("display_errors", ini_get("display_errors"));

            \http_response_code($error->getCode());

            if ($error->getPrevious() instanceof \gcm\db\exceptions\ConnectError) {
                return $this->renderTemplate("error.html", ["error" => $error]);
            }

            try {
                try {
                    return $this->renderLayoutTemplate("error".$error->getCode().".html", ["error" => $error]);
                } catch (\Twig\Error\LoaderError $e) {
                    try {
                        return $this->renderLayoutTemplate("error".($error->getCode() / 100 * 100).".html", ["error" => $error]);
                    } catch (\Twig\Error\LoaderError $e) {
                        return $this->renderLayoutTemplate("error.html", ["error" => $error]);
                    }
                }
            } catch (\Throwable $t) {
                try {
                    return $this->renderTemplate("error".$error->getCode().".html", ["error" => $error]);
                } catch (\Twig\Error\LoaderError $e) {
                    try {
                        return $this->renderTemplate("error".($error->getCode() / 100 * 100).".html", ["error" => $error]);
                    } catch (\Twig\Error\LoaderError $e) {
                        return $this->renderTemplate("error.html", ["error" => $error]);
                    }
                }
            }
        }

        public function error403(\Throwable $t=NULL) {
            $e = new Forbidden(NULL, $t);
            return $this->http_error($e);
        }

        public function error404(\Throwable $t=NULL) {
            $e = new NotFound(NULL, $t);
            return $this->http_error($e);
        }

        public function error500(\Throwable $t) {
            $e = new HTTPException(HTTPException::HTTP_INTERNAL_SERVER_ERROR, NULL, $t);
            return $this->http_error($e);
        }

        protected function selectLayout() {
            try {
                parent::selectLayout();
            } catch (\Throwable $t) {
            }
        }
    }
}
