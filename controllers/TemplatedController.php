<?php

namespace gcm\controllers;

use \gcm\util\TwigEnv;
use \gcm\fastrouter\Controller;
use \gcm\kisscms\Layout;

class TemplatedController extends TwigEnv implements Controller {
    protected function renderTemplate(string $name, array $context = []) {
        $template = $this->twig->load($name);
        return $template->render($context);
    }

    protected function selectLayout() {
        $layout = Layout::getLayout();

        // Clone loader, because we want to modify it only for one render and then restore it's previous settings.
        $loader = clone $this->twig->getLoader();
        $loader->prependPath($layout->getPath());
        $this->twig->setLoader($loader);

        $layout->select();
    }

    protected function renderLayoutTemplate(string $name, array $context = []) {
        $original = $this->twig->getLoader();
        try {
            $this->selectLayout();

            $template = $this->twig->load($name);
            return $template->render($context);
        } finally {
            $this->twig->setLoader($original);
        }
    }
}
