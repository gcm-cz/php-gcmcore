<?php

namespace gcm\i18n;

use \gcm\util\Dispatcher;
use \gcm\config\Config;

class L10N {
    const EV_LOAD_RESOURCES = __CLASS__."::EV_LOAD_RESOURCES";

    const DEFAULT_LOCALE = "en_US";
    const DEFAULT_DOMAIN = "default";

    protected $domains = [];
    protected $locale;
    protected static $resources_loaded = false;

    protected static $instance;

    public function __construct($locale=NULL) {
        $found = false;
        foreach (I18N::list() as $one_locale) {
            if (!is_null($locale) && $one_locale->getLocale() == $locale) {
                $found = true;
            }
        }

        if (!$found) {
            $locale = Config::get("core", "default_locale", self::DEFAULT_LOCALE);
        }

        $this->locale = $locale;
    }

    protected function ensureResources() {
        if (!self::$resources_loaded) {
            Dispatcher::dispatch(self::EV_LOAD_RESOURCES, $this, $this->locale);
            self::$resources_loaded = true;
        }
    }

    public function getLocaleName() {
        return $this->locale;
    }

    public static function getInstance($locale=NULL) {
        if (is_null(self::$instance) || (!is_null($locale) && self::$instance->locale != $locale)) {
            self::$instance = new self($locale);
        }

        return self::$instance;
    }

    public function addResourceFile($resource, $domain=self::DEFAULT_DOMAIN) {
        if (!isset($this->domains[$domain])) {
            $this->domains[$domain] = new Domain();
        }

        $this->domains[$domain]->addResourceFile($resource);
    }

    public function addResource($resource, $domain=self::DEFAULT_DOMAIN) {
        if (!isset($this->domains[$domain])) {
            $this->domains[$domain] = new Domain();
        }

        $this->domains[$domain]->addResource($resource);
    }

    public function getDomain($domain=self::DEFAULT_DOMAIN) {
        $this->ensureResources();
        return $this->domains[$domain] ?? $this->domains[self::DEFAULT_DOMAIN] ?? new Domain();
    }
}
