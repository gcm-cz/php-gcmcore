<?php

namespace gcm\i18n;

class ParseException extends \Exception {
    public function __construct($message, $resource, $line, $column) {
        parent::__construct(\sprintf("%s in %s on line %s, column %s.", $message, $resource, $line, $column));
    }
}
