<?php

namespace gcm\i18n;

class Domain {
    protected $strings = [];

    /**
     * Add translation resource from file.
     */
    public function addResourceFile(string $resource) {
        if (file_exists($resource)) {
            $f = file_get_contents($resource);
            $this->addResource($f, $resource);
        } else {
            throw new \RuntimeException("Specified resource file ".$resource." does not exists.");
        }
    }

    /**
     * Add translation resource from string.
     */
    public function addResource(string $resource, string $name="{}") {
        $parser = new ResourceParser();
        $this->strings = array_merge($this->strings, $parser->parse($resource, $name));
    }

    public function getLocalizedMessage(string $message, int $amount=NULL) {
        if (!isset($this->strings[$message])) {
            return $message;
        }

        $message = $this->strings[$message];
        if (is_array($message)) {
            if (isset($message[$amount])) {
                return $message[$amount];
            } else {
                return end($message);
            }
        } else {
            return $message;
        }
    }
}
