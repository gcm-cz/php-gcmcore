<?php

namespace gcm\i18n;

class ResourceParser {
    const TOK_STRING = 1;
    const TOK_ASSIGN = 2;
    const TOK_ARRAY = 3;
    const TOK_END = 4;

    const ST_WHITESPACE = 1;
    const ST_LINE_COMMENT = 2;
    const ST_BLOCK_COMMENT = 3;
    const ST_STRING = 4;
    const ST_ARRAY = 6;

    protected $string;
    protected $source;

    protected $line;
    protected $column;

    protected $prevline;
    protected $prevcolumn;

    protected $pos;
    protected $len;

    protected $out;

    public function parse(string $string, string $source) {
        $this->string = $string;
        $this->source = $source;

        $this->line = 1;
        $this->column = 1;
        $this->prevline = 1;
        $this->prevcolumn = 1;

        $this->pos = 0;
        $this->len = strlen($this->string);

        $this->out = [];

        while ($this->parse_string_combo());

        return $this->out;
    }

    protected function tok_to_string($type) {
        switch ($type) {
            case self::TOK_STRING:
                return "STRING";

            case self::TOK_ASSIGN:
                return "ASSIGN";

            case self::TOK_ARRAY:
                return "ARRAY";

            case self::TOK_END:
                return "END";

            default:
                return "ERROR";
        }
    }

    protected function parse_string_combo() {
        list($type, $source) = $this->get_token();
        if ($type == self::TOK_END) {
            return false;
        }

        if ($type != self::TOK_STRING) {
            $this->error("Unexpected ".$this->tok_to_string($type));
        }

        list($type, $dummy) = $this->get_token();
        if ($type != self::TOK_ASSIGN) {
            $this->error("Unexpected ".$this->tok_to_string($type));
        }

        list($type, $buffer) = $this->get_token();
        if ($type == self::TOK_STRING) {
            $this->out[$source] = "";

            while ($type == self::TOK_STRING) {
                $this->out[$source] .= $buffer;
                list($type, $buffer) = $this->get_token();
            }

            if ($type == self::TOK_END) {
                return true;
            } else {
                $this->error("Unexpected ".$this->tok_to_string($type));
            }
        } elseif ($type == self::TOK_ARRAY) {
            $this->out[$source] = [];
            while ($type != self::TOK_END) {
                $num = $buffer;
                list($type, $buffer) = $this->get_token();
                if ($type != self::TOK_ASSIGN) {
                    $this->error("Unexpected ".$this->tok_to_string($type));
                }

                list($type, $buffer) = $this->get_token();
                if ($type != self::TOK_STRING) {
                    $this->error("Unexpected ".$this->tok_to_string($type));
                }

                $this->out[$source][$num] = $buffer;

                list($type, $buffer) = $this->get_token();
            }
            return true;
        }
    }

    protected function advance($howmany) {
        $this->prevline = $this->line;
        $this->prevcolumn = $this->column;

        while ($howmany > 0) {
            if ($this->string[$this->pos] == "\n") {
                ++$this->line;
                $this->column = 1;
            }
            ++$this->pos;
            --$howmany;
        }
    }

    protected function look_ahead($howmany) {
        if ($howmany == 1) {
            return $this->string[$this->pos] ?? "";
        } else {
            return substr($this->string, $this->pos, $howmany);
        }
    }

    protected function error($str) {
        throw new ParseException($str, $this->source, $this->prevline, $this->prevcolumn);
    }

    protected function get_token() {
        $state = self::ST_WHITESPACE;
        $buffer = "";

        while ($this->pos < $this->len) {
            $ch = $this->string[$this->pos];
            $this->advance(1);

            switch ($state) {
                case self::ST_WHITESPACE:
                    if ($ch == "#") {
                        $state = self::ST_LINE_COMMENT;
                    } elseif ($ch == "/" && $this->look_ahead(1) == "*") {
                        $this->advance(1);
                        $state = self::ST_BLOCK_COMMENT;
                    } elseif ($ch == "\"") {
                        $state = self::ST_STRING;
                        $buffer = "";
                    } elseif ($ch == "=" && $this->look_ahead(1) == ">") {
                        $this->advance(1);
                        return [self::TOK_ASSIGN, ""];
                    } elseif ($ch == "[") {
                        $state = self::ST_ARRAY;
                        $buffer = "";
                    } elseif ($ch == ";") {
                        return [self::TOK_END, ""];
                    } elseif (!ctype_space($ch)) {
                        $this->error("Invalid character '".$ch."'");
                    }
                    break;

                case self::ST_LINE_COMMENT:
                    if ($ch == "\n") {
                        $state = self::ST_WHITESPACE;
                    }
                    break;

                case self::ST_BLOCK_COMMENT:
                    if ($ch == "*" && $this->look_ahead(1) == "/") {
                        $this->advance(1);
                        $state = self::ST_WHITESPACE;
                    }
                    break;

                case self::ST_STRING:
                    if ($ch == "\"") {
                        $state = self::ST_WHITESPACE;
                        return [self::TOK_STRING, $buffer];
                    } elseif ($ch == "\\") {
                        $buffer .= $this->read_escape_char();
                    } else {
                        $buffer .= $ch;
                    }
                    break;

                case self::ST_ARRAY:
                    if (ctype_digit($ch)) {
                        $buffer .= $ch;
                    } elseif ($ch == "]") {
                        return [self::TOK_ARRAY, $buffer];
                    } elseif (ctype_space($ch)) {
                        continue;
                    } else {
                        $this->error("Invalid character '".$ch."'");
                    }
                    break;
            }
        }

        return [self::TOK_END, ""];
    }

    protected function read_escape_char() {
        $ch = $this->string[$this->pos];
        switch ($ch) {
            case 'a':
                $this->advance(1);
                return "\a";

            case 'b':
                $this->advance(1);
                return "\b";

            case 'f':
                $this->advance(1);
                return "\f";

            case 'n':
            case "\n":
                $this->advance(1);
                return "\n";

            case 'r':
            case "\r":
                $this->advance(1);
                return "\r";

            case 't':
            case "\t":
                $this->advance(1);
                return "\t";

            case 'v':
                $this->advance(1);
                return "\v";

            case 'x':
                $this->advance(1); // skip x
                $hex = $this->look_ahead(2); // read two hex chars
                if (ctype_xdigit($hex)) {
                    $this->advance(3);
                    return chr(hexdec($hex));
                } else {
                    $this->error("Unknown escape sequence");
                }

            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
                $oct = $this->look_ahead(3);
                if (decoct(octdec($oct)) == $oct) {
                    $this->advance(3);
                    return chr(octdec($oct));
                } else {
                    $this->error("Unknown escape sequence");
                }

            default:
                $this->advance(1);
                return $ch;
        }
    }
}
