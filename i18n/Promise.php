<?php

namespace gcm\i18n;

/**
 * Holds translatable string and translates it when required.
 */
class Promise {
    public function __construct($domain, $value, $amount=NULL) {
        $this->domain = $domain;
        $this->value = $value;
        $this->amount = $amount;
    }

    public function __toString(): string {
        return $this->get();
    }

    public function get(\gcm\i18n\L10N $l10n=NULL): string {
        if (is_null($l10n)) {
            $l10n = \gcm\i18n\L10N::getInstance();
        }

        return $l10n->getDomain($this->domain)->getLocalizedMessage($this->value, $this->amount);
    }
}
