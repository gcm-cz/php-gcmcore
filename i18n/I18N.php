<?php

namespace gcm\i18n;

/**
 * Provides description of possible language variant.
 */
class I18N {
    const EV_ENUM_LOCALES = __CLASS__."::EV_ENUM_LOCALES";

    protected $locale;
    protected $name;

    public function __construct(string $locale, string $name) {
        $this->locale = $locale;
        $this->name = $name;
    }

    public function getLocale() {
        return $this->locale;
    }

    public function getName() {
        return $this->name;
    }

    public static function list() {
        return array_filter(\gcm\util\Dispatcher::dispatch(self::EV_ENUM_LOCALES), function($locale) {
            if (is_null($locale)) {
                return false;
            }

            if (!is_object($locale) || !($locale instanceof I18N)) {
                throw new \RuntimeException(self::EV_ENUM_LOCALES." handler must return instance of ".I18N::class." class.");
            }

            return true;
        });
    }
}
