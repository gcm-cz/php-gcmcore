<?php

namespace gcm\config;

/**
 * Helper class to access global configuation.
 */
class Config {
    private static $instance = NULL;

    /**
     * Get instance of global ConfigSet.
     */
    public static function getInstance(CacheInterface $cache=NULL) {
        if (is_null(self::$instance)) {
            self::$instance = new ConfigSet($cache);
        } elseif (!is_null($cache)) {
            self::$instance->setCache($cache);
        }

        return self::$instance;
    }

    /**
     * Get config option.
     * @param string $section Section name
     * @param string $name Option name
     * @param $default Value returned when option does not exists.
     * @return Value of option specified by ($section, $name) combination, or $default if requested option
     *    does not exists.
     */
    public static function get(string $section, string $name, $default=NULL) {
        return self::getInstance()->get($section, $name, $default);
    }

    /**
     * Set config option.
     * @param string $section Section name
     * @param string $name Option name
     * @param $value Option value
     * @param bool $final Is the value final?
     * @param int $source Source of the value. One of ConfigValue::SOURCE_* constants.
     * @return Instance of ConfigValue representing specified option.
     */
    public static function set(string $section, string $name, $value, bool $final=ConfigValue::DEFAULT_FINAL, int $source=ConfigValue::DEFAULT_SOURCE) {
        return self::getInstance()->set($section, $name, $value, $final, $source);
    }
}
