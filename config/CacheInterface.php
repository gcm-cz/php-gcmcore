<?php

namespace gcm\config;

/**
 * Generic interface to provide cache for configuration.
 */
interface CacheInterface {
    /**
     * Load configuration from cache.
     * @param ConfigSet $set ConfigSet where to set the configuration.
     * @return true if load was successfull.
     */
    public function load(ConfigSet $set): bool;

    /**
     * Store configuration to cache.
     * @param ConfigSet $set ConfigSet to store.
     * @return true if store was successfull.
     */
    public function store(ConfigSet $set): bool;

    /**
     * Invalidate configuration item in cache.
     * @param value ConfigValue to invalidate. If NULL, whole cache should be invalidated.
     */
    public function invalidate(ConfigValue $value=NULL);
}
