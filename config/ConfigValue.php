<?php

namespace gcm\config;

/**
 * Class representing config values.
 */
class ConfigValue {
    const SOURCE_CODE = 1;  /**< ConfigValue come from source code. */
    const SOURCE_DB = 2;  /**< ConfigValue come from database. */
    const SOURCE_CACHE = 3;  /**< ConfigValue come from cache. */

    const DEFAULT_FINAL = false;  /**< Default value of $final option if not specified. */
    const DEFAULT_SOURCE = self::SOURCE_CODE;  /**< Default value of $source option if not specified. */

    protected $section;  /**< Name of section where this value is located. */
    protected $name;  /**< Name of option */
    protected $value;  /**< Value */
    protected $final;  /**< If true, value cannot be further modified. */
    protected $source;  /**< Source where this option come from. */
    protected $changed = false;  /**< Whether the option was changed and should be saved. */

    /**
     * Constructor.
     * @param string $section Section name
     * @param string $name Option name
     * @param $value Option value
     * @param bool $final Is value final?
     * @param int $source Source of the value. Can be one of ConfigValue::SOURCE_* constants.
     */
    public function __construct(string $section, string $name, $value, bool $final=self::DEFAULT_FINAL, int $source=self::DEFAULT_SOURCE) {
        $this->section = $section;
        $this->name = $name;
        $this->value = $value;
        $this->final = $final;
        $this->source = $source;
    }

    /**
     * Set value of option. Can be only set if value is not final.
     * @param $value Value of option.
     * @throws RuntimeException when trying to modify final value.
     */
    public function setValue($value) {
        if (!$this->final) {
            $this->value = $value;
            $this->changed = true;
        } else {
            throw new \RuntimeException("Trying to modify final value ".$this->section."::".$this->name.".");
        }
    }

    /**
     * Set final indicator of value. Final option cannot be further modified.
     * @param bool $final Is value final?
     * @throws RuntimeException when trying to modify final value.
     */
    public function setFinal(bool $final) {
        if (!$this->final) {
            $this->final = $final;
        } else {
            throw new \RuntimeException("Trying to modify final value ".$this->section."::".$this->name.".");
        }
    }

    /**
     * Get section name.
     * @return string Section name
     */
    public function getSection() {
        return $this->section;
    }

    /**
     * Get option name.
     * @return string Option name.
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get value.
     * @return Option value.
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Return whether the option is final.
     * @return bool True if option is final, false otherwise.
     */
    public function isFinal() {
        return $this->final;
    }

    /**
     * Get source of option.
     * @return int One of ConfigValue::SOURCE_* constants.
     */
    public function getSource() {
        return $this->source;
    }

    /**
     * Set source of option.
     * @param int $source One of ConfigValue::SOURCE_* constants.
     * @throws RuntimeException when trying to modify final value.
     */
    public function setSource(int $source) {
        if (!$this->final) {
            $this->source = $source;
        } else {
            throw new \RuntimeException("Trying to modify final value ".$this->section."::".$this->name.".");
        }
    }

    /**
     * Return true if value has been changed in runtime.
     * @return bool True if option has been changed.
     */
    public function isChanged() {
        return $this->changed;
    }

    /**
     * Set changed status of option.
     * @param bool $changed Changed status.
     */
    public function setChanged(bool $changed) {
        $this->changed = $changed;
    }
}
