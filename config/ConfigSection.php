<?php

namespace gcm\config;

/**
 * Class representing one section of configuration, holding various options.
 */
class ConfigSection {
    protected $name;
    protected $values = [];

    /**
     * Constructor.
     * @param string $name Name of config section.
     */
    public function __construct(string $name) {
        $this->name = $name;
    }

    /**
     * Return name of this section.
     * @return string This section name.
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get value of option.
     * @param string $name Option name
     * @param $default Default value returned when given option does not exists.
     * @return Value of option or $default if given option does not exists.
     */
    public function get(string $name, $default=NULL) {
        if (isset($this->values[$name])) {
            return $this->values[$name]->getValue();
        } else {
            return $default;
        }
    }

    /**
     * Set value of option.
     * @param string $name Option name.
     * @param $value Option value
     * @param bool $final Optional. Is value final?
     * @param int $source Optional. Source of option. Can be one of ConfigValue::SOURCE_* constants.
     * @return Instance of ConfigValue representing setted option.
     */
    public function set(string $name, $value, bool $final, int $source=NULL) {
        if (isset($this->values[$name])) {
            $val = $this->values[$name];

            $val->setValue($value);
            if (!is_null($source)) {
                $val->setSource($source);
            }

            return $val;
        } else {
            return $this->values[$name] = new ConfigValue($this->name, $name, $value, $final, $source);
        }
    }

    /**
     * Return list of option names in this section.
     * @return array of string representing name of options in this section.
     */
    public function items() {
        return array_keys($this->values);
    }

    /**
     * Return ConfigValue object representing given option.
     * @param string $name Option name.
     * @return ConfigValue representing that option or NULL if that option does not exists.
     */
    public function item($name) {
        return $this->values[$name] ?? NULL;
    }
}
