<?php

namespace gcm\config;

/**
 * Set of config sections.
 */
class ConfigSet {
    protected $sections = [];  /**< Holds sections. */
    protected $db_table_name = NULL;  /**< Name of database table where to store the configuration. */
    protected $cache;

    public function __construct(CacheInterface $cache=NULL) {
        $this->cache = $cache;
    }

    public function __destruct() {
        $to_insert = [];
        $args = [];

        foreach ($this->sections as $section) {
            foreach ($section->items() as $item) {
                $item = $section->item($item);
                if ($item->getSource() == ConfigValue::SOURCE_DB && $item->isChanged()) {
                    $to_insert[] = "(?, ?, ?)";
                    $args[] = $section->getName();
                    $args[] = $item->getName();
                    $args[] = $item->getValue();
                }
            }
        }

        if (!empty($to_insert) && !is_null($this->db_table_name)) {
            inject_transaction(function($db) use ($to_insert, $args) {
                $db->query("INSERT INTO `".$this->db_table_name."` (`section`, `item`, `value`)
                    VALUES ".implode(", ", $to_insert)."
                    ON DUPLICATE KEY UPDATE `value` = VALUES(`value`)", ...$args);
            });
        }
    }

    /**
     * Change cache provider of this config set.
     * @param CacheInterface $cache New cache provider.
     */
    public function setCache(CacheInterface $cache=NULL) {
        $this->cache = $cache;
    }

    /**
     * Get configured cache interface.
     */
    public function getCache(): ?CacheInterface {
        return $this->cache;
    }

    /**
     * Load configuration from database.
     * @param \gcm\db\Transaction $db Transaction to the database.
     * @param string $table_name Name of database table where the configuration is stored.
     * @return True if load was successfull.
     */
    public function fromDb(\gcm\db\Transaction $db, string $table_name="config"): bool {
        try {
            $q = $db->query("SELECT `section`, `item`, `value` FROM `".$table_name."`");
            while ($a = $q->fetch_object()) {
                $this->set($a->section, $a->item, $a->value, false, ConfigValue::SOURCE_DB);
            }
            $this->db_table_name = $table_name;

            if (!is_null($this->cache)) {
                $this->cache->store($this);
            }

            return true;
        } catch (\gcm\db\exceptions\QueryError $e) {
            // Table does not exists is ignored, because it is not mandatory.
            if ($e->getCode() != 1146) {
                throw $e;
            }

            return false;
        }
    }

    /**
     * Load configuration from cache, if cache is enabled.
     * @return true if load was successfull.
     */
    public function fromCache(): bool {
        if ($this->cache) {
            return $this->cache->load($this);
        } else {
            return false;
        }
    }

    /**
     * Get value of option.
     * @param string $section Section name.
     * @param string $name Option name.
     * @param $default Default value used when option does not exists.
     * @return Value of option specified by ($section, $name) or $default if given option does not exists.
     */
    public function get(string $section, string $name, $default=NULL) {
        if (!isset($this->sections[$section])) {
            return $default;
        }

        return $this->sections[$section]->get($name, $default) ?? $default;
    }

    /**
     * Set value of option.
     * @param string $section Section name.
     * @param string $name Option name.
     * @param $value Option value.
     * @param bool $final Optional. Is the value final or not? Defaults to false.
     * @param int $source Source of value. Can be one of ConfigValue::SOURCE_* constants.
     * @return ConfigValue instance of given option.
     */
    public function set(string $section, string $name, $value, bool $final=ConfigValue::DEFAULT_FINAL, int $source=ConfigValue::DEFAULT_SOURCE): ConfigValue {
        if (!isset($this->sections[$section])) {
            $this->sections[$section] = new ConfigSection($section);
        }

        // When config is changed, clear the config cache.
        $val = $this->sections[$section]->item($name);
        if ($val !== NULL && $val->getSource() == ConfigValue::SOURCE_CACHE && !is_null($this->cache)) {
            $this->cache->invalidate($val);
        }

        return $this->sections[$section]->set($name, $value, $final, $source);
    }

    /**
     * Get instance of ConfigSection with given name. If section does not exists, it is crated.
     * @param string $name Name of section.
     * @return ConfigSection instance.
     */
    public function getSection(string $name): ConfigSection {
        if (!isset($this->sections[$name])) {
            return $this->sections[$name] = new ConfigSection($name);
        }
        return $this->sections[$name];
    }

    /**
     * Return list of section names.
     * @return arrat of string representing names of existing sections.
     */
    public function listSections(): array {
        return array_keys($this->sections);
    }
}
