<?php

namespace gcm\config;

/**
 * APCU cache interface. Works only if APCU module is enabled.
 */
class APCUCache implements CacheInterface {
    const DEFAULT_CACHE_KEY = "config";
    const DEFAULT_TTL_SECONDS = 60;

    protected $apcu_cache_key;
    protected $ttl;

    /**
     * Create new instance of APCU Cache. If APCU is not available, returns NULL. NULL means that cache won't be used.
     */
    public static function create(string $apcu_cache_key=self::DEFAULT_CACHE_KEY, int $ttl=self::DEFAULT_TTL_SECONDS): ?APCUCache {
        if (!function_exists("apcu_enabled") || !apcu_enabled()) {
            return NULL;
        }

        return new self($apcu_cache_key, $ttl);
    }

    protected function __construct(string $apcu_cache_key=self::DEFAULT_CACHE_KEY, int $ttl=self::DEFAULT_TTL_SECONDS) {
        $this->apcu_cache_key = $apcu_cache_key;
        $this->ttl = $ttl;
    }

    public function load(ConfigSet $config): bool {
        if (!apcu_exists($this->apcu_cache_key)) {
            return false;
        }

        $success = false;
        $content = apcu_fetch($this->apcu_cache_key, $success);
        if (!$success) {
            return false;
        }

        foreach ($content as $section => $options) {
            foreach ($options as $key => $val) {
                $config->set($section, $key, $val, false, ConfigValue::SOURCE_CACHE);
            }
        }

        return true;
    }

    public function store(ConfigSet $config): bool {
        $store = [];
        foreach ($config->listSections() as $section) {
            $options = $config->getSection($section);

            $store[$section] = [];
            foreach ($options->items() as $name) {
                $option = $options->item($name);

                // Hardcoded options are not cached.
                if ($option->getSource() != ConfigValue::SOURCE_CODE) {
                    $store[$section][$name] = $option->getValue();
                }
            }
        }

        return apcu_store($this->apcu_cache_key, $store, $this->ttl);
    }

    public function invalidate(ConfigValue $value=NULL) {
        apcu_delete($this->apcu_cache_key);
    }
}
