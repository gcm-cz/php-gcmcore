### GCM Core

This repository is just a library that must become part of another project. It is not working by itself.

## Bootstrap

Easiest way to bootstrap new project using GCM Core is by executing simple one-liner in shell:

```
curl https://gitlab.com/gcm-cz/php-gcmcore/-/raw/master/bootstrap/install.sh -o install.sh && bash install.sh
```

Or if you want more control over the setup progress, you can install it manually:

1. `mkdir -p app/modules`
1. `git submodule add https://gitlab.com/gcm-cz/php-gcmcore/ app/modules/core`
1. `cp -r app/modules/core/bootstrap/* .`
1. Edit `app/config.php.example`.
1. Now you should be ready to start development.

