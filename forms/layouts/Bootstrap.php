<?php

namespace gcm\forms\layouts;

class Bootstrap implements \gcm\forms\Layout {
    public function renderControl(\gcm\forms\Control $control) {
        $out = [];

        if ($control instanceof \gcm\forms\controls\StaticText) {
            $control->addClass("form-control-static");
        } elseif (!($control instanceof \gcm\forms\controls\Checkbox)) {
            $control->addClass("form-control");
        }

        if ($control instanceof \gcm\forms\controls\Checkbox) {
            $out[] = "\t<div class=\"checkbox\">";
            $out[] = "\t\t<label>";
            $out[] = "\t\t\t".$control->renderControl();
            $out[] = "\t\t\t".$control->getLabel();
            $out[] = "\t\t</label>";
            $out[] = "\t</div>";
        } elseif ($control instanceof \gcm\forms\controls\Hidden) {
            $out[] = "\t".$control->renderControl();
        } else {
            $out[] = "\t<div class=\"form-group".(($control->hasErrors())?" has-error":"").(($control->isRequired())?" required":"")."\">";
            $out[] = "\t\t".$control->renderLabel();
            $out[] = "\t\t".$control->renderControl();

            if ($control->getComment()) {
                $out[] = "\t\t<p class=\"help-block\">".$control->getComment()."</p>";
            }

            foreach ($control->getErrors() as $error) {
                $out[] = "\t\t<p class=\"help-block\">".$error."</p>\n";
            }

            $out[] = "\t</div>";
        }

        if ($control->getJsFuncName()) {
            $out[] = $control->getJsCode();
        }

        $control->setRendered(true);

        return implode("\n", $out);
    }

    public function render(\gcm\forms\Form $form) {
        $out = [];

        $enctype = "";
        foreach ($form->getControls() as $control) {
            if ($control instanceof \gcm\forms\controls\File) {
                $enctype = " enctype=\"multipart/form-data\"";
                break;
            }
        }

        $out[] = "<form action=\"".$form->getAction()."\" method=\"post\" class=\"form\"".$enctype.">";

        $anyRequiredField = false;

        foreach ($form->getControls() as $control) {
            if ($control instanceof \gcm\forms\controls\Button) {
                continue;
            }

            $out[] = $this->renderControl($control);
            $anyRequiredField |= $control->isRequired();
        }

        if ($anyRequiredField) {
            $out[] = "\t<div class=\"form-group\">";
            $out[] = "\t\t<p class=\"help-text required\"><span class=\"required-mark\">*</span> ".$form->getI18N()->getLocalizedMessage("Required field")."</p>";
            $out[] = "\t</div>";
        }

        $out[] = "\t<div class=\"form-group\">";

        $hasSubmitButton = false;
        foreach ($form->getControls() as $control) {
            if (!$control instanceof \gcm\forms\controls\Button) {
                continue;
            }


            if ($control->getType() == \gcm\forms\controls\Button::TYPE_SUBMIT) {
                $out[] = $control->renderControl(["class" => "btn btn-primary"]);
                $hasSubmitButton = true;
            } else {
                $out[] = $control->renderControl(["class" => "btn"]);
            }
        }

        if (!$hasSubmitButton) {
            $out[] = "\t\t<button type=\"submit\" class=\"btn btn-primary\">".$form->getI18N()->getLocalizedMessage("Submit")."</button>";
        }

        $out[] = "\t</div>";

        $out[] = "</form>";

        return implode("\n", $out);
    }
}
