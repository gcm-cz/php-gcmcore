<?php

namespace gcm\forms\layouts;

use \gcm\forms\Layout;
use \gcm\forms\Form;
use \gcm\forms\Control;
use \gcm\forms\controls\Button;

class MaterialDesign implements Layout {
    public function renderControl(Control $control) {
        $out = [];

        $clz = explode("\\", get_class($control));
        $clz = strtolower(preg_replace("/([A-Z])/", "-\$1", array_pop($clz)));

        $out[] = "\t<div class=\"form-control".(($control->hasErrors())?" has-error":"").(($control->isRequired())?" required":"")." form-control".$clz."\">";
        $out[] = "\t\t".$control->renderControl(["placeholder" => " "]);
        $out[] = "\t\t".$control->renderLabel();
        $out[] = "\t</div>";

        if ($control->getComment()) {
            $out[] = "\t\t<p class=\"help-block\">".$control->getComment()."</p>";
        }

        foreach ($control->getErrors() as $error) {
            $out[] = "\t\t<p class=\"help-block error\">".$error."</p>";
        }

        return implode("\n", $out);
    }

    public function render(Form $form) {
        $out = [
            "<form action=\"".$form->getAction()."\" method=\"post\">",
        ];

        foreach ($form->getControls() as $control) {
            if ($control instanceof Button) {
                continue;
            }

            $out[] = $this->renderControl($control);
        }

        $out[] = "\t<div class=\"text-right\">";

        $hasSubmitButton = false;
        foreach ($form->getControls() as $control) {
            if (!$control instanceof Button) {
                continue;
            }

            $out[] = $control->renderControl();
            if ($control->getType() == \gcm\forms\controls\Button::TYPE_SUBMIT) {
                $hasSubmitButton = true;
            }
        }

        if (!$hasSubmitButton) {
            $out[] = "\t\t<button type=\"submit\">".$form->getI18N()->getLocalizedMessage("Submit")."</button>";
        }

        $out[] = "\t</div>";
        $out[] = "</form>";

        return implode("\n", $out);
    }
}
