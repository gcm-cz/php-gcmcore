<?php

namespace gcm\forms\layouts;

class ColumnarBootstrap extends Bootstrap {
    protected $columnspec = NULL;
    protected $placed_controls = [];

    /**
     * @param array $columnspec Column specification. It is array where each entry represents one column. It's key
     *   is bootstrap column class and value is list of control names that should be included in that column.
     *   If value is NULL, all controls that does not fit to another column are placed here.
     *   There must be always exactly one NULL column.
     */
    public function __construct(array $columnspec=["col-sm-12" => NULL]) {
        $has_null_column = false;

        foreach ($columnspec as $key=>$val) {
            if (is_null($val)) {
                if (!$has_null_column) {
                    $has_null_column = true;
                } else {
                    throw new \InvalidArgumentException("In \$columnspec, there must be exactly one NULL column.");
                }
            } else {
                if (!is_array($val)) {
                    throw new \InvalidArgumentException("In \$columnspec, array value can be either array or NULL. Got ".gettype($val));
                }

                foreach ($val as $control) {
                    if (in_array($control, $this->placed_controls)) {
                        throw new \InvalidArgumentException("In \$columnspec, each control can be present only once.");
                    }

                    $this->placed_controls[] = $control;
                }
            }
        }

        if (!$has_null_column) {
            throw new \InvalidArgumentException("In \$columnspec, there must be exactly one NULL column.");
        }

        $this->columnspec = $columnspec;
    }

    public function render(\gcm\forms\Form $form) {
        $out = [];

        $enctype = "";
        foreach ($form->getControls() as $control) {
            if ($control instanceof \gcm\forms\controls\File) {
                $enctype = " enctype=\"multipart/form-data\"";
                break;
            }
        }

        $out[] = "<form action=\"".$form->getAction()."\" method=\"post\" class=\"form\"".$enctype.">";

        $anyRequiredField = false;

        $out[] = "\t<div class=\"row\">";
        foreach ($this->columnspec as $class => $controls) {
            $out[] = "\t\t<div class=\"".htmlspecialchars($class)."\">";
            foreach ($form->getControls() as $control) {
                if ($control instanceof \gcm\forms\controls\Button) {
                    continue;
                }

                if ((is_null($controls) && !in_array($control->getName(), $this->placed_controls)) || (!is_null($controls) && in_array($control->getName(), $controls))) {
                    $out[] = $this->renderControl($control);
                    $anyRequiredField |= $control->isRequired();
                }
            }
            $out[] = "\t\t</div>";
        }
        $out[] = "\t</div>";

        if ($anyRequiredField) {
            $out[] = "\t<div class=\"form-group\">";
            $out[] = "\t\t<p class=\"help-text required\"><span class=\"required-mark\">*</span> ".$form->getI18N()->getLocalizedMessage("Required field")."</p>";
            $out[] = "\t</div>";
        }

        $out[] = "\t<div class=\"form-group\">";

        $hasSubmitButton = false;
        foreach ($form->getControls() as $control) {
            if (!$control instanceof \gcm\forms\controls\Button) {
                continue;
            }

            $out[] = $control->renderControl(["class" => "btn btn-primary"]);
            if ($control->getType() == \gcm\forms\controls\Button::TYPE_SUBMIT) {
                $hasSubmitButton = true;
            }
        }

        if (!$hasSubmitButton) {
            $out[] = "\t\t<button type=\"submit\" class=\"btn btn-primary\">".$form->getI18N()->getLocalizedMessage("Submit")."</button>";
        }

        $out[] = "\t</div>";

        $out[] = "</form>";

        return implode("\n", $out);
    }
}