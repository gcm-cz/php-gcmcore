<?php

namespace gcm\forms;

use \gcm\util\Session;

class SessionPersistence implements Persistence {
    protected $requestParamName;

    public function __construct($requestParamName) {
        $this->requestParamName = $requestParamName;
    }

    public function store(Form $form) {
        $id = \md5(\spl_object_hash($form));
        Session::set($id, \json_encode($form->serialize()));
        return $id;
    }

    public function load(Form $form) {
        if (isset($_REQUEST[$this->requestParamName])) {
            $form->deserialize(\json_decode(Session::get($_REQUEST[$this->requestParamName]) ?? NULL));
        }
    }
}
