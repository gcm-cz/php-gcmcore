<?php

namespace gcm\forms;

interface Layout {
    public function renderControl(\gcm\forms\Control $control);
    public function render(\gcm\forms\Form $form);
}
