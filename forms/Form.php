<?php

namespace gcm\forms;

class Form {
    protected $controls = [];
    protected $layout;
    protected $persistence = NULL;
    protected $i18n = NULL;
    protected $action;

    public function __construct(Layout $layout=NULL) {
        $this->action = $_SERVER['REQUEST_URI'];

        if (is_null($layout)) {
            $this->layout = new layouts\Bootstrap();
        } else {
            $this->layout = $layout;
        }

        // By default, do not translate anything.
        $this->i18n = \gcm\i18n\L10N::getInstance()->getDomain("forms");
    }

    public function getLayout() {
        return $this->layout;
    }

    public function getAction() {
        $action = $this->action;

        if ($this->action != $_SERVER["REQUEST_URI"]) {
            if (strpos($action, "?") !== false) {
                $action .= "&";
            } else {
                $action .= "?";
            }

            $action .= "referer=".$_SERVER["REQUEST_URI"];
        }

        return $action;
    }

    public function setAction(string $action) {
        $this->action = $action;
        return $this;
    }

    public function setI18N(\gcm\i18n\Domain $i18n) {
        $this->i18n = $i18n;

        foreach ($this->controls as $control) {
            $control->setI18N($this->i18n);
        }
        return $this;
    }

    public function getI18N() {
        return $this->i18n;
    }

    public function add(Control $control) {
        $this->controls[$control->getName()] = $control;
        $control->setI18N($this->i18n);
        return $this;
    }

    public function get($name) {
        if (isset($this->controls[$name])) {
            return $this->controls[$name];
        } else {
            trigger_error("Unknown form control `".$name."`. Form contains only ".implode(", ", array_keys($this->controls)).".", E_USER_NOTICE);
            return NULL;
        }
    }

    public function hasControl($name) {
        return isset($this->controls[$name]);
    }

    public function getControls() {
        return $this->controls;
    }

    public function validate() {
        $valid = true;

        foreach ($this->controls as $control) {
            $value = $control->getValue();
            $valid &= $control->validate($value);
        }

        return $valid;
    }

    public function isPosted() {
        return $_SERVER["REQUEST_METHOD"] == "POST";
    }

    public function setPersistence(Persistence $persistence) {
        $this->persistence = $persistence;
        return $this;
    }

    public function getPersistence() {
        return $this->persistence;
    }

    public function __toString() {
        $this->load();
        return $this->layout->render($this);
    }

    public function store() {
        if (!is_null($this->persistence)) {
            return $this->persistence->store($this);
        }
        return NULL;
    }

    public function load() {
        if (!is_null($this->persistence)) {
            $this->persistence->load($this);
        }
        return $this;
    }

    public function serialize() {
        return [
            "controls" => array_map(function($control) {
                return $control->serialize();
            }, $this->controls),
        ];
    }

    public function deserialize($object) {
        if (is_object($object) && isset($object->controls)) {
            foreach ($object->controls as $name => $control) {
                if (isset($this->controls[$name])) {
                    $this->controls[$name]->deserialize($control);
                }
            }
        }
        return $this;
    }

    public function populate($values) {
        foreach ($this->controls as $name=>$control) {
            if (empty($name)) continue;

            $control->setValue($control->getValue($values));
        }

        return $this;
    }

    public function hasErrors(): bool {
        foreach ($this->controls as $control) {
            if ($control->hasErrors()) {
                return true;
            }
        }

        return false;
    }
}
