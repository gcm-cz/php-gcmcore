<?php

namespace gcm\forms;

use gcm\controllers\TemplatedController;

use gcm\config\Config;

class Stylesheet extends TemplatedController {
    public function material() {
        header("Content-Type: text/css");
        return $this->renderTemplate("@core/forms/material.css", [
            "basecolor" => Config::get("materialform", "basecolor", "#1976d2"),
            "bgcolor" => Config::get("materialform", "bgcolor", "#f5f5f5"),
        ]);
    }
}
