<?php

namespace gcm\forms;

interface Persistence {
    public function store(Form $form);
    public function load(Form $form);
}