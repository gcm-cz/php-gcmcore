<?php

namespace gcm\forms\controls;

class StaticText extends \gcm\forms\Control {
    public function renderControl(array $attributes=[]) {
        $attributes["class"] = $this->combineClasses($attributes["class"] ?? "");
        return $this->renderHtml("p", true, htmlspecialchars($this->getValue()), $attributes);
    }
}