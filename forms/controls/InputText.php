<?php

namespace gcm\forms\controls;

class InputText extends Input {
    public function __construct($name, $label="", $value="") {
        parent::__construct($name, $label, $value, "text");
    }
}
