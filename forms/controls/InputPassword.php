<?php

namespace gcm\forms\controls;

class InputPassword extends Input {
    public function __construct($name, $label="", $value="") {
        parent::__construct($name, $label, $value, "password");
    }
}
