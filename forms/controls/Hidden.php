<?php

namespace gcm\forms\controls;

class Hidden extends Input {
    public function __construct($name, $value) {
        parent::__construct($name, "", $value, "hidden");
    }
}