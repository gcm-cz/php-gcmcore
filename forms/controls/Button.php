<?php

namespace gcm\forms\controls;

class Button extends \gcm\forms\Control {
    const TYPE_SUBMIT = "submit";
    const TYPE_RESET = "reset";
    const TYPE_BUTTON = "button";

    protected $type;

    public function __construct($name, $label="", $value="", $type=self::TYPE_BUTTON) {
        parent::__construct($name, $label, $value);
        $this->type = $type;
    }

    public function renderControl(array $attributes=[]) {
        $attributes["name"] = $this->getName();
        $attributes["type"] = $this->type;
        $attributes["value"] = $this->getValue();
        $attributes["id"] = $this->getId();
        $attributes["class"] = $this->combineClasses($attributes["class"] ?? "");

        return $this->renderHtml("button", true, $this->getLabel(), $attributes);
    }

    public function getType() {
        return $this->type;
    }
}
