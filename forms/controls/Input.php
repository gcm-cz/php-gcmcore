<?php

namespace gcm\forms\controls;

abstract class Input extends \gcm\forms\Control {
    public function __construct($name, $label="", $value="", $type) {
        parent::__construct($name, $label, $value);

        $this->type = $type;
    }

    public function renderControl(array $attributes=[]) {
        $attributes["name"] = $this->getName();
        $attributes["type"] = $this->type;
        $attributes["value"] = $this->getValue();
        $attributes["id"] = $this->getId();
        $attributes["class"] = $this->combineClasses($attributes["class"] ?? "");

        if ($this->getJsFuncName()) {
            $attributes["onchange"] = $this->getJsFuncName()."(this)";
        }

        return $this->renderHtml("input", false, NULL, $attributes);
    }
}
