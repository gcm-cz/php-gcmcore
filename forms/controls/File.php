<?php

namespace gcm\forms\controls;

class File extends Input {
    public function __construct($name, $label="", $value="") {
        parent::__construct($name, $label, $value, "file");
    }

    protected function testRequired($value) {
        return is_uploaded_file($_FILES[$this->getName()]["tmp_name"]);
    }
}