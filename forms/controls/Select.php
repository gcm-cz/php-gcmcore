<?php

namespace gcm\forms\controls;

class Select extends \gcm\forms\Control {
    protected $options = [];

    public function __construct($name, $label="", $value="") {
        parent::__construct($name, $label, $value);

        // Validate that submitted value for the select matches one of the options.
        $this->addValidator([$this, "select_validator"]);
    }

    protected function select_validator($value, \gcm\forms\Control $control) {
        foreach ($this->options as $option) {
            if ((is_array($value) && in_array($option["value"], $value)) || (!is_array($value) && $option["value"] == $value)) {
                return true;
            }
        }

        $this->addError($this->i18n->getLocalizedMessage("Invalid value."));
        return false;
    }

    public function setValue($value) {
        if (substr($this->name, -2) == "[]") {
            if (!is_array($value)) {
                $value = [$value];
            }

            $out = [];

            foreach ($this->options as $option) {
                foreach ($value as $val) {
                    if ($option["value"] == $val) {
                        $out[] = $val;
                    }
                }
            }

            $this->value = $out;
        } else {
            foreach ($this->options as $option) {
                if ($option["value"] == $value) {
                    $this->value = $value;
                }
            }
        }
        return $this;
    }

    public function addOption($value, $label=NULL) {
        if (is_null($label)) {
            $label = $value;
        }

        $this->options[] = ["label" => $label, "value" => $value];
        return $this;
    }

    public function addOptions($options) {
        if (array_values($options) === $options) {
            foreach ($options as $val) {
                $this->options[] = ["label" => $val, "value" => $val];
            }
        } else {
            foreach ($options as $key=>$val) {
                $this->options[] = ["label" => $val, "value" => $key];
            }
        }
        return $this;
    }

    public function renderControl(array $attributes=[]) {
        $attributes["name"] = $this->getName();
        $attributes["id"] = $this->getId();
        $attributes["class"] = $this->combineClasses($attributes["class"] ?? "");

        if ($this->getJsFuncName()) {
            $attributes["onchange"] = $this->getJsFuncName()."(this)";
        }

        $content = [];

        foreach ($this->options as $option) {
            $attrs = [
                "value" => $option["value"]
            ];

            if ($option["value"] == $this->getValue()) {
                $attrs["selected"] = true;
            }

            $content[] = $this->renderHtml("option", true, htmlspecialchars($option["label"]), $attrs);
        }

        return $this->renderHtml("select", true, implode("\n", $content), $attributes);
    }
}