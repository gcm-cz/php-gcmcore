<?php

namespace gcm\forms\controls;

class Checkbox extends \gcm\forms\Control {
    public function __construct($name, $label="", bool $value=false) {
        parent::__construct($name, $label, $value);
    }

    public function getValue(&$values=NULL) {
        return (bool)($this->getPostedValue($values) ?? $this->getDefaultValue());
    }

    public function renderControl(array $attributes=[]) {
        $attributes["type"] = "checkbox";
        $attributes["name"] = $this->getName();
        $attributes["id"] = $this->getId();
        $attributes["class"] = $this->combineClasses($attributes["class"] ?? "");
        $attributes["value"] = "1";

        if ($this->getJsFuncName()) {
            $attributes["onchange"] = $this->getJsFuncName()."(this)";
        }

        if ($this->getValue()) {
            $attributes["checked"] = true;
        }

        return $this->renderHtml("input", false, "", ["type" => "hidden", "name" => $this->getName(), "value" => "0"]).$this->renderHtml("input", false, "", $attributes);
    }
}