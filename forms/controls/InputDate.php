<?php

namespace gcm\forms\controls;

class InputDate extends Input {
    public function __construct($name, $label="", $value="") {
        parent::__construct($name, $label, $value, "date");
        $this->regex('/^([0-9]{4}-[0-9]{2}-[0-9]{2})?$/', function(){
            return $this->form->i18n->getLocalizedMessage("Must contain valid date.");
        });
    }

    protected function convertValue($value) {
        if (empty($value)) return null;
        return \DateTime::createFromFormat("Y-m-d", $value);
    }
}
