<?php

namespace gcm\forms\controls;

class RadioList extends Select {
    const OTHER_OPTION_VALUE = "__other_option__";

    protected $allow_other = false;
    protected $other_title = NULL;
    protected $other_control = NULL;

    protected function select_validator($value, \gcm\forms\Control $control) {
        if ($this->allow_other) {
            if (!parent::select_validator($value, $control)) {
                $this->errors = [];
                $value = $this->other_control->getValue();
                $result = $this->other_control->validate($value);
                if (!$result) {
                    foreach ($this->other_control->getErrors() as $error) {
                        $this->addError($error);
                    }
                }
                return $result;
            } else {
                return true;
            }
        } else {
            return parent::select_validator($value, $control);
        }
    }

    public function setValue($value) {
        if ($this->allow_other) {
            $this->value = $value;
        } else {
            parent::setValue($value);
        }

        return $this;
    }

    public function getPostedValue(&$values=NULL) {
        $value = parent::getPostedValue($values);

        if ($this->allow_other) {
            if ($value == self::OTHER_OPTION_VALUE) {
                return $this->other_control->getPostedValue($values) ?? NULL;
            } else {
                return $value;
            }
        } else {
            return $value;
        }
    }

    public function renderControl(array $attributes=[]) {
        $out = [];

        $attributes["class"] = $this->combineClasses(($attributes["class"] ?? "")." checkbox");

        $first = true;
        $is_other = true;

        foreach ($this->options as $option) {
            $input_attributes = [
                "name" => $this->getName(),
                "type" => "radio",
                "value" => $option["value"],
            ];

            if ($option["value"] == $this->getValue()) {
                $input_attributes["checked"] = true;
                $is_other = false;
            } else {
                $input_attributes["checked"] = false;
            }

            if ($first) {
                $input_attributes["id"] = $this->getId();
                $first = false;
            }

            if ($this->getJsFuncName()) {
                $input_attributes["onclick"] = $this->getJsFuncName()."(this)";
            }

            $out[] = $this->renderHtml("div", true,
                $this->renderHtml(
                    "label",
                    true,
                    $this->renderHtml("input", false, "", $input_attributes)." ".$option["label"]
                ),
                $attributes);
        }

        if ($this->allow_other) {
            $input_attributes = [
                "type" => "radio",
                "name" => $this->getName(),
                "value" => self::OTHER_OPTION_VALUE,
            ];

            if ($first) {
                $input_attributes["id"] = $this->getId();
                $first = false;
            }

            if ($this->getJsFuncName()) {
                $input_attributes["onclick"] = $this->getJsFuncName()."(this)";
            }

            if ($is_other) {
                $input_attributes["checked"] = true;
                $this->other_control->setValue($this->getValue());
            }

            $out[] = $this->renderHtml("div", true,
                $this->renderHtml(
                    "label",
                    true,
                    $this->renderHtml("input", false, "", $input_attributes)
                    ." ".$this->other_control->getLabel()." "
                    .$this->other_control->renderControl()
                ),
                $attributes
            );
        }

        return implode("\n", $out);
    }

    public function getJsValueSelector() {
        return "(function(){
            var options = document.getElementById('".$this->getId()."').form[\"".addcslashes($this->getName(), "\"\r\n\t")."\"];
            for (var i = 0; i < options.length; ++i) {
                if (options[i].checked) return options[i];
            }
        })()";
    }

    public function allowOther(\gcm\forms\Control $other_control) {
        $this->allow_other = true;
        $this->other_control = $other_control;

        $other_control->name = "_other_".$this->getName();

        return $this;
    }
}