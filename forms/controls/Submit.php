<?php

namespace gcm\forms\controls;

class Submit extends Button {
    public function __construct($name, $label="", $value="") {
        parent::__construct($name, $label, $value, Button::TYPE_SUBMIT);
    }
}
