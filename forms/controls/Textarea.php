<?php

namespace gcm\forms\controls;

class Textarea extends \gcm\forms\Control {
    protected $rows = NULL;
    protected $cols = NULL;

    public function rows($rows) {
        $this->rows = $rows;
        return $this;
    }

    public function cols($cols) {
        $this->cols = $cols;
        return $this;
    }

    public function renderControl(array $attributes=[]) {
        if (!is_null($this->rows)) {
            $attributes["rows"] = $this->rows;
        }

        if (!is_null($this->cols)) {
            $attributes["cols"] = $this->cols;
        }

        $attributes["name"] = $this->getName();
        $attributes["id"] = $this->getId();
        $attributes["class"] = $this->combineClasses($attributes["class"] ?? "");

        if ($this->getJsFuncName()) {
            $attributes["onchange"] = $this->getJsFuncName()."(this)";
        }

        return $this->renderHtml("textarea", true, htmlspecialchars($this->getValue()), $attributes);
    }
}
