<?php

namespace gcm\forms\controls;

class CheckboxList extends Select {
    public function renderControl(array $attributes=[]) {
        $out = [];

        $attributes["class"] = $this->combineClasses(($attributes["class"] ?? "")." checkbox");

        foreach ($this->options as $option) {
            $input_attributes = [
                "name" => $this->getName(),
                "type" => "checkbox",
                "value" => $option["value"],
                "checked" => in_array($option["value"], (array)$this->getValue())
            ];

            if ($this->getJsFuncName()) {
                $input_attributes["onchange"] = $this->getJsFuncName()."(this)";
            }

            $out[] = $this->renderHtml("div", true,
                $this->renderHtml(
                    "label",
                    true,
                    $this->renderHtml("input", false, "", $input_attributes)." ".$option["label"]
                ),
                $attributes);
        }

        return implode("\n", $out);
    }

    protected function testRequired($value) {
        return is_array($value) && !empty($value);
    }
}
