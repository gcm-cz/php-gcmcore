<?php

namespace gcm\forms;

abstract class Control {
    protected $name;
    protected $label = "";
    protected $value = "";

    protected $validators = [];
    protected $errors = [];

    protected $required = false;

    protected $comment;
    protected $classes = [];

    protected $i18n;

    protected $script;
    protected $script_name;

    protected $rendered = false;

    const EMAIL_REGEX = '/(?:[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/';

    public function __construct($name, $label="", $value="") {
        $this->name = $name;
        $this->label = $label;
        $this->value = $value;
    }

    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    public function addValidator(callable $callback) {
        $this->validators[] = $callback;
        return $this;
    }

    public function minLength($minLength) {
        return $this->addValidator(function($value, Control $control) use ($minLength) {
            if (strlen($value) < $minLength) {
                $this->addError(sprintf($this->i18n->getLocalizedMessage("Must be at least %d characters long."), $minLength));
                return false;
            }
            return true;
        });
    }

    public function maxLength($maxLength) {
        return $this->addValidator(function($value, Control $control) use ($maxLength) {
            if (strlen($value) > $maxLength) {
                $this->addError(sprintf($this->i18n->getLocalizedMessage("Cannot be longer than %d characters."), $maxLength));
                return false;
            }
            return true;
        });
    }

    public function exactLength($exactLength) {
        return $this->addValidator(function($value, Control $control) use ($exactLength) {
            if (strlen($value) != $exactLength) {
                $this->addError(sprintf($this->i18n->getLocalizedMessage("Must be exactly %d characters long."), $exactLength));
                return false;
            }
            return true;
        });
    }

    public function greaterThan($reference) {
        return $this->addValidator(function($value, Control $control) use ($reference) {
            if ($value <= $reference) {
                $this->addError(sprintf($this->i18n->getLocalizedMessage("Must be greater than %d."), $reference));
                return false;
            }
            return true;
        });
    }

    public function lessThan($reference) {
        return $this->addValidator(function($value, Control $control) use ($reference) {
            if ($value >= $reference) {
                $this->addError(sprintf($this->i18n->getLocalizedMessage("Must be lower than %d."), $reference));
                return false;
            }
            return true;
        });
    }

    /**
     * Perform regexp validation on the control's value. Optional error message can be specified.
     * @param string $regex PCRE compatibile regular expression that will be used to validate the value.
     * @param string|callable $error Error message to display when regexp text fails. If $error is string,
     *    the message is used as is. If $error is callable, that callable is called and is expected to return
     *    string value of the error message. This is particullary handy when using localization, because
     *    when the control is being created, there is no $this->i18n initialized. So you cannot use it to translate
     *    the messages. When you specify the callback, the message will be evaluated only when input validation
     *    fails, but in that case, $this->i18n is already initalized, so localized error message can be returned.
     */
    public function regex(string $regex, $error=NULL) {
        return $this->addValidator(function($value, Control $control) use ($regex, $error) {
            if (!preg_match($regex, $value)) {
                if (is_callable($error)) {
                    $this->addError($error());
                } elseif (!empty($error)) {
                    $this->addError($error);
                } else {
                    $this->addError($this->i18n->getLocalizedMessage("Invalid value."));
                }
                return false;
            }
            return true;
        });
    }

    public function email() {
        return $this->regex(self::EMAIL_REGEX, function(){
            return $this->i18n->getLocalizedMessage("Must contain valid email address.");
        });
    }

    public function alpha() {
        return $this->regex('/^[[:alpha:]]$/', function(){
            return $this->i18n->getLocalizedMessage("Must contain only letters.");
        });
    }

    public function alpha_numeric() {
        return $this->regex('/^[[:alnum:]]*$/', function(){
            return $this->i18n->getLocalizedMessage("Must contain only letters and numbers.");
        });
    }

    public function identifier() {
        return $this->regex('/^[[:alpha:]][[:alnum:]-_]*$/', function(){
            return $this->i18n->getLocalizedMessage("Must be valid identifier.");
        });
    }

    public function numeric() {
        return $this->addValidator(function($value, Control $control) {
            if (!is_numeric($value)) {
                $this->addError($this->i18n->getLocalizedMessage("Must be number."));
                return false;
            }
            return true;
        });
    }

    public function integer() {
        return $this->addValidator(function($value, Control $control) {
            if (!preg_match('/^[+-]?[0-9]+$/', $value)) {
                $this->addError($this->i18n->getLocalizedMessage("Must be integer."));
                return false;
            }
            return true;
        });
    }

    public function decimal() {
        return $this->addValidator(function($value, Control $control) {
            if (!preg_match('/^[+-]?[0-9]*(\.[0-9]+)?$/', $value)) {
                $this->addError($this->i18n->getLocalizedMessage("Must be decimal number."));
                return false;
            }
            return true;
        });
    }

    public function required($required = true) {
        $this->required = $required;
        return $this;
    }

    public function isRequired() {
        return $this->required;
    }

    protected function testRequired($value) {
        if (is_null($value) || strlen($value) == 0) {
            return false;
        }

        return true;
    }

    public function validate($value) {
        $out = true;

        if ($this->isRequired()) {
            if (!$this->testRequired($value)) {
                $this->addError($this->i18n->getLocalizedMessage("Required."));
                return false;
            }
        }

        if ($this->testRequired($value)) {
            foreach ($this->validators as $validator) {
                $out &= $validator($value, $this);
            }
        }

        return $out;
    }

    public function getName() {
        return $this->name;
    }

    public function getLabel() {
        return $this->label;
    }

    public function setLabel(string $label) {
        $this->label = $label;
        return $this;
    }

    public function getDefaultValue() {
        return $this->value;
    }

    protected function getValues(&$values=NULL) {
        if (is_null($values)) {
            return $_POST;
        } else {
            return $values;
        }
    }

    public function getPostedValue(&$values=NULL) {
        $values = $this->getValues($values);

        // It is array.
        if (preg_match('/^([^\\]\\[]+?)(\\[([^\\]\\[]+?)\\])+(\\[\\])?$/', $this->getName(), $matches)) {
            $field_name = $matches[1];
            if (preg_match_all('/\\[([^\\]\\[]+)\\]/', $this->getName(), $matches)) {
                if (is_array($values)) {
                    $value = $values[$field_name] ?? NULL;
                } elseif (is_object($values)) {
                    $value = $values->{$field_name} ?? NULL;
                } else {
                    $value = NULL;
                }

                foreach ($matches[1] as $item) {
                    if ($item == "") {
                        return (array)$value;
                    }

                    if (is_array($value)) {
                        $value = $value[$item] ?? NULL;
                    } elseif (is_object($value)) {
                        $value = $value->{$item} ?? NULL;
                    } else {
                        $value = NULL;
                    }
                }
                return $value;
            } else {
                return NULL;
            }
        } elseif (preg_match('/^([^\\]\\[]+?)(\\[\\])$/', $this->getName(), $matches)) {
            if (is_array($values)) {
                return $values[$matches[1]] ?? NULL;
            } elseif (is_object($values)) {
                return $values->{$matches[1]} ?? NULL;
            } else {
                return NULL;
            }
        } else {
            if (is_array($values)) {
                return $values[$this->getName()] ?? NULL;
            } elseif (is_object($values)) {
                return $values->{$this->getName()} ?? NULL;
            } else {
                return NULL;
            }
        }
    }

    /**
     * This method gets called internally to process value from POST and optionally convert
     * it to internal representation.
     * By default, this method does nothing, but you can override it with your custom code.
     */
    protected function convertValue($value) {
        return $value;
    }

    public function getValue(&$values=NULL) {
        $value = $this->convertValue($this->getPostedValue($values) ?? $this->getDefaultValue());
        return $value;
    }

    public function setComment($comment) {
        $this->comment = $comment;
        return $this;
    }

    public function getComment() {
        return $this->comment;
    }

    public function addClass($class) {
        if (!in_array($class, $this->classes)) {
            $this->classes[] = $class;
        }
        return $this;
    }

    public function combineClasses($additional) {
        $out = $this->classes;
        foreach (explode(" ", $additional) as $class) {
            if (!empty($class) && !in_array($class, $out)) {
                $out[] = $class;
            }
        }
        return implode(" ", $out);
    }

    public function addError($error) {
        $this->errors[] = $error;
        return $this;
    }

    public function hasErrors() {
        return !empty($this->errors);
    }

    public function getErrors() {
        return $this->errors;
    }

    protected function renderHtml($tag, $pair=false, $content="", $attributes=[]) {
        $out = "<".$tag;

        foreach ($attributes as $key=>$val) {
            if (is_bool($val)) {
                if ($val) {
                    $out .= " ".$key;
                }
            } else {
                $out .= " ".$key."=\"".htmlspecialchars($val)."\"";
            }
        }

        if ($pair) {
            $out .= ">".$content."</".$tag.">";
        } else {
            $out .= "/>";
        }

        return $out;
    }

    public function getId() {
        return "input_".$this->getName();
    }

    public function renderLabel() {
        return $this->renderHtml("label", true, $this->getLabel(), ["for" => $this->getId()]);
    }

    public abstract function renderControl(array $attributes=[]);

    public function serialize() {
        // Handle arrays correctly.
        $name = $this->name;
        if (substr($name, -2) == "[]") {
            $name = substr($name, 0, -2);
        }

        return [
            "value" => $_POST[$name] ?? NULL,
            "errors" => $this->errors
        ];
    }

    public function deserialize($object) {
        $this->errors = $object->errors ?? [];
        $_POST[$this->getName()] = $object->value ?? NULL;
    }

    public function setI18N(\gcm\i18n\Domain $i18n) {
        $this->i18n = $i18n;
    }

    public function getI18N() {
        return $this->i18n;
    }

    /**
     * Sets live validation JavaScript code for this option. This code will be called when value of control changes
     * and there will be predefined variable 'value' that holds new value of the control.
     */
    public function setLiveValidation($script) {
        if (!empty($script)) {
            $this->script = $script;
            $this->script_name = "form_onchange_".uniqid();
        } else {
            $this->script = NULL;
            $this->script_name = NULL;
        }
        return $this;
    }

    /**
     * Return JavaScript code to inject into the form that holds the validation callback.
     */
    public function getJsCode() {
        if (!empty($this->script)) {
            return "<script>function ".$this->getJsFuncName()."(control){".$this->script."} $(function(){ var control = ".$this->getJsValueSelector()."; if (control) { ".$this->getJsFuncName()."(control); }});</script>";
        } else {
            return NULL;
        }
    }

    public function getJsValueSelector() {
        return "document.getElementById('".$this->getId()."')";
    }

    /**
     * Return name of JavaScript function with validation code for this control.
     */
    public function getJsFuncName() {
        return $this->script_name;
    }

    public function setRendered(bool $rendered) {
        $this->rendered = $rendered;
        return $this;
    }

    public function isRendered() {
        return $this->rendered;
    }
}
