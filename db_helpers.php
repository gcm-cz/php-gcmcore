<?php

use \gcm\db\mysql\Connection;
use \gcm\config\Config;
use \gcm\db\Transaction;

/**
 * Return MySQL database connection instance.
 */
function get_db_conn() {
    static $db = NULL;
    if (is_null($db)) {
        $db = new Connection(
            Config::get("mysql", "host", "localhost"),
            Config::get("mysql", "user", "root"),
            Config::get("mysql", "password", ""),
            Config::get("mysql", "database", "kiss-cms")
        );
    }

    return $db;
}

/**
 * Return new MySQL transaction.
 */
function transaction($isolation_level=Transaction::REPEATABLE_READ) {
    $db = get_db_conn();
    return $db($isolation_level);
}

/**
 * Injects given callback into currently opened transaction, or start a new temporary one if there is no
 * active transaction given.
 *
 * This method is needed for non-deterministic calling of event handlers. The event handler might need DB, and
 * the dispatcher of event does not need to provide it's transaction to the handlers. So we need a way how to obtain
 * link to current transaction or provide a new one for the code to complete. And this is way how to do it.
 *
 * @param callable $callback Callback accepting one argument of type \gcm\db\Transaction, which will be called to
 *     do the DB stuff.
 * @return Return value from the callback.
 */
function inject_transaction(callable $callback) {
    $db = get_db_conn();

    $transaction = NULL;
    $transaction_opened = false;

    if (!$db->in_transaction()) {
        $transaction = $db();
        $transaction_opened = true;
    } else {
        $transaction = $db->current_transaction();
    }

    try {
        return $callback($transaction);
    } catch (\Throwable $t) {
        if (!$transaction->is_commited() && $transaction_opened) {
            $transaction->rollback();
        }

        throw $t;
    } finally {
        if (!$transaction->is_commited() && $transaction_opened) {
            $transaction->commit();
        }
    }
}