<?php

namespace gcm;

use \gcm\util\Message;
use \gcm\util\Dispatcher;

use \gcm\config\Config;
use \gcm\config\APCUCache;

require_once __DIR__.DIRECTORY_SEPARATOR."db_helpers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."hr_helpers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."i18n_helpers.php";

if (is_dir(__DIR__.DIRECTORY_SEPARATOR."vendor")) {
    require_once __DIR__.DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";
} else {
    trigger_error(E_USER_ERROR, __DIR__.DIRECTORY_SEPARATOR."vendor does not exists. Have you forgotten to run composer install?");
}

Dispatcher::bind(Dispatcher::EV_TWIG_INIT, function(\Twig\Environment $env){
    $env->addFunction(new \Twig\TwigFunction("config", function($section, $name){
        return \gcm\config\Config::get($section, $name);
    }));

    $env->addFunction(new \Twig\TwigFunction("messages", function(){
        return Message::flashed();
    }));

    //$env->addGlobal("current_user", \gcm\kisscms\LoginManager::getCurrentUser());
    $env->addFunction(new \Twig\TwigFunction("current_user", function(){
        return \gcm\kisscms\LoginManager::getCurrentUser();
    }));

    if ($env->getLoader() instanceof \Twig\Loader\FilesystemLoader) {
        $env->getLoader()->addPath(__DIR__."/templ", "core");
    }
});

// English is default built-in language. It is always available.
Dispatcher::bind(i18n\I18N::EV_ENUM_LOCALES, function(){ return new i18n\I18N("en_US", "English (United States)"); });

if (php_sapi_name() != "cli") {
    $dbg = new \gcm\util\Debugger();
    $dbg->install();
}

if (!file_exists(BASE_PATH.DIRECTORY_SEPARATOR."app".DIRECTORY_SEPARATOR."config.php")) {
    /*if (is_dir(__DIR__."/app") && is_writable(__DIR__."/app")) {
        // TODO: Place installer here.
    } else {*/
        throw new RuntimeException("Configuration not found. Please, create file app/config.php");
    //}
}

require BASE_PATH.DIRECTORY_SEPARATOR."app".DIRECTORY_SEPARATOR."/config.php";

try {
    if (!Config::getInstance(APCUCache::create(
        Config::get("config", "cache_key", APCUCache::DEFAULT_CACHE_KEY),
        Config::get("config", "cache_ttl_seconds", APCUCache::DEFAULT_TTL_SECONDS)
    ))->fromCache()) {
        $db = transaction();
        try {
            Config::getInstance()->fromDb($db);
        } finally {
            $db->commit();
        }
    }
} catch (\gcm\db\exceptions\ConnectError $e) {
}

\date_default_timezone_set(\gcm\config\Config::get("core", "timezone", "Europe/Prague"));

Dispatcher::bind(\gcm\util\CLI::EV_REGISTER_COMMAND, function($cli) {
    $cli->registerCommand(new \gcm\util\cli\corecommands\HelpCommand());
    $cli->registerCommand(new \gcm\util\cli\corecommands\QuitCommand());
    $cli->registerCommand(new \gcm\util\cli\corecommands\MigrateCommand());
    $cli->registerCommand(new \gcm\util\cli\corecommands\ModuleCommands());
    $cli->registerCommand(new \gcm\util\cli\corecommands\UserCommands());
    $cli->registerCommand(new \gcm\util\cli\corecommands\RoleCommands());
});

header_register_callback(function() {
    $tm = $GLOBALS["tm"];

    $tm_db = 0;
    $transactions = 0;
    $query = 0;

    $qs = \gcm\db\mysql\Connection::$statistics;
    if (!is_null($qs)) {
        $tm_db = $qs->time_sum;

        $transactions = count($qs);

        foreach ($qs as $trans) {
            $query += count($trans);
        }
    }

    header("Server-Timing: db;dur=".($tm_db * 1000).", ttfb;dur=".($tm->fromStart() * 1000));
    header("X-DB-Stats: trans=".$transactions." query=".$query);
});
