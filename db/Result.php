<?php

namespace gcm\db;

interface Result {
    public function data_seek($offset);
    public function fetch_assoc();
    public function fetch_row();
    public function fetch_object(string $class_name="\\stdClass", array $params=NULL);
    public function fetch_all_assoc(callable $callback=NULL);
    public function fetch_all_row(callable $callback=NULL);
    public function fetch_all_object(string $class_name="\\stdClass", callable $callback=NULL, array $params=NULL);
}
