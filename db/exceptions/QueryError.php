<?php

namespace gcm\db\exceptions;

class QueryError extends Error {
    protected $query;

    public function __construct(string $message, int $code=0, string $query="") {
        if (!empty($query)) {
            parent::__construct($message."\nQuery: ".$query, $code);
        } else {
            parent::__construct($message, $code);
        }

        $this->query = $query;
    }
}
