<?php

namespace gcm\db\exceptions;

class Error extends \Exception {
    public function __construct(string $message, int $code=0) {
        parent::__construct($message, $code);
    }
}
