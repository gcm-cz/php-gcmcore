<?php

namespace gcm\db\mysql;

class QueryStatistics implements \Iterator, \Countable {
    protected $log = [];
    protected $parent = NULL;

    public $commited = false;
    public $time_sum = 0;

    protected $it_index = 0;

    public function log(string $query, float $time, bool $success, int $affected_rows=NULL, int $result_rows=NULL) {
        $stat = new \stdClass();
        $stat->query = $query;
        $stat->time = $time;
        $stat->success = $success;
        $stat->affected_rows = $affected_rows;
        $stat->result_rows = $result_rows;

        $p = $this;
        while (!is_null($p)) {
            $p->time_sum += $time;
            $p = $p->parent;
        }

        $this->log[] = $stat;
    }

    public function transaction() {
        $qs = new QueryStatistics();
        $qs->parent = $this;
        $this->log[] = $qs;
        return $qs;
    }

    public function current() {
        return $this->log[$this->it_index];
    }

    public function key() {
        return $this->it_index;
    }

    public function next() {
        $this->it_index++;
    }

    public function rewind() {
        $this->it_index = 0;
    }

    public function valid() {
        return isset($this->log[$this->it_index]);
    }

    public function count() {
        return count($this->log);
    }
}
