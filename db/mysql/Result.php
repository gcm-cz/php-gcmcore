<?php

namespace gcm\db\mysql;

use \gcm\db\exceptions\ArgumentError;
use \gcm\db\exceptions\LogicError;

/**
 * Class representing result of the query.
 */
class Result implements \gcm\db\Result {
    protected $link;
    protected $result;
    protected $query;

    protected $fetched = false;
    protected $result_fetched = NULL;

    public function __construct(\mysqli $link, callable $result_fetched, string $query) {
        $this->link = $link;
        $this->result_fetched = $result_fetched;
        $this->query = $query;
    }

    public function __toString() {
        return $this->query;
    }

    /**
     * Ensure result is available by either store() or use(). If not, use store() to retrieve the result, because
     * it is default for MySQL.
     */
    protected function ensure() {
        if (!$this->fetched) {
            $this->store();
        }
    }

    /**
     * Store result for processing. Use this to buffer whole result in memory.
     */
    public function store() {
        if ($this->fetched) {
            throw new LogicError("Multiple call to Result::store() or Result::use().", 0, $this->query);
        }

        $this->result = $this->link->store_result();
        $this->fetched = true;

        if (!is_null($this->result_fetched)) {
            call_user_func($this->result_fetched, $this);
        }

        if ($this->result === false && $this->link->errno != 0) {
            throw QueryErrorFactory::factory($this->link->error, $this->link->errno, $this->query);
        }
    }

    /**
     * Use result for processing. Call this if you don't want to buffer whole result set in the memory.
     */
    public function use() {
        if ($this->fetched) {
            throw new LogicError("Multiple call to Result::store() or Result::use().", 0, $this->query);
        }

        $this->result = $this->link->use_result();
        $this->fetched = true;

        if (!is_null($this->result_fetched)) {
            call_user_func($this->result_fetched, $this);
        }

        if ($this->result === false && $this->link->errno != 0) {
            throw QueryError::factory($this->link->error, $this->link->errno, $query);
        }
    }

    /**
     * Return number of rows this result contains.
     */
    public function num_rows(): int {
        $this->ensure();
        return $this->result->num_rows ?? 0;
    }

    /**
     * Seek the result object.
     * @param offset Offset where to seek.
     */
    public function data_seek($offset) {
        $this->ensure();
        $this->result->data_seek($offset);
    }

    /**
     * Fetch row from result as associative array.
     * @return Array with fields or NULL if there are no more rows available.
     */
    public function fetch_assoc() {
        $this->ensure();
        return $this->result->fetch_assoc();
    }

    /**
     * Fetch row from result as array.
     * @return Array with fields or NULL if there are no more rows available.
     */
    public function fetch_row() {
        $this->ensure();
        return $this->result->fetch_row();
    }

    /**
     * Fetch row from result as object.
     * @param string $class_name Class that should be used to represent the row.
     * @param array $params Optional params to pass to the object constructor.
     */
    public function fetch_object(string $class_name="\\stdClass", array $params=NULL) {
        //if (!class_exists($class_name)) {
        //    throw new ArgumentError("Class ".$class_name." does not exists.", 0);
        //}

        $this->ensure();

        if (!is_null($params)) {
            return $this->result->fetch_object($class_name, $params);
        } else {
            return $this->result->fetch_object($class_name);
        }
    }

    /**
     * Fetch all rows from result into array as associative array.
     * @param callable $callback Can be used to modify each row before it is stored in the result array.
     */
    public function fetch_all_assoc(callable $callback=NULL) {
        $this->ensure();

        $out = [];
        while ($a = $this->result->fetch_assoc()) {
            if (!is_null($callback)) {
                $callback($a);
            }

            $out[] = $a;
        }
        return $out;
    }

    /**
     * Fetch all rows from result into array as numeric array.
     * @param callable $callback Can be used to modify each row before it is stored in the result array.
     */
    public function fetch_all_row(callable $callback=NULL) {
        $this->ensure();

        $out = [];
        while ($a = $this->result->fetch_row()) {
            if (!is_null($callback)) {
                $callback($a);
            }

            $out[] = $a;
        }
        return $out;
    }

    /**
     * Fetch all rows from result into array as object.
     * @param string $class_name Class that should be used to represent the row.
     * @param callable $callback Can be used to modify each row before it is stored in the result array.
     * @param array $params Optional array of arguments to pass to the object constructor.
     */
    public function fetch_all_object(string $class_name="\\stdClass", callable $callback=NULL, array $params=NULL) {
        $this->ensure();

        $out = [];
        while ($a = $this->fetch_object($class_name, $params)) {
            if (!is_null($callback)) {
                $callback($a);
            }

            $out[] = $a;
        }
        return $out;
    }

    /**
     * Destructor.
     */
    public function __destruct() {
        $this->ensure();

        if (is_object($this->result)) {
            $this->result->free();
        }
    }
}
