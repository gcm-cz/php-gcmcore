<?php

namespace gcm\db\mysql;

use \gcm\db\exceptions\ConnectError;
use \gcm\db\exceptions\LogicError;

class Connection {
    protected $link;
    protected $transaction_guard;

    public static $statistics;

    /**
     * Connects to the database.
     * @param string $host Hostname of the MySQL server.
     * @param string $user Username to use when connecting.
     * @param string $password Password to use when connecting.
     * @param string $db Database to open.
     */
    public function __construct($host, $user, $password, $db="", $charset="utf8mb4") {
        if (is_null(self::$statistics)) {
            self::$statistics = new QueryStatistics();
        }

        $retries = 10;
        while ($retries > 0) {
            try {
                $this->link = @new \mysqli($host, $user, $password, $db);

                if ($this->link->connect_error) {
                    throw new ConnectError($this->link->connect_error, $this->link->connect_errno);
                }
                break;
            } catch (Throwable $t) {
                --$retries;
                if ($retries == 1) {
                    throw $e;
                } else {
                    // wait 0.1s before trying again.
                    usleep(100000);
                }
            }
        }

        $this->link->autocommit(false);
        $this->link->query("SET NAMES ".$charset);
        $this->transaction_guard = new TransactionGuard();
    }

    /**
     * Create new transaction object, that can perform queries.
     * @param string $isolation_level Transaction isolation level. See Transaction for valid values.
     * @throws LogicError if there is uncommited transaction open for this connection.
     */
    public function __invoke($isolation_level=Transaction::REPEATABLE_READ) {
        if ($this->transaction_guard->in_transaction) {
            throw new LogicError("Transaction already open.", 0, "BEGIN");
        }

        return new TransactionWrapper(new Transaction($this->link, $this->transaction_guard));
    }

    /**
     * Return true if transaction is currently opened.
     * @return true if transaction is opened, false otherwise.
     */
    public function in_transaction() {
        return $this->transaction_guard->in_transaction;
    }

    /**
     * Return current transaction, if any.
     * @return Current Transaction instance or NULL if there is no active transaction.
     */
    public function current_transaction() {
        return $this->transaction_guard->current_transaction;
    }
}
