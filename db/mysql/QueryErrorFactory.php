<?php

namespace gcm\db\mysql;

use gcm\db\exceptions\QueryError;
use gcm\db\exceptions\DuplicateEntry;

class QueryErrorFactory {
    private function __construct() {}

    public static function factory($message, $code, $query) {
        switch ($code) {
            case 1062:
                return new DuplicateEntry($message, $code, $query);

            default:
                return new QueryError($message, $code, $query);
        }
    }
}
