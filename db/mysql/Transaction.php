<?php

namespace gcm\db\mysql;

use \gcm\db\exceptions\ArgumentError;
use \gcm\db\exceptions\LogicError;
use \gcm\db\exceptions\QueryError;
use \gcm\db\exceptions\TooManyRowsError;
use \gcm\db\exceptions\EntityNotFound;

class Transaction implements \gcm\db\Transaction {
    protected $link;
    protected $transaction_guard;
    protected $commited = false;
    protected $healthy = true;

    protected $stats;

    /**
     * Constructor. Do not use directly, just invoke instance of the MySQL connection class.
     */
    public function __construct(\mysqli $link, TransactionGuard $transaction_guard, string $isolation_level=self::REPEATABLE_READ) {
        $this->link = $link;
        $this->transaction_guard = $transaction_guard;

        $this->stats = Connection::$statistics->transaction();

        switch ($isolation_level) {
            case self::REPEATABLE_READ:
            case self::READ_COMMITED:
            case self::READ_UNCOMMITED:
            case self::SERIALIZABLE:
                $this->query("SET TRANSACTION ISOLATION LEVEL ".$isolation_level);
                break;

            default:
                throw new ArgumentError("Unknown transaction isolation level.", 0);
        }

        $this->query("BEGIN");
        $this->transaction_guard->in_transaction = true;
        $this->transaction_guard->current_transaction = $this;
    }

    /**
     * Destructor. Rollbacks the transaction when the transaction is not commited and emits warning in that case.
     */
    public function __destruct() {
        //if ($this->transaction_guard->in_transaction && $this->transaction_guard->current_transaction == $this) {
        if (!$this->commited) {
            trigger_error("Rolling back uncommited transaction.", E_USER_WARNING);
            $this->rollback();
        }
    }

    /**
     * Return true if transaction has already been commited.
     */
    public function is_commited() {
        return $this->commited;
    }

    /**
     * Replace placeholders (?[num]) with arguments in query.
     * @param string $query Query to be executed. The query can contain placeholders (?[num]) to replace for autoescaped
     *   value from $args. Optional index of argument can be specified, 1-based.
     * @param mixed ...$args List of arguments to be inserted into query instead placeholders.
     */
    protected function replace_args(string $query, ...$args) {
        preg_match_all('/\\?([0-9]*)/', $query, $match, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
        $offset = 0;
        $auto_args_index = 0;

        foreach ($match as $arg) {
            $args_index = $auto_args_index;

            // Optional parameter index.
            if (!empty($arg[1][0])) {
                // Index in placeholder is 1-based. Create 0-based by subtracting 1.
                $args_index = (int)$arg[1][0] - 1;
            }

            if (!array_key_exists($args_index, $args)) {
                throw new \InvalidArgumentException("Not all arguments replaced during query formatting.");
            }

            $arg_offset = $arg[0][1] + $offset;

            $value = $this->escape($args[$args_index]);

            // Place value instead of placeholder.
            $query = substr($query, 0, $arg_offset).$value.substr($query, $arg_offset + strlen($arg[0][0]));

            // Modify offsets captured by preg_match_all to match after value has been replaced.
            $offset += strlen($value) - strlen($arg[0][0]);

            if (empty($arg[1][0])) {
                ++$auto_args_index;
            }
        }

        return $query;
    }

    /**
     * Execute query on the server.
     * @param string $query Query to be executed. The query can contain placeholders (?[num]) to replace for autoescaped
     *   value from $args. Optional index of argument can be specified, 1-based.
     * @param mixed ...$args List of arguments to be inserted into query instead placeholders.
     * @return Instance of Result class if query returns result, or true.
     * @throws LogicError when transaction has already been commited or rollbacked.
     * @throws QueryError or subclass of QueryError instance depending on nature of the error.
     */
    public function query(string $query, ...$args) {
        if ($this->commited) {
            throw new LogicError("Cannot perform query on commited transaction.", 0, $query);
        }

        if (!$this->healthy) {
            throw new LogicError("You must first store() or use() result of previous query.", 0, $query);
        }

        if (!empty($args)) {
            $query = $this->replace_args($query, ...$args);
        }

        $tm_start = microtime(true);
        $res = $this->link->real_query($query);
        $tm_duration = microtime(true) - $tm_start;

        if ($res === false) {
            $this->stats->log($query, $tm_duration, false, NULL, NULL);
            throw QueryErrorFactory::factory($this->link->error, $this->link->errno, $query);
        }

        if ($this->link->field_count) {
            //return new Result($this->link, $query);
            $this->healthy = false;
            return new Result($this->link, function($res) use ($query, $tm_duration) {
                $this->healthy = true;
                $this->stats->log($query, $tm_duration, true, 0, $res->num_rows() ?? NULL);
            }, $query);
        } else {
            $res = $this->link->store_result();
            if (is_object($res)) {
                $res->free();
            }

            $this->stats->log($query, $tm_duration, true, $this->link->affected_rows, NULL);
            return $res;
        }
    }

    /**
     * Return last value of AUTO_INCREMENT column after INSERT query.
     * @return int Last inserted ID.
     */
    public function last_insert_id() {
        if ($this->commited) {
            throw new LogicError("Cannot perform operation on commited transaction.", 0, $query);
        }

        return $this->link->insert_id;
    }

    /**
     * Return number of affected rows for DML.
     * @return int Number of affected rows.
     */
    public function affected_rows() {
        if ($this->commited) {
            throw new LogicError("Cannot perform operation on commited transaction.", 0, $query);
        }

        return $this->link->affected_rows;
    }

    /**
     * Prepare value for the query. Quote and escape strings, format dates, handle NULL values.
     * @param mixed $data Data to be escaped.
     */
    public function escape($data) {
        if (is_null($data)) {
            return "NULL";
        } elseif (is_string($data)) {
            return "'".$this->link->real_escape_string($data)."'";
        } elseif (is_numeric($data)) {
            return $data;
        } elseif (is_object($data)) {
            if ($data instanceof \DateTime) {
                return "'".$this->link->real_escape_string($data->format(self::DB_DATE_FORMAT))."'";
            } else {
                return "'".$this->link->real_escape_string((string)$data)."'";
            }
        } elseif (is_bool($data)) {
            return (($data)?"1":"0");
        } elseif (is_array($data)) {
            return implode(", ", array_map(array($this, "escape"), $data));
        } else {
            throw new ArgumentError("Cannot serialize value of type ".gettype($data)." into query.", 0);
        }
    }

    /**
     * Heal connection in case of commit/rollback - eat all results if necessary to perform clean
     * transaction shutdown.
     */
    private function heal() {
        if ($this->healthy) {
            return;
        }

        // Cleanup the connection there...
        $res = $this->link->store_result();
        $res->free();

        while ($this->link->more_results()) {
            $this->link->next_result();
            $result = $this->link->store_result();
            $result->free();
        }

        $this->healthy = true;
    }

    /**
     * Commit the transaction.
     * @throws LogicError when transaction has already been commited or rollbacked.
     */
    public function commit() {
        if ($this->commited) {
            throw new LogicError("Cannot perform operation on commited transaction.", 0, "COMMIT");
        }

        $this->heal();

        try {
            $this->query("COMMIT");
            $this->stats->commited = true;
            $this->transaction_guard->in_transaction = false;
            $this->transaction_guard->current_transaction = NULL;
        } finally {
            $this->commited = true;
        }
    }

    /**
     * Rollback the transaction.
     * @throws LogicError when transaction has already been commited or rollbacked.
     */
    public function rollback() {
        if ($this->commited) {
            throw new LogicError("Cannot perform operation on commited transaction.", 0, "ROLLBACK");
        }

        $this->heal();

        try {
            $this->query("ROLLBACK");
            $this->transaction_guard->in_transaction = false;
            $this->transaction_guard->current_transaction = NULL;
        } finally {
            $this->commited = true;
        }
    }

    /**
     * Execute query and if it returns exactly one row, return it as object. Otherwise, exception is thrown.
     * @param string $query Query to be executed
     * @param string $class_name Class that should be used to represent the result.
     * @param array $params Optional array of params to pass to the constructor of the class.
     * @return Object of class $class_name with fields set to data loaded from the database.
     * @throws EntityNotFound if query did not returned a result.
     * @throws TooManyRowsError when query returned more than one row.
     */
    public function get_object(string $query, array $query_args=[], string $class_name="\\stdClass", array $params=NULL) {
        return $this->_get($query, $query_args, function($q) use ($class_name, &$params) {
            return $q->fetch_object($class_name, $params);
        });
    }

    /**
     * Execute query and if it returns exactly one row, return it as associative array. Otherwise, exception is thrown.
     * @param string $query Query to be executed
     * @return Associative array with fields set to data loaded from the database.
     * @throws EntityNotFound if query did not returned a result.
     * @throws TooManyRowsError when query returned more than one row.
     */
    public function get_assoc(string $query, array $query_args=[]) {
        return $this->_get($query, $query_args, function($q) {
            return $q->fetch_assoc();
        });
    }

    /**
     * Execute query and if it returns exactly one row, return it as array. Otherwise, exception is thrown.
     * @param string $query Query to be executed
     * @return Array with values loaded from the database.
     * @throws EntityNotFound if query did not returned a result.
     * @throws TooManyRowsError when query returned more than one row.
     */
    public function get_row(string $query, array $query_args=[]) {
        return $this->_get($query, $query_args, function($q) {
            return $q->fetch_row();
        });
    }

    /**
     * Execute query and if it returns exactly one row and one column, return that column. Otherwise, exception is thrown.
     * @param string $query Query to be executed
     * @return Scalar value of the first column of first row of the result.
     * @throws EntityNotFound if query did not returned a result.
     * @throws TooManyRowsError when query returned more than one row or row contains more than one column.
     */
    public function get_scalar(string $query, array $query_args=[]) {
        $row = $this->get_row($query, $query_args);

        if (empty($row) || count($row) > 1) {
            throw new TooManyRowsError("Result must contain only one column to be fetched as scalar.", 0, $query);
        }

        return $row[0];
    }

    /**
     * Helper method used in get_* methods. Given query, runs callback for result that extracts row from result
     * with requested type. Throws exceptions then there is no row or there are more rows than one.
     */
    protected function _get(string $query, array $query_args=[], callable $handle_result) {
        $q = $this->query($query, ...$query_args);
        $row = $handle_result($q);

        if (is_null($row)) {
            throw new EntityNotFound("Requested entity was not found.", 0, (string)$q);
        }

        $another = $handle_result($q);
        if (!is_null($another)) {
            throw new TooManyRowsError("Query returned more than one row.", 0, (string)$q);
        }

        return $row;
    }
}
