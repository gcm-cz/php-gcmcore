<?php

namespace gcm\db\mysql;

/**
 * By wrapping transaction into this transaction wrapper, auto rollback on destruct works even if we have
 * transaction_guard in place. MySQL's __invoke() method automatically wraps transactions into this object,
 * so it is transparent to the user.
 */
class TransactionWrapper implements \gcm\db\Transaction {
    protected $transaction;

    /**
     * Just store wrapped transaction.
     */
    public function __construct(Transaction $transaction) {
        $this->transaction = $transaction;
    }

    /**
     * In destructor, we checks whether the transaction is commited, and if not, rollbacks it and emits warning about
     * uncommited transaction.
     */
    public function __destruct() {
        if (!$this->transaction->is_commited()) {
            trigger_error("Rolling back uncommited transaction.", E_USER_WARNING);
            $this->transaction->rollback();
        }
    }

    // Just pass all methods to the actual transaction object.
    public function is_commited() { return $this->transaction->is_commited(); }
    public function query(string $query, ...$args) { return $this->transaction->query($query, ...$args); }
    public function last_insert_id() { return $this->transaction->last_insert_id(); }
    public function affected_rows() { return $this->transaction->affected_rows(); }
    public function escape($data) { return $this->transaction->escape($data); }
    public function commit() { return $this->transaction->commit(); }
    public function rollback() { return $this->transaction->rollback(); }
    public function get_object(string $query, array $query_args=[], string $class_name="\\stdClass", array $params=NULL) { return $this->transaction->get_object($query, $query_args, $class_name, $params); }
    public function get_assoc(string $query, array $query_args=[]) { return $this->transaction->get_assoc($query, $query_args); }
    public function get_row(string $query, array $query_args=[]) { return $this->transaction->get_row($query, $query_args); }
    public function get_scalar(string $query, array $query_args=[]) { return $this->transaction->get_scalar($query, $query_args); }
}
