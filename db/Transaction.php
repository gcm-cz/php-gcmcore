<?php

namespace gcm\db;

interface Transaction {
    const REPEATABLE_READ = "REPEATABLE READ";
    const READ_COMMITED = "READ_COMMITED";
    const READ_UNCOMMITED = "READ UNCOMMITED";
    const SERIALIZABLE = "SERIALIZABLE";

    const DB_DATE_FORMAT = "Y-m-d H:i:s";

    public function is_commited();
    public function query(string $query, ...$args);
    public function last_insert_id();
    public function affected_rows();
    public function escape($data);
    public function commit();
    public function rollback();
    public function get_object(string $query, array $query_args=[], string $class_name="\\stdClass", array $params=NULL);
    public function get_assoc(string $query, array $query_args=[]);
    public function get_row(string $query, array $query_args=[]);
    public function get_scalar(string $query, array $query_args=[]);
}
