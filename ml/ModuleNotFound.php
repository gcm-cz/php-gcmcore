<?php

namespace gcm\ml;

class ModuleNotFound extends LoaderException {
    public function __construct($module) {
        parent::__construct($module, "Module was not found.");
    }
}
