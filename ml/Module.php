<?php

namespace gcm\ml;

class Module {
    public $name;
    public $title;
    public $description;
    public $version;
    public $enabled = false;
    public $dependencies = [];
    public $path;
    public $error = NULL;
    public $loaded = false;

    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setVersion($version) {
        $this->version = $version;
        return $this;
    }

    public function depends($module, $minVersion=NULL, $maxVersion=NULL) {
        $dep = new ModuleDependency();
        $dep->name = $module;
        $dep->minVersion = $minVersion;
        $dep->maxVersion = $maxVersion;

        $this->dependencies[] = $dep;

        return $this;
    }

    public function create_phar() {
        $path = sys_get_temp_dir().DIRECTORY_SEPARATOR.$this->name.".tar";
        if (file_exists($path)) unlink($path);
        $phar = new \PharData($path, \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::KEY_AS_FILENAME, $this->name.".phar", \Phar::TAR);
        $phar->buildFromDirectory($this->path);

        return $path;
    }
}
