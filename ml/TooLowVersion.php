<?php

namespace gcm\ml;

class TooLowVersion extends DependencyException {
    public $dependency;
    public $dep_version;
    public $required_version;

    public function __construct($module, $dependency, $dep_version, $required_version) {
        parent::__construct($module, "Dependency problem: ".$dependency." is required in minimum version ".$required_version.", but got ".$dep_version);
        $this->dependency = $dependency;
        $this->dep_version = $dep_version;
        $this->required_version = $required_version;
    }
}
