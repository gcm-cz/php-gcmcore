<?php

namespace gcm\ml;

class ModuleDependency {
    public $name;
    public $minVersion;
    public $maxVersion;

    public function __toString() {
        $out = $this->name;
        if (!is_null($this->minVersion) || !is_null($this->maxVersion)) {
            $out .= "(";

            if (!is_null($this->minVersion)) {
                $out .= ">= ".$this->minVersion;
            }

            if (!is_null($this->minVersion) && !is_null($this->maxVersion)) {
                $out .= " ";
            }

            if (!is_null($this->maxVersion)) {
                $out .= "< ".$this->maxVersion;
            }

            $out .= ")";
        }

        return $out;
    }
}
