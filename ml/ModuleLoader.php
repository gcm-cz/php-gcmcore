<?php

namespace gcm\ml;

define(__NAMESPACE__.'\\EV_GET_MODULE_LOADER', __NAMESPACE__."::EV_GET_MODULE_LOADER");

class ModuleLoader {
    const MODULES_DIR = "modules";
    const INIT_FILE = "init.php";
    const META_FILE = "meta.php";

    const CONF_SECTION = "core";
    const CONF_DISABLED_ITEM = "disabled_modules";

    const PROTECTED_MODULE_NAMES = ["gcm"];

    public $dir;
    private $disabled_modules = [];
    private $module_meta = [];

    private $module_to_file = [];

    public function __construct(string $dir=NULL) {
        if (is_null($dir)) {
            $dir = BASE_PATH.DIRECTORY_SEPARATOR."app".DIRECTORY_SEPARATOR.self::MODULES_DIR;
        }

        $this->dir = $dir;
        $this->scan();
        $this->reload_config();
    }

    public function scan() {
        $d = dir($this->dir);
        while ($f = $d->read()) {
            if ($f[0] == ".") continue;

            $mod_name = $this->getModuleName($f);
            if (!isset($this->module_to_file[$mod_name])) {
                $this->module_to_file[$mod_name] = $d->path.DIRECTORY_SEPARATOR.$f;
            } else {
                throw new LoaderException($mod_name, "Duplicate module ".$mod_name.". It exists both in ".$this->module_to_file[$mod_name]." and in ".$d->path.DIRECTORY_SEPARATOR.$f);
            }
        }
    }

    public function reload_config() {
        if (class_exists("gcm\\config\\Config")) {
            $this->disabled_modules = (array)json_decode(\gcm\config\Config::get(self::CONF_SECTION, self::CONF_DISABLED_ITEM, "[]"));
        }
    }

    protected function getModuleName(string $name) {
        return pathinfo($name, PATHINFO_BASENAME);
    }

    public function enable(string $name) {
        if (($key = array_search($name, $this->disabled_modules)) !== false) {
            unset($this->disabled_modules[$key]);
            $this->save_settings();
        }
    }

    public function disable(string $name) {
        if (!in_array($name, $this->disabled_modules)) {
            $this->disabled_modules[] = $name;
            $this->save_settings();
        }
    }

    public function list() {
        return $this->module_meta;
    }

    private function save_settings() {
        \gcm\config\Config::set(self::CONF_SECTION, self::CONF_DISABLED_ITEM, json_encode(array_values($this->disabled_modules)), false, \gcm\config\ConfigValue::SOURCE_DB);
    }

    public function require(string $mod_name) {
        try {
            if (isset($this->module_meta[$mod_name])) {
                return $this->module_meta[$mod_name];
            } elseif (!isset($this->module_to_file[$mod_name])) {
                throw new ModuleNotFound($mod_name);
            } else {
                $meta = $this->intRequire($mod_name);
                return $meta;
            }
        } catch (LoaderException $e) {
            if (isset($this->module_meta[$mod_name])) {
                $this->module_meta[$mod_name]->error = $e;
                return NULL;
            } else {
                throw $e;
            };
        }
    }

    private function intRequire($mod_name) {
        $path = $this->module_to_file[$mod_name];
        if (!file_exists($path.DIRECTORY_SEPARATOR.self::META_FILE)) {
            throw new LoaderException($mod_name, "Module cannot be loaded, because file ".self::META_FILE." does not exists.");
        }

        $meta = require $path.DIRECTORY_SEPARATOR.self::META_FILE;
        if (!is_object($meta) || !($meta instanceof Module)) {
            throw new LoaderException($mod_name, "File ".$path.DIRECTORY_SEPARATOR.self::META_FILE." must return instance of ".Module::class." class.");
        }
        $this->module_meta[$mod_name] = $meta;

        $meta->enabled = !in_array($mod_name, $this->disabled_modules);
        $meta->path = $path;

        if ($meta->name != $mod_name) {
            throw new NameMismatch($mod_name, $meta->name);
        }

        if ($meta->enabled) {
            if ($this->satisfyDepends($meta)) {
                if (!file_exists($path.DIRECTORY_SEPARATOR.self::INIT_FILE)) {
                    throw new LoaderException($mod_name, "Module cannot be loaded, because file ".self::INIT_FILE." does not exists.");
                }

                require $meta->path.DIRECTORY_SEPARATOR.self::INIT_FILE;
                $meta->loaded = true;
            }

            return $meta;
        } else {
            throw new ModuleDisabled($meta->name);
        }
    }

    private function satisfyDepends($meta) {
        foreach ($meta->dependencies as $dependency) {
            if (!array_key_exists($dependency->name, $this->module_meta)) {
                try {
                    $dep = $this->require($dependency->name);
                } catch (ModuleNotFound $e) {
                    throw new MissingDependencyException($meta->name, $dependency->name);
                }
            } elseif (!$this->module_meta[$dependency->name]->loaded) {
                throw new MissingDependencyException($meta->name, $dependency->name);
            } else {
                $dep = $this->module_meta[$dependency->name];
            }

            if (is_null($dep)) {
                throw new MissingDependencyException($meta->name, $dependency->name);
            }

            if (!is_null($dependency->minVersion)) {
                if (\version_compare($dep->version, $dependency->minVersion) < 0) {
                    throw new TooLowVersion($meta->name, $dependency->name, $dep->version, $dependency->minVersion);
                }
            }

            if (!is_null($dependency->maxVersion)) {
                if (\version_compare($dep->version, $dependency->maxVersion) >= 0) {
                    throw new TooHighVersion($meta->name, $dependency->name, $dep->version, $dependency->maxVersion);
                }
            }
        }

        return true;
    }

    public function load() {
        foreach (array_keys($this->module_to_file) as $module) {
            $this->require($module);
        }
    }
}
