<?php

namespace gcm\ml;

class LoaderException extends \RuntimeException {
    public function __construct($module, $message) {
        parent::__construct("While loading module ".$module.": ".$message);
    }
}
