<?php

namespace gcm\ml;

class VersionMismatch extends LoaderException {
    public $file_version;
    public $module_version;

    public function __construct($module, $file_version, $module_version) {
        parent::__construct($module, "Version mismatch. File version is `".$file_version."`, but actual module version is `".$module_version."`.");
        $this->file_version = $file_version;
        $this->module_version = $module_version;
    }
}
