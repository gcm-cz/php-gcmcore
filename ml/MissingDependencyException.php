<?php

namespace gcm\ml;

class MissingDependencyException extends DependencyException {
    public $dependency;

    public function __construct($module, $dependency) {
        parent::__construct($module, "Dependency problem: ".$dependency." missing or is disabled.");
        $this->dependency = $dependency;
    }
}
