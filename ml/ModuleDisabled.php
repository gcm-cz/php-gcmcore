<?php

namespace gcm\ml;

class ModuleDisabled extends LoaderException {
    public function __construct($module) {
        parent::__construct($module, "Module is disabled.");
    }
}
