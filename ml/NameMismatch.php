<?php

namespace gcm\ml;

class NameMismatch extends LoaderException {
    public $file_name;

    public function __construct($module, $file_name) {
        parent::__construct($module, "Module file name `".$file_name."` differs from expected `".$module."`.");
    }
}
