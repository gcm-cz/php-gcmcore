<?php

namespace gcm\kisscms\exceptions;

/**
 * Thrown when user that is suspended tries to log in.
 */
class UserSuspendedException extends HRException {
}
