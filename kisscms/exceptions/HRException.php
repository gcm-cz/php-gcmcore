<?php

namespace gcm\kisscms\exceptions;

/**
 * Base class for all HR exceptions.
 */
class HRException extends \Exception {
}
