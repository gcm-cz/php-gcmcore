<?php

namespace gcm\kisscms\exceptions;

/**
 * Thrown when user tries to verify, but is already verified.
 */
class UserAlreadyVerified extends HRException {
}
