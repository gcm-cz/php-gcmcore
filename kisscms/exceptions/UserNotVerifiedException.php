<?php

namespace gcm\kisscms\exceptions;

/**
 * Thrown when user that is not verified tries to log in.
 */
class UserNotVerifiedException extends HRException {
}
