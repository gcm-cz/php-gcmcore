<?php

namespace gcm\kisscms\exceptions;

/**
 * When verifying user credentials, this exception is thrown when username and password combination does not match the
 * one stored in the database.
 */
class UserPasswordMismatchException extends HRException {
}
