<?php

namespace gcm\kisscms;

/**
 * Class representing one role in the system. Role groups several actions into one logical unit. Roles can be assigned
 * to users.
 */
class Role {
    public $id;  /**< Role ID */
    public $name;  /**< Role name */

    /**
     * Get role by ID.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param int $id Role ID.
     */
    public static function getById(\gcm\db\Transaction $db, int $id) {
        return $db->get_object("SELECT `id`, `name` FROM `roles` WHERE `id` = ?", [$id], Role::class);
    }

    /**
     * List all roles in the system, sorted by role name.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param int $limit Maximum number of roles to display.
     * @param int $offset Pagination offset.
     */
    public static function list(\gcm\db\Transaction $db, int $limit=NULL, int $offset=NULL) {
        $query = "SELECT `id`, `name` FROM `roles` ORDER BY `name` ASC";
        $args = [];

        if (!is_null($limit)) {
            $query .= " LIMIT ?";
            if (!is_null($offset)) {
                $query .= ", ?";
                $args[] = $offset;
            }
            $args[] = $limit;
        }

        $q = $db->query($query, ...$args);
        return $q->fetch_all_object(Role::class);
    }

    /**
     * Return number of roles known to the system.
     * @param \gcm\db\Transaction $db MySQL transaction.
     */
    public static function count(\gcm\db\Transaction $db) {
        return $db->get_scalar("SELECT COUNT(`id`) FROM `roles`");
    }

    /**
     * Create new role and return it's instance.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param string $name Role name
     */
    public static function create(\gcm\db\Transaction $db, $name) {
        $db->query("INSERT INTO `roles` (`name`) VALUES (?)", $name);

        $r = new Role();
        $r->id = $db->last_insert_id();
        $r->name = $name;

        return $r;
    }

    /**
     * Update role options.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param array $options List of options to update. Key is option name, value is it's value.
     *   Valid options are: `name`.
     */
    public function update(\gcm\db\Transaction $db, array $options=[]) {
        $to_update = [];
        $args = [];

        foreach ($options as $key=>$val) {
            switch ($key) {
                case "name":
                    $to_update[] = "`name` = ?";
                    $args[] = $val;
                    break;

                default:
                    throw new \InvalidArgumentException("Invalid option name `".$key."`.");
            }
        }

        if (!empty($to_update)) {
            $args[] = $this->id;
            $db->query("UPDATE `roles` SET ".implode(", ", $to_update)." WHERE `id` = ?", ...$args);
        }
    }

    /**
     * Sets which actions are contained inside this role.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param array $actions List of Action instances or action IDs which should be assigned with this role.
     */
    public function set_actions(\gcm\db\Transaction $db, array $actions=[]) {
        $existing_actions = [];

        foreach ($this->list_action_ids($db) as $id) {
            $existing_actions[$id] = false;
        }

        $to_insert = [];
        $args = [];

        foreach ($actions as $action) {
            if (is_object($action) && $action instanceof Action) {
                $action = $action->id;
            } elseif (!is_numeric($action)) {
                throw new \InvalidArgumentException("Array \$actions can contain only Action instances or ints ".
                    "of action IDs. Instead, got ".gettype($action));
            }

            if (array_key_exists($action, $existing_actions)) {
                $existing_actions[$action] = true;
            } else {
                $to_insert[] = "(?, ?)";
                $args[] = $this->id;
                $args[] = $action;
            }
        }

        if (!empty($to_insert)) {
            $db->query("INSERT INTO `role_action` (`role_id`, `action_id`) VALUES ".implode(",", $to_insert), ...$args);
        }

        $to_delete = array_filter($existing_actions, function($item) {
            return !$item;
        });

        if (!empty($to_delete)) {
            $db->query("DELETE FROM `role_action`
                WHERE `role_id` = ? AND `action_id` IN (".implode(",", array_fill(0, count($to_delete), "?")).")",
                $this->id, ...array_keys($to_delete));
        }
    }

    /**
     * List currently assigned actions to this role.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @return List of action IDs assigned to this role.
     */
    public function list_action_ids(\gcm\db\Transaction $db) {
        $q = $db->query("SELECT `action_id` FROM `role_action` WHERE `role_id` = ?", $this->id);
        $out = [];
        while ($a = $q->fetch_object()) {
            $out[] = $a->action_id;
        }
        return $out;
    }

    /**
     * Remove role.
     * @param \gcm\db\Transaction $db MySQL transaction.
     */
    public function remove(\gcm\db\Transaction $db) {
        $db->query("DELETE FROM `roles` WHERE `id` = ?", $this->id);
    }
}
