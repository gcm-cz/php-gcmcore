<?php

namespace gcm\kisscms;

/**
 * Class representing one action in the system. The action can then be used to restrict what user can do.
 */
class Action {
    const SORT_ASC = "ASC";  /**< Ascending sorting direction. */
    const SORT_DESC = "DESC";  /**< Descending sorting direction. */

    public $id;  /**< Action ID */
    public $name;  /**< Action name */
    public $description;  /**< Action description */
    public $parent_id;  /**< Parent action. If action has parent and user has this action, it also has all parent
      actions. */

    public $childs = [];  /**< When loaded as tree, contains list of all child actions. */
    public $parent = NULL;  /**< When loaded as tree, contains link to parent action, if the action is on top level. */

    /**
     * Get action by name.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param string $name Action name.
     * @return Instance of Action.
     * @throws \gcm\db\exceptions\EntityNotFound if Action does not exists.
     */
    public static function getByName(\gcm\db\Transaction $db, string $name) {
        return $db->get_object("SELECT `id`, `name`, `description`, `parent_id`
            FROM `actions`
            WHERE `name` = ?", [$name], Action::class);
    }

    /**
     * Get action by ID.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param int $id Action ID.
     * @return Instance of Action.
     * @throws \gcm\db\exceptions\EntityNotFound if Action does not exists.
     */
    public static function getById(\gcm\db\Transaction $db, int $id) {
        return $db->get_object("SELECT `id`, `name`, `description`, `parent_id`
            FROM `actions`
            WHERE `id` = ?", [$id], Action::class);
    }

    /**
     * List all actions in a flat structure, sorted by name.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param int $limit Number of actions to return.
     * @param int $offset Pagination offset.
     * @return Array of all actions.
     */
    public static function list(\gcm\db\Transaction $db, int $limit=NULL, int $offset=NULL) {
        $params = [];

        $query = "SELECT `id`, `name`, `description`, `parent_id` FROM `actions` ORDER BY `name` ASC";
        if (!is_null($limit)) {
            $query .= " LIMIT ?";
            if (!is_null($offset)) {
                $query .= ", ?";
                $params[] = $offset;
            }
            $params[] = $limit;
        }

        $q = $db->query($query, ...$params);
        return $q->fetch_all_object(Action::class);
    }

    /**
     * Return total number of known actions.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @return Number of actions.
     */
    public static function count(\gcm\db\Transaction $db) {
        return $db->get_scalar("SELECT COUNT(`id`) FROM `actions`");
    }

    /**
     * List all actions in tree structure.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @return List of top level actions, each with filled-in childs.
     */
    public static function tree(\gcm\db\Transaction $db, $ignoreSubtree=NULL) {
        $actions = self::list($db);

        $actions_by_id = [];
        foreach ($actions as $action) {
            $actions_by_id[$action->id] = $action;
        }

        if (is_object($ignoreSubtree) && $ignoreSubtree instanceof Action) {
            $ignoreSubtree = $ignoreSubtree->id;
        } elseif (!is_numeric($ignoreSubtree) && !is_null($ignoreSubtree)) {
            throw new \InvalidArgumentException("Argument \$ignoreSubtree must be instance of Action or int. ".
                "Instead, got ".gettype($ignoreSubtree));
        }

        foreach ($actions as $action) {
            if (!is_null($action->parent_id) && (is_null($ignoreSubtree) || $ignoreSubtree != $action->id)) {
                $actions_by_id[$action->parent_id]->childs[] = $action;
                $action->parent = $actions_by_id[$action->parent_id];
            }
        }

        return array_filter($actions, function($action) use ($ignoreSubtree) {
            return is_null($action->parent_id) && (is_null($ignoreSubtree) || $action->id != $ignoreSubtree);
        });
    }

    /**
     * Create new action.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param string $name Action name
     * @param string $description Optional description.
     * @param mixed $parent Parent action ID, parent action instance or NULL for top level action.
     */
    public static function create(\gcm\db\Transaction $db, string $name, string $description=NULL, $parent=NULL) {
        if (is_object($parent) && isinstance($parent, "Action")) {
            $parent_id = $parent->id;
        } elseif (is_numeric($parent)) {
            $parent_id = $parent;
        } elseif (!is_null($parent)) {
            throw new \InvalidArgumentException("\$parent must be instance of Action or int.");
        }

        $db->query("INSERT INTO `actions` (`name`, `description`, `parent_id`) VALUES (?, ?, ?)",
            $name, $description, $parent_id);

        $a = new Action();
        $a->id = $db->last_insert_id();
        $a->name = $name;
        $a->description = $description;
        $a->parent_id = $parent_id;

        return $a;
    }

    /**
     * Update action.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param array $options Options to update. Key contains option name, value contains it's value. Valid options are:
     *   `name`, `description`, `parent`. For `parent` key, the `value` can be parent action id, parent action instance
     *   or NULL.
     */
    public function update(\gcm\db\Transaction $db, array $options=[]) {
        if (empty($options)) {
            return;
        }

        $query = "UPDATE `actions` SET ";

        $to_update = [];
        $args = [];
        foreach ($options as $key=>$val) {
            switch ($key) {
                case "name":
                    $to_update[] = "`name` = ?";
                    $args[] = $val;
                    break;

                case "description":
                    $to_update[] = "`description` = ?";
                    $args[] = $val;
                    break;

                case "parent":
                    $to_update[] = "`parent_id` = ?";

                    if (is_object($val) && isinstance($val, "Action")) {
                        $args[] = $val->id;
                    } elseif (is_numeric($val)) {
                        if (empty($val)) {
                            $args[] = NULL;
                        } else {
                            $args[] = $val;
                        }
                    } elseif (!is_null($val)) {
                        throw new \InvalidArgumentException("Value of `parent` option can be instance of ".
                            "Action, int or NULL. Instead, got ".gettype($val));
                    } else {
                        $args[] = NULL;
                    }
                    break;

                default:
                    throw new \InvalidArgumentException("Invalid option name `".$key."`.");
            }
        }

        $query .= implode(", ", $to_update);
        $query .= " WHERE `id` = ?";
        $args[] = $this->id;

        $db->query($query, ...$args);
    }

    /**
     * Remove the action.
     * @param \gcm\db\Transaction $db MySQL transaction.
     */
    public function remove(\gcm\db\Transaction $db) {
        $db->query("DELETE FROM `actions` WHERE `id` = ?", $this->id);
    }
}
