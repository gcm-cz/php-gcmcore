<?php

namespace gcm\kisscms;

/**
 * Class for managing user's settings.
 *
 * Settings are lazy loaded on first use. Also, they are stored to DB automatically at the destruction of the object.
 * Settings are stored in database as JSON, so you should restrict settings usage to plain objects or other
 * JSON-serializable types.
 */
class UserSettings {
    protected $user; /**< User */
    protected $settings = []; /**< Key-value pair of setting items. */
    protected $changed = []; /**< List of changed settings that should be saved to db. */
    protected $loaded = false; /**< Indicator whether the settings were loaded. */

    /**
     * Constructor.
     * @param User $user User instance
     */
    public function __construct(User $user) {
        $this->user = $user;
    }

    /**
     * Get setting value.
     * @param string $name Setting name.
     * @return Setting value or $default if setting does not exists.
     */
    public function get(string $name, $default=NULL) {
        $this->ensure();
        return $this->settings[$name] ?? $default;
    }

    /**
     * List names of existing settings.
     * @return array of string containing names of existing settings. Value can then be individually retrieved
     *     by get() method.
     */
    public function items() {
        $this->ensure();
        return array_keys($this->settings);
    }

    /**
     * Change setting value. The settings are automatically saved on destruction of the object.
     * @param string $name Setting name.
     * @param $value Setting value. Settings are stored in database as JSON, so the value must be JSON-serializable.
     */
    public function set(string $name, $value) {
        $this->settings[$name] = $value;
        $this->changed[] = $name;
    }

    /**
     * Load settings if necessary. Called internally by get() to ensure setting are loaded before returning any
     * option.
     *
     * If get() is called after set(), the value set by set() call is not overwritten when loading settings from db.
     */
    protected function ensure() {
        if ($this->loaded) {
            return;
        }

        inject_transaction(function($db) {
            $q = $db->query("SELECT `item`, `value` FROM `user_settings` WHERE `user_id` = ?", $this->user->id);
            while ($a = $q->fetch_object()) {
                if (!isset($this->settings[$a->item])) {
                    $this->settings[$a->item] = json_decode($a->value);
                }
            }
        });

        $this->loaded = true;
    }

    /**
     * Destructor. Saves settings to the database.
     */
    public function __destruct() {
        inject_transaction(function($db) {
            $to_insert = [];
            $args = [];

            foreach (array_unique($this->changed) as $item) {
                $to_insert[] = "(?, ?, ?)";
                $args[] = $this->user->id;
                $args[] = $item;
                $args[] = json_encode($this->settings[$item]);
            }

            if (!empty($to_insert)) {
                $db->query("INSERT INTO `user_settings` (`user_id`, `item`, `value`) VALUES
                    ".implode(", ", $to_insert)." ON DUPLICATE KEY UPDATE `value` = VALUES(`value`)", ...$args);
            }
        });
    }
}
