<?php

namespace gcm\kisscms;

/**
 * Class encapsulating methods for working with currently logged user.
 */
class LoginManager {
    const PERSISTENT_TIMEOUT = 86400*365;  /**< Number of seconds of how long should the persistent session be held active. */

    public static $session_name = "user_id";  /**< Name of session variable to store current user id. Can be changed
        before first call to getCurrentUser(), login() or logout(). */

    private static $current_user = false;  /**< Storage for current user to avoid multiple queries to DB. */

    public static $update_login_date = true;

    /**
     * Set current user by user_id. This can be used if external authentication method is used, for example API HTTP header.
     * @param int $user_id User ID of user to set as logged in.
     * @param \gcm\db\Transaction $db Optionally, provide existing transaction. If no transaction is provided,
     *   new is started.
     */
    public static function setUserById(int $user_id, \gcm\db\Transaction $db=NULL) {
        $transaction_opened = false;
        if (is_null($db)) {
            $db = transaction();
            $transaction_opened = true;
        }

        try {
            self::$current_user = $db->get_object("SELECT
                `id`, `username`, `realname`, `registered_date`, `last_login`, `email`, `status_id` AS `status`
                FROM `users`
                WHERE `id` = ? AND `status_id` IN (?)", [$user_id, [User::STATUS_ACTIVE, User::STATUS_MUST_CHANGE_PASSWORD]], User::class);

            if (self::$update_login_date) {
                self::$current_user->updateLoginDate($db);
            }

            self::$current_user->loadActions($db);

            return self::$current_user;
        } catch (\gcm\db\exceptions\EntityNotFound $e) {
            self::$current_user = NULL;
            return NULL;
        } finally {
            if ($transaction_opened) {
                $db->commit();
            }
        }
    }

    /**
     * Return currently logged user or NULL if user is not logged in.
     * @param \gcm\db\Transaction $db Optionally, provide existing transaction. If no transaction is provided,
     *   new is started.
     * @return Instance of User class of currently logged user or NULL if user is not logged in.
     */
    public static function getCurrentUser(\gcm\db\Transaction $db=NULL) {
        if (self::$current_user !== false) {
            return self::$current_user;
        }

        $user_id = ((\gcm\util\Session::tryInit($db))?\gcm\util\Session::get(self::$session_name, NULL, $db):NULL);
        if (is_null($user_id)) {
            return NULL;
        }

        $res = NULL;
        if (!is_null($db)) {
            $res = self::setUserById($user_id, $db);
        } else {
            inject_transaction(function($db) use (&$res, $user_id) {
                $res = self::setUserById($user_id, $db);
            });
        }

        if (is_null($res)) {
            self::logout();
        } else {
            if (isset($_SERVER["HTTP_USER_AGENT"])) {
                \gcm\util\Session::set("UA", $_SERVER["HTTP_USER_AGENT"]);
            }

            $ip = $_SERVER["REMOTE_ADDR"];
            if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            }
            \gcm\util\Session::set("IP", $ip);
        }

        return self::$current_user;
    }

    /**
     * Login user.
     * @param string $username User name
     * @param string $password Password
     * @param \gcm\db\Transaction $db Optionally, provide existing transaction. If no transaction is provided,
     *   new is started.
     * @return true if user is logged in.
     * @throws UserPasswordMismatchException when username does not exists or password does not match.
     * @throws UserNotVerifiedException when user is not verified.
     * @throws UserSuspendedException when user is suspended.
     */
    public static function login(string $username, string $password, \gcm\db\Transaction $db=NULL) {
        $transaction_opened = false;
        if (is_null($db)) {
            $db = transaction();
            $transaction_opened = true;
        }

        try {
            $user = $db->get_object("SELECT `id`, `status_id` FROM `users`
                WHERE `username` = ? AND (
                    (LEFT(`password_hash`, 4) = 'md5\$' AND `password_hash` = CONCAT('md5$', MD5(CONCAT(`salt`, ?)))) OR
                    (LEFT(`password_hash`, 5) = 'sha1\$' AND `password_hash` = CONCAT('sha1$', SHA1(CONCAT(`salt`, ?)))) OR
                    `password_hash` = SHA2(CONCAT(`salt`, ?), 256)
                )", [$username, $password, $password, $password]);

            if ($user->status_id == User::STATUS_NEW) {
                throw new exceptions\UserNotVerifiedException();
            }

            if ($user->status_id == User::STATUS_SUSPENDED) {
                throw new exceptions\UserSuspendedException();
            }

            \gcm\util\Session::set(self::$session_name, $user->id);
            return true;
        } catch (\gcm\db\exceptions\EntityNotFound $e) {
            throw new exceptions\UserPasswordMismatchException();
        } finally {
            if ($transaction_opened) {
                $db->commit();
            }
        }
    }

    public static function setLoginPersistent(bool $persistent) {
        $user_id = \gcm\util\Session::get(self::$session_name);
        if (!is_null($user_id)) {
            \gcm\util\Session::set(self::$session_name, $user_id, ($persistent)?self::PERSISTENT_TIMEOUT:NULL);
        }
    }

    /**
     * Logout logged in user, if any.
     */
    public static function logout() {
        \gcm\util\Session::set(self::$session_name, NULL);
    }
}
