<?php

namespace gcm\kisscms;

/**
 * Class representing one user in the system.
 */
class User {
    const STATUS_NEW = 1;  /**< User that needs to be verified. */
    const STATUS_ACTIVE = 2;  /**< User than can login. */
    const STATUS_SUSPENDED = 3;  /**< User that cannot login. */
    const STATUS_MUST_CHANGE_PASSWORD = 4;  /**< User must change password. */

    public static $verify_timeout = 24*3600;  /**< Number of seconds when new user must verify it's account,
        before the registration is deleted. Can be modified at runtime, but it only affects new registrations.
        Default is 24 hours. */

    public $id;  /**< User ID */
    public $username;  /**< User name */
    public $realname;  /**< User's real name */
    public $registered_date;  /**< Date of registration */
    public $last_login;  /**< Date of last login */
    public $email;  /**< Email */
    public $status;  /**< Status (one of STATUS_* constants) */

    public $settings; /**< User's unique settings */

    protected $actions = NULL;  /**< List of actions the user has. Only action names are listed.
      Filled by calling `loadActions()`. */

    /**
     * List users sorted by name.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param int $limit Limit number of entries returned
     * @param int $offset Pagination offset.
     * @return Array of User instances.
     */
    public static function list(\gcm\db\Transaction $db, int $limit=NULL, int $offset=NULL) {
        $query = "SELECT `id`, `username`, `realname`, `registered_date`, `last_login`, `email`, `status_id`
            FROM `users`
            ORDER BY `username` ASC";

        $args = [];

        if (!is_null($limit)) {
            $query .= " LIMIT ?";
            if (!is_null($offset)) {
                $query .= ", ?";
                $args[] = $offset;
            }
            $args[] = $limit;
        }

        $q = $db->query($query, ...$args);
        return $q->fetch_all_object(User::class);
    }

    /**
     * Count users.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @return Number of users in the system.
     */
    public static function count(\gcm\db\Transaction $db) {
        return $db->get_scalar("SELECT COUNT(id) FROM `users`");
    }

    /**
     * Register new user, and return the created user.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param string $username User name
     * @param string $password Password, that will be salted and hashed before saving to DB.
     * @param string $email Email used to recover user's password and send verification links.
     * @param int $status One of STATUS_* constant. If status is STATUS_NEW, verification code
     *   will be generated and set in the resulting struct as $user->verify_code. User than has User::$verify_timeout
     *   seconds to complete the verification until it's registration is cancelled. The cancellation is automatic
     *   utilizing event scheduler in database.
     * @param string $realname Optional real name of the user.
     * @return User instance
     */
    public static function register(\gcm\db\Transaction $db, string $username, string $password, string $email,
            int $status=self::STATUS_NEW, string $realname=NULL)
    {
        $salt = self::genSalt();
        $db->query("INSERT INTO `users`
            (`username`, `salt`, `password_hash`, `email`, `registered_date`, `status_id`, `realname`)
            VALUES
            (?, ?, SHA2(CONCAT(?2, ?), 256), ?, NOW(), ?, ?)", $username, $salt, $password, $email, $status, $realname);

        $u = new User();
        $u->id = $db->last_insert_id();
        $u->username = $username;
        $u->realname = $realname;
        $u->registered_date = date("c");
        $u->last_login = NULL;
        $u->email = $email;
        $u->status = $status;

        if ($status == self::STATUS_NEW) {
            $u->verify_code = \gcm\util\Session::rand_id();
            $db->query("INSERT INTO `user_verification_codes` (`user_id`, `verify_code`, `valid_until`)
                VALUES (?, ?, DATE_ADD(NOW(), INTERVAL ? SECOND))", $u->id, $u->verify_code, self::$verify_timeout);
        }

        return $u;
    }

    /**
     * Resolve user by ID.
     * @param int $id User ID
     * @param \gcm\db\Transaction $db Optionally, provide existing transaction. If no transaction is provided,
     *   new is started.
     * @return Instance of User.
     * @throws \gcm\db\exceptions\EntityNotFound if user does not exists.
     */
    public static function getById(int $id, \gcm\db\Transaction $db=NULL) {
        $transaction_opened = false;

        if (is_null($db)) {
            $db = transaction();
            $transaction_opened = true;
        }

        try {
            return $db->get_object("SELECT
                `id`, `username`, `realname`, `registered_date`, `last_login`, `email`, `status_id` AS `status`
                FROM `users`
                WHERE `id` = ?", [$id], User::class);
        } finally {
            if ($transaction_opened) {
                $db->commit();
            }
        }
    }

    /**
     * Resolve user by name.
     * @param string $username User name
     * @param \gcm\db\Transaction $db Optionally, provide existing transaction. If no transaction is provided,
     *   new is started.
     * @return Instance of User.
     * @throws \gcm\db\exceptions\EntityNotFound if user does not exists.
     */
    public static function getByName(string $username, \gcm\db\Transaction $db=NULL) {
        $transaction_opened = false;

        if (is_null($db)) {
            $db = transaction();
            $transaction_opened = true;
        }

        try {
            return $db->get_object("SELECT
                `id`, `username`, `realname`, `registered_date`, `last_login`, `email`, `status_id` AS `status`
                FROM `users`
                WHERE `username` = ?", [$username], User::class);
        } finally {
            if ($transaction_opened) {
                $db->commit();
            }
        }
    }

    /**
     * Constructor.
     */
    public function __construct() {
        $this->settings = new UserSettings($this);
    }

    /**
     * Update user's logged in date.
     * @param \gcm\db\Transaction $db MySQL transaction.
     */
    public function updateLoginDate(\gcm\db\Transaction $db) {
        $db->query("UPDATE `users` SET `last_login` = NOW() WHERE `id` = ?", $this->id);
    }

    /**
     * Load actions of this user.
     * @param \gcm\db\Transaction $db MySQL transaction.
     */
    public function loadActions(\gcm\db\Transaction $db) {
        if (!is_null($this->actions)) {
            throw new \LogicException("Actions already loaded.");
        }

        $q = $db->query("SELECT `a`.`name` FROM `user_role` `ur`
                JOIN `role_action` `ra` ON (`ur`.`role_id` = `ra`.`role_id`)
                JOIN `actions` `a` ON (`a`.`id` = `ra`.`action_id`)
                WHERE `ur`.`user_id` = ?", $this->id);

        $this->actions = [];

        while ($a = $q->fetch_object()) {
            $this->actions[] = $a->name;
        }
    }

    /**
     * Return true if user has action with given name or false if it does not. Actions must be loaded by calling
     * `loadActions()` prior to calling this method.
     * @param string $action Action name.
     * @return true if user has given action, false otherwise.
     */
    public function has_action(string $action) {
        if (is_null($this->actions)) {
            throw new \LogicException("Actions must first be loaded by calling \$user->loadActions(\$db).");
        }

        return in_array($action, $this->actions);
    }

    /**
     * Sets which roles are assigned to the user.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param array $actions List of Action instances or action IDs which should be assigned with this role.
     */
    public function set_roles(\gcm\db\Transaction $db, array $roles=[]) {
        $existing_roles = [];

        foreach ($this->list_role_ids($db) as $id) {
            $existing_roles[$id] = false;
        }

        $to_insert = [];
        $args = [];

        foreach ($roles as $role) {
            if (is_object($role) && $role instanceof Role) {
                $role = $role->id;
            } elseif (!is_numeric($role)) {
                throw new \InvalidArgumentException("Array \$roles can contain only Role instances or ints ".
                    "of role IDs. Instead, got ".gettype($role));
            }

            if (array_key_exists($role, $existing_roles)) {
                $existing_roles[$role] = true;
            } else {
                $to_insert[] = "(?, ?)";
                $args[] = $this->id;
                $args[] = $role;
            }
        }

        if (!empty($to_insert)) {
            $db->query("INSERT INTO `user_role` (`user_id`, `role_id`) VALUES ".implode(",", $to_insert), ...$args);
        }

        $to_delete = array_filter($existing_roles, function($item) {
            return !$item;
        });

        if (!empty($to_delete)) {
            $db->query("DELETE FROM `user_role`
                WHERE `user_id` = ? AND `role_id` IN (".implode(",", array_fill(0, count($to_delete), "?")).")",
                $this->id, ...array_keys($to_delete));
        }
    }

    /**
     * List all role IDs that are assigned to the user.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @return List of role IDs assigned to this user.
     */
    public function list_role_ids(\gcm\db\Transaction $db) {
        $q = $db->query("SELECT `role_id` FROM `user_role` WHERE `user_id` = ?", $this->id);
        $out = [];
        while ($a = $q->fetch_object()) {
            $out[] = $a->role_id;
        }
        return $out;
    }

    /**
     * List all roles of this user sorted by role name.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @return List of roles assigned to this user.
     */
    public function listRoles(\gcm\db\Transaction $db) {
        $q = $db->query("SELECT `r`.`id`, `r`.`name`
            FROM `user_role` `ur`
            JOIN `roles` `r` ON (`r`.`id` = `ur`.`role_id`)
            WHERE `ur`.`user_id` = ?
            ORDER BY `r`.`name` ASC", $this->id);

        return $q->fetch_all_object(Role::class);
    }

    /**
     * Verify user account based on username and verify code.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param string $verify_code Verification code.
     * @return true when user has been verified, false if verification code is wrong.
     * @throws UserAlreadyVerified exception when user does not have STATE_NEW state.
     */
    public function verify(\gcm\db\Transaction $db, string $verify_code) {
        if ($this->status == self::STATUS_NEW) {
            try {
                $db->get_scalar("SELECT `id` FROM `user_verification_codes` WHERE `user_id` = ? AND `verify_code` = ?",
                    [$this->id, $verify_code]);

                $db->query("UPDATE `users` SET `status_id` = ? WHERE `id` = ?", self::STATUS_ACTIVE, $this->id);
                $db->query("DELETE FROM `user_verification_codes` WHERE `user_id` = ?", $this->id);

                return true;
            } catch (\gcm\db\exceptions\EntityNotFound $e) {
                return false;
            }
        } else {
            throw new exceptions\UserAlreadyVerified();
        }
    }

    /**
     * Update user's options.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param array $options Options to be updated. Key is option name, value is option value.
     *   Valid options are: `username`, `realname`, `email`, `password`.
     */
    public function update(\gcm\db\Transaction $db, array $options=[]) {
        $to_update = [];
        $args = [];

        foreach ($options as $key=>$val) {
            switch ($key) {
                case "username":
                    $to_update[] = "`username` = ?";
                    $args[] = $val;
                    break;

                case "realname":
                    $to_update[] = "`realname` = ?";
                    $args[] = $val;
                    break;

                case "email":
                    $to_update[] = "`email` = ?";
                    $args[] = $val;
                    break;

                case "password":
                    $salt = self::genSalt();

                    $to_update[] = "`salt` = ?";
                    $to_update[] = "`password_hash` = SHA2(CONCAT(?, ?), 256)";
                    $to_update[] = "`status_id` = IF(`status_id` = ?, ?, `status_id`)";

                    $args[] = $salt;
                    $args[] = $salt;
                    $args[] = $val;
                    $args[] = self::STATUS_MUST_CHANGE_PASSWORD;
                    $args[] = self::STATUS_ACTIVE;
                    break;

                case "status":
                    if (in_array($val, [self::STATUS_NEW, self::STATUS_ACTIVE, self::STATUS_SUSPENDED, self::STATUS_MUST_CHANGE_PASSWORD])) {
                        $to_update[] = "`status_id` = ?";
                        $args[] = $val;
                    } else {
                        throw new \InvalidArgumentException("Invalid status. Must be one of ".self::class."::STATUS_*.");
                    }
                    break;

                default:
                    throw new \InvalidArgumentException("Invalid option name `".$key."`.");
            }
        }

        if (!empty($to_update)) {
            $args[] = $this->id;

            $db->query("UPDATE `users` SET ".implode(", ", $to_update)." WHERE `id` = ?", ...$args);
        }
    }

    /**
     * Remove user.
     * @param \gcm\db\Transaction $db MySQL transaction.
     */
    public function remove(\gcm\db\Transaction $db) {
        $db->query("DELETE FROM `users` WHERE `id` = ?", $this->id);
    }

    /**
     * Check user's password.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param string $password Password that should be checked.
     * @return true if the password is correct, false otherwise.
     */
    public function check_password(\gcm\db\Transaction $db, string $password) {
        $q = $db->query("SELECT `id` FROM `users` WHERE `id` = ? AND `password_hash` = SHA2(CONCAT(`salt`, ?), 256)",
            $this->id, $password);
        $a = $q->fetch_object();

        if (!is_null($a)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Generate salt for setting the password.
     */
    protected static function genSalt() {
        return \gcm\util\Session::rand_id();
    }
}
