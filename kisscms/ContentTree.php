<?php

namespace gcm\kisscms;

class CtWrapper {
    protected $itemid;
    protected $callable;

    public function __construct($itemid, $callable) {
        $this->itemid = $itemid;
        $this->callable = $callable;
    }

    public function __invoke() {
        $args = [];
        $callable = $this->callable;
        if (is_array($callable) && !empty($callable) && !\is_object($callable[0])) {
            $callable[0] = new $callable[0]();
        }

        $db = transaction();
        $item = ContentTree::findById($db, $this->itemid);
        $db->commit();

        return \call_user_func($callable, $item, ...$args);
    }

    public function __toString() {
        return "ContentTree(".$this->itemid.")";
    }
}

class ContentTree extends Model {
    const EV_CT_FACTORY = __CLASS__."::EV_CT_FACTORY";

    private static $routes = [];

    public $id;
    public $type;
    public $parent_id = NULL;
    public $parent = NULL;
    public $childs = [];
    public $slug;
    public $order;

    /**
     * Register new content type.
     * @param string $type Content type as stored in DB.
     * @param string $classname Class name representing given type.
     * @param mixed $route Route used to display the content type.
     */
    public static function factory(string $type, string $classname, $route=NULL) {
        if (!class_exists($classname)) {
            throw new \InvalidArgumentException("Class ".$classname." does not exists.");
        }

        \gcm\util\Dispatcher::bind(self::EV_CT_FACTORY, function($in_type) use ($type, $classname) {
            if ($in_type == $type) {
                return $classname;
            } else {
                return NULL;
            }
        });

        if (!is_null($route)) {
            self::$routes[$type] = $route;
        }
    }

    public static function bindRouter(\gcm\fastrouter\Router $router) {
        $items = []; // Root items
        $items_by_id = [];

        $db = transaction();
        $q = $db->query("SELECT `id`, `parent_id`, `slug`, `type` FROM `content_tree` ORDER BY `parent_id` ASC, `order` ASC");
        while ($item = $q->fetch_object(__CLASS__)) {
            $items_by_id[$item->id] = $item;
        }
        $db->commit();

        // Build tree
        foreach ($items_by_id as $item) {
            if (!is_null($item->parent_id)) {
                $items_by_id[$item->id]->parent = $items_by_id[$item->parent_id];
            }
        }

        foreach ($items_by_id as $item) {
            if (isset(self::$routes[$item->type])) {
                $router->bind($item->getUrl(), new CtWrapper($item->id, self::$routes[$item->type]));
            }
        }
    }

    public function getLevel() {
        if (!is_null($this->parent)) {
            return $this->parent->getLevel() + 1;
        } else {
            return 1;
        }
    }

    /**
     * Fetch item information from db.
     * @param \gcm\db\Transaction $db MySQL transaction.
     * @param array $items Associative array of id -> item mapping of items that should be fetched.
     */
    public function fetch(\gcm\db\Transaction $db, array $items) {
    }

    public function __toString() {
        return "ContentTree node ".$this->id;
    }

    /**
     * Extends this object properties from another object.
     */
    public function extend($othertype) {
        foreach ($othertype as $key=>$val) {
            $this->$key = $val;
        }
    }

    /**
     * Find content type by ID and return it's correct instance.
     */
    public static function findById(\gcm\db\Transaction $db, int $id) {
        $obj = $db->get_object("SELECT `id`, `parent_id`, `slug`, `type`, `order` FROM `content_tree` WHERE `id` = ?", [$id], __CLASS__);
        $out = self::ct_factory($obj->type, $obj);

        $fetch_by_type = [
            $obj->type => [$out->id => $out]
        ];

        // Load all parents up to the top level.
        $item = $out;
        while (!is_null($item->parent_id)) {
            $parent_item = $db->get_object("SELECT `id`, `parent_id`, `slug`, `type` FROM `content_tree` WHERE `id` = ?", [$item->parent_id], __CLASS__);

            $parent = self::ct_factory($parent_item->type, $parent_item);

            if (!isset($fetch_by_type[$parent->type])) {
                $fetch_by_type[$parent->type] = [];
            }
            $fetch_by_type[$parent->type][$parent->id] = $parent;

            $item->parent = $parent;
            $item = $parent;
        }

        foreach ($fetch_by_type as $type => $items) {
            self::ct_fetch($db, $type, $items);
        }

        return $out;
    }

    public static function load_parents(\gcm\db\Transaction $db, array $items) {
        $items_by_id = [];
        $items_by_type = [];

        foreach ($items as $item) {
            $items_by_id[$item->id] = $item;
        }

        // Try to assign existing parents.
        $to_fetch = [];
        foreach ($items as $item) {
            if (!is_null($item->parent_id) && is_null($item->parent)) {
                if (isset($items_by_id[$item->parent_id])) {
                    $item->parent = $items_by_id[$item->parent_id];
                    $items_by_id[$item->parent_id]->childs[] = $item;
                } else {
                    $to_fetch[] = $item->parent_id;
                }
            }
        }

        while (!empty($to_fetch)) {
            $q = $db->query("SELECT `id`, `parent_id`, `slug`, `type` FROM `content_tree` WHERE `id` IN (?)", $to_fetch);

            $to_fetch = [];
            while ($parent_item = $q->fetch_object(__CLASS__)) {
                $parent = self::ct_factory($parent_item->type, $parent_item);
                $items_by_id[$parent->id] = $parent;

                if (!isset($items_by_type[$parent->type])) {
                    $items_by_type[$parent->type] = [];
                }

                $items_by_type[$parent->type][$parent->id] = $parent;

                if (!is_null($parent->parent_id)) {
                    if (isset($items_by_id[$parent->parent_id])) {
                        $parent->parent = $items_by_id[$parent->parent_id];
                        $items_by_id[$parent->parent_id]->childs[] = $parent;
                    } else {
                        $to_fetch[] = $parent->parent_id;
                    }
                }
            }
        }

        foreach ($items_by_id as $item) {
            if (!is_null($item->parent_id) && is_null($item->parent)) {
                if (isset($items_by_id[$item->parent_id])) {
                    $item->parent = $items_by_id[$item->parent_id];
                    $items_by_id[$item->parent_id]->childs[] = $item;
                } else {
                    throw new \RuntimeException("No parent fetched for item ".$item->id.". This should not happen.");
                }
            }
        }

        foreach ($items_by_type as $type => $items) {
            self::ct_fetch($db, $type, $items);
        }
    }

    public static function create_ct(\gcm\db\Transaction $db, int $parent_id=NULL, string $name, string $type, int $order=0) {
        $slug = self::url_from_title($name);
        $db->query("INSERT INTO `content_tree` (`parent_id`, `slug`, `type`, `order`) VALUES (?, ?, ?, ?)", $parent_id, $slug, $type, $order);

        $self = new self();
        $self->id = $db->last_insert_id();
        $self->type = $type;
        $self->parent_id = $parent_id;
        $self->slug = $slug;
        $self->order = $order;

        return $self;
    }

    /**
     * Resolve content-type class by type name.
     * @param string $type Content type name
     * @param array $type_override Can be used to override factory for given content types.
     */
    private static function ct_resolve(string $type, array $type_override=[]) {
        if (isset($type_override[$type])) {
            $cls = $type_override[$type];
            if (!class_exists($cls)) {
                throw new \LogicException(spritnf("Class %s does not exists. Probably missing plugin?", $cls));
            }
        } else {
            $cls = array_filter(\gcm\util\Dispatcher::dispatch(self::EV_CT_FACTORY, $type), function($item) { return !is_null($item); });
            if (count($cls) == 1) {
                $cls = reset($cls);
            } else {
                throw new \LogicException(sprintf("Don't know how to handle content of type %s. Probably missing plugin?", $type));
            }
        }

        return $cls;
    }

    /**
     * Create object by it's type.
     */
    private static function ct_factory(string $type, $dbobj, array $type_override=[]) {
        $cls = self::ct_resolve($type, $type_override);
        $obj = new $cls();

        foreach ($dbobj as $key=>$val) {
            $obj->$key = $val;
        }

        return $obj;
    }

    /**
     * Fetch additional properties of objects, grouped by type.
     */
    private static function ct_fetch(\gcm\db\Transaction $db, string $type, array $items, array $type_override=[]) {
        if (empty($items)) {
            return;
        }

        $cls = self::ct_resolve($type, $type_override);

        $obj = new $cls();
        $obj->fetch($db, $items);
    }

    public static function full_subtree(\gcm\db\Transaction $db, ContentTree $parent=NULL, array $type_override=[]): array {
        $items_by_id = [];

        $q = $db->query("SELECT `id`, `parent_id`, `slug`, `order`, `type` FROM `content_tree` ORDER BY `parent_id` ASC, `order` ASC");

        $load_by_type = [];

        while ($item = $q->fetch_object()) {
            $ct = self::ct_factory($item->type, $item, $type_override);
            $ct->parent = NULL;
            $ct->childs = [];

            if (!isset($load_by_type[$ct->type])) {
                $load_by_type[$ct->type] = [];
            }
            $load_by_type[$item->type][$ct->id] = $ct;

            $items_by_id[$ct->id] = $ct;
        }

        foreach ($load_by_type as $type => $items) {
            self::ct_fetch($db, $type, $items, $type_override);
        }

        $items = [];

        foreach ($items_by_id as $item) {
            if (is_null($item->parent_id)) {
                $items[] = $item;
            } else {
                $items_by_id[$item->parent_id]->childs[] = $item;
                $item->parent = $items_by_id[$item->parent_id];
            }
        }

        if (is_null($parent)) {
            return $items;
        } else {
            return $items_by_id[$parent->id]->childs;
        }
    }

    public function update(\gcm\db\Transaction $db, array $options) {
        $to_update = [];
        $args = [];

        foreach ($options as $key => $val) {
            switch ($key) {
                case "parent_id":
                    $to_update[] = "`parent_id` = ?";
                    if (empty($val)) {
                        $args[] = NULL;
                    } else {
                        $args[] = $val;
                    }
                    break;

                case "slug":
                    $to_update[] = "`slug` = ?";

                    // Top level item has no slug, because it binds to / at router.
                    if (is_null($this->parent_id)) {
                        $args[] = '';
                    } else {
                        $args[] = self::url_from_title($val);
                    }
                    break;

                case "order":
                    $to_update[] = "`order` = ?";
                    $args[] = $val;
                    break;

                default:
                    throw new \InvalidArgumentException("Unknown property `".$key."`.");
            }
        }

        if (!empty($to_update)) {
            $args[] = $this->id;
            $db->query("UPDATE `content_tree` SET ".implode(", ", $to_update)." WHERE `id` = ?", ...$args);
        }
    }

    /**
     * Convert title to url representation. It removes all non-alphanum characters and replaces them with dashes.
     * @param string $title Original title
     * @return string URL created from title.
     */
    protected static function url_from_title(string $title, string $separator="-"): string {
        // Get current setting of LC_CTYPE.
        $save = setlocale(LC_CTYPE, "0");

        // For iconv to properly work with TRANSLIT, we need to set locale to en_US.UTF-8.
        setlocale(LC_CTYPE, "en_US.UTF-8");

        // remove unwanted characters
        $title = preg_replace('/[^\\pL\\d]+/u', $separator, $title);

        // transliterate
        $title = iconv('UTF-8', 'ASCII//TRANSLIT', $title);

        // lowercase
        $title = strtolower($title);

        // trim
        $title = trim($title, $separator);

        // remove unwanted characters
        $title = preg_replace('/[^-\w]+/', '', $title);

        // Restore original locale
        setlocale(LC_CTYPE, $save);

        if (empty($title)) {
            return "n-a";
        }

        return $title;
    }

    /**
     * Move page up within same parent.
     * @param \gcm\db\Transaction $db MySQL transaction.
     */
    public function move_up(\gcm\db\Transaction $db) {
        $ct = $db->get_object("SELECT `id`, `parent_id`, `order` FROM `content_tree` WHERE `id` = ?", [$this->id]);

        $q = $db->query("SELECT `id`, `order`
            FROM `content_tree`
            WHERE
                `parent_id` = ?
                AND `order` < ?
            ORDER BY `order` DESC
            LIMIT 1", $ct->parent_id, $ct->order);
        if ($upper = $q->fetch_object()) {
            $db->query("UPDATE `content_tree` SET `order` = ? WHERE `id` = ?", $ct->order, $upper->id);
            $db->query("UPDATE `content_tree` SET `order` = ? WHERE `id` = ?", $upper->order, $ct->id);
        }
    }

    /**
     * Move page down within same parent.
     * @param \gcm\db\Transaction $db MySQL transaction.
     */
    public function move_down(\gcm\db\Transaction $db) {
        $ct = $db->get_object("SELECT `id`, `parent_id`, `order` FROM `content_tree` WHERE `id` = ?", [$this->id]);

        $q = $db->query("SELECT `id`, `order`
            FROM `content_tree`
            WHERE
                `parent_id` = ?
                AND `order` > ?
            ORDER BY `order` ASC
            LIMIT 1", $ct->parent_id, $ct->order);
        if ($lower = $q->fetch_object()) {
            $db->query("UPDATE `content_tree` SET `order` = ? WHERE `id` = ?", $ct->order, $lower->id);
            $db->query("UPDATE `content_tree` SET `order` = ? WHERE `id` = ?", $lower->order, $ct->id);
        }
    }

    /**
     * Return URL of page from top-level page as list of URL parts, suitable for `url_for` function.
     * @return array of string Array of URL parts.
     */
    public function getUrlArray(): array {
        return array_filter(array_map(function($item){ return $item->slug; }, $this->getParents()), function($item) { return $item != ""; });
    }

    public function getUrl(): string {
        return "/".implode("/", $this->getUrlArray());
    }

    /**
     * Return list of parents for building breadcrumb navigation to the page.
     * @return array of Page List of parents of this page, including the page itself.
     */
    public function getParents(): array {
        $parents = [];
        $ct = $this;
        while (!is_null($ct)) {
            $parents[] = $ct;
            $ct = $ct->parent;
        }

        return array_reverse($parents);
    }
}
