<?php

namespace gcm\kisscms;

/**
 * Layout class representing one layout.
 */
class Layout {
    const EV_LAYOUT = Layout::class."::EV_LAYOUT";  /**< Event triggered when layouts are evaluated. */

    protected $id;  /**< Layout ID, should correspond to name of module for uniqueness */
    protected $name;  /**< Layout name for representation. */
    protected $path;  /**< Path to templates */

    protected $on_select = NULL;  /**< Callback called when layout is selected to be used on the site. */

    /**
     * Constructor.
     * @param string $id Layout ID, should correspond to name of module for uniqueness.
     * @param string $name Layout name for representation. Can be localized.
     * @param string $path Path to templates directory.
     */
    public function __construct(string $id, string $name, string $path) {
        $this->id = $id;
        $this->name = $name;
        $this->path = $path;
    }

    /**
     * Return layout ID
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Return layout name
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Return path to templates.
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Registers on_select handler.
     */
    public function on_select(callable $callback) {
        $this->on_select = $callback;
        return $this;
    }

    /**
     * Select this layout to be used on the site.
     */
    public function select() {
        if (!is_null($this->on_select)) {
            call_user_func($this->on_select, $this);
        }
    }

    /**
     * List available layouts.
     * @return array of Layout instances of all available layouts.
     */
    public static function listLayouts(): array {
        $layouts = array_filter(\gcm\util\Dispatcher::dispatch(self::EV_LAYOUT), function($item) { return !is_null($item); });

        foreach ($layouts as $layout) {
            if (!is_object($layout) || !($layout instanceof Layout)) {
                throw new \RuntimeException(self::EV_LAYOUT." event handler must return instace of ".Layout::class." class.");
            }
        }

        return $layouts;
    }

    /**
     * Return configured layout.
     * @return Layout instance of configured layout, or if that layout is not found, return first available layout.
     */
    public static function getLayout(): Layout {
        $layouts = self::listLayouts();

        if (empty($layouts)) {
            throw new \RuntimeException("There are no layouts. Install some layout module to continue.");
        }

        foreach ($layouts as $layout) {
            if ($layout->getId() == \gcm\config\Config::get("site", "layout", NULL)) {
                return $layout;
            }
        }

        return $layouts[0];
    }
}
