<?php

namespace gcm\kisscms;

/**
 * Class implementing querying of sets of entites from MySQL.
 * It allows to specify SELECT stub, that will be modified by this class to allow set sorting and pagination. Also,
 * additional columns can be queried to avoid loading all possible columns every time. Purpose of this class is to
 * avoid code duplication such as building of columns lists, sorting and limiting. On the other hand, you have
 * full control of the SELECT itself, so you can do all sort of optimalizations on the select that general purpose
 * ORMs won't allow you to do.
 */
class Listing implements \Iterator, \ArrayAccess, \Countable {
    const ORDER_ASC = "ASC";
    const ORDER_DESC = "DESC";

    protected $db;
    protected $select_stub = "";
    protected $args;
    protected $class_name;
    protected $class_construct_params;

    protected $table = NULL;
    protected $count_rows = false;

    protected $join = [];
    protected $join_args = [];

    protected $where = [];
    protected $where_args = [];

    protected $group = [];
    protected $having = [];
    protected $having_args = [];

    protected $result = NULL;
    protected $result_valid = false;

    protected $current_obj;
    protected $current_obj_index;

    protected $columns = [];
    protected $limit = NULL;
    protected $offset = NULL;
    protected $order = [];

    protected $data = NULL;
    protected $found_rows = NULL;

    public function __construct(\gcm\db\Transaction $db, string $select_stub=NULL, array $args=[],
            string $class_name="\\stdClass", array $class_construct_params=NULL)
    {
        $this->db = $db;
        $this->select_stub = $select_stub;
        $this->args = $args;
        $this->class_name = $class_name;
        $this->class_construct_params = $class_construct_params;
    }

    protected function build_select_stub() {
        if (empty($this->select_stub)) {
            $this->select_stub = "SELECT";

            if ($this->count_rows) {
                $this->select_stub .= " SQL_CALC_FOUND_ROWS";
            }
            $this->select_stub .= " %{COLUMNS} FROM %{TABLE} %{JOIN} %{WHERE} %{GROUP BY}";
        }
    }

    public function request_count() {
        $this->count_rows = true;
    }

    public function total_count() {
        if (!$this->result_valid) {
            $this->perform();
        }

        if (is_null($this->found_rows)) {
            throw new \LogicException("request_count() must be called before total_count().");
        } else {
            return $this->found_rows;
        }
    }

    public function select(string ...$columns) {
        $this->columns = array_merge($this->columns, array_map([$this, "prepare_column"], $columns));
        return $this;
    }

    public function order(string $column, string $direction) {
        if ($this->result_valid) {
            throw new \LogicException("You cannot modify already loaded list.");
        }

        if ($direction != self::ORDER_ASC && $direction != self::ORDER_DESC) {
            throw new \InvalidArgumentException("Invalid direction value. Can be one of List::ORDER_ASC or ".
                "List::ORDER_DESC.");
        }

        $this->order[] = $column." ".$direction;

        return $this;
    }

    public function limit(int $limit, int $offset=NULL) {
        if ($this->result_valid) {
            throw new \LogicException("You cannot modify already loaded list.");
        }

        $this->limit = $limit;
        $this->offset = $offset;

        return $this;
    }

    public function prepare_column(string $column) {
        return "`".$column."`";
    }

    public function perform() {
        if ($this->result_valid) {
            throw new \LogicException("You cannot modify already loaded list.");
        }

        $this->build_select_stub();

        $query = $this->select_stub;
        if (!empty($this->columns)) {
            $query = str_replace("%{COLUMNS}", implode(", ", $this->columns), $query);
        } else {
            $query = str_replace("%{COLUMNS}", "1", $query);
        }

        $query = str_replace("%{TABLE}", $this->table, $query);

        $join = "";
        if (!empty($this->join)) {
            $join = implode(" ", array_unique($this->join));
            $this->args = array_merge($this->args, $this->join_args);
        }
        $query = str_replace("%{JOIN}", $join, $query);

        if (!empty($this->where)) {
            $where = "WHERE ".implode(" AND ", $this->where);
            $this->args = array_merge($this->args, $this->where_args);
            $query = str_replace("%{WHERE}", $where, $query);
        } else {
            $query = str_replace("%{WHERE}", "", $query);
        }

        $group = "";
        if (!empty($this->group)) {
            $group = "GROUP BY ".implode(", ", $this->group);
        }

        if (!empty($this->having)) {
            $group .= " HAVING ".implode(" AND ", $this->having);
            $this->args = array_merge($this->args, $this->having_args);
        }

        $query = str_replace("%{GROUP BY}", $group, $query);

        if (!empty($this->order)) {
            $query .= " ORDER BY ".implode(", ", $this->order);
        }

        if (!is_null($this->limit)) {
            $query .= " LIMIT ?";
            if (!is_null($this->offset)) {
                $query .= ", ?";
                $this->args[] = $this->offset;
            }
            $this->args[] = $this->limit;
        }

        $this->result = $this->db->query($query, ...$this->args);
        $this->result->store();
        $this->result_valid = true;
        $this->current_obj_index = 0;

        if (preg_match('/SELECT\\s+SQL_CALC_FOUND_ROWS\\s+/', $this->select_stub)) {
            $this->found_rows = $this->db->get_scalar("SELECT FOUND_ROWS()");
        }

        return $this;
    }

    private function ensure_data() {
        if (is_null($this->data)) {
            if (!$this->result_valid) {
                $this->perform();
            }

            assert($this->result_valid);

            $this->data = [];
            while ($item = $this->result->fetch_object($this->class_name, $this->class_construct_params)) {
                $this->data[] = $item;
            }
        }
    }

    public function current() {
        return current($this->data);
    }

    public function key() {
        return key($this->data);
    }

    public function next() {
        next($this->data);
    }

    public function rewind() {
        $this->ensure_data();
        reset($this->data);
    }

    public function valid() {
        return current($this->data) !== false;;
    }

    public function offsetExists($offset): boolean {
        $this->ensure_data();
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        $this->ensure_data();
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value) {
        throw new \LogicException("You cannot change contents of listing.");
    }

    public function offsetUnset($offset) {
        throw new \LogicException("You cannot change contents of listing.");
    }

    public function count() {
        $this->ensure_data();
        return count($this->data);
    }
}
