<?php

namespace gcm\kisscms;

class Model {
    private $__init = false;

    public function init_fields() {
        $this->__init = true;
    }

    public function __set($name, $value) {
        if (!$this->__init) {
            $this->init_fields();
            if (!$this->__init) {
                throw new \BadMethodCallException("You must call parent::init_fields() in your implementation of init_fields().");
            }
        }

        if (substr($name, 0, 1) == "_") {
            trigger_error("Unknown property ".get_class($this)."->".$name.".", E_USER_NOTICE);
        }

        if (property_exists($this, $name)) {
            if ($this->{$name} instanceof \DateTime) {
                $this->{$name} = new \DateTime($value);
            } else {
                $this->{$name} = $value;
            }
        } elseif (strpos($name, ".") !== false) {
            list($property, $key) = explode(".", $name, 2);
            if (property_exists($this, $property)) {
                $this->{$property}->$key = $value;
            } else {
                //trigger_error("Unknown property ".get_class($this)."->".$property.".", E_USER_NOTICE);
                $this->{$property}->$key = $value;
            }
        } else {
            //trigger_error("Unknown property ".get_class($this)."->".$name.".", E_USER_NOTICE);
            $this->{$name} = $value;
        }
    }

    public function __get($name) {
        if (!$this->__init) {
            $this->init_fields();
            if (!$this->__init) {
                throw new \BadMethodCallException("You must call parent::init_fields() in your implementation of init_fields().");
            }
        }

        if (substr($name, 0, 1) != "_" && property_exists($this, $name)) {
            return $this->{$name};
        } else {
            trigger_error("Unknown property ".get_class($this)."->".$name.".", E_USER_NOTICE);
            return NULL;
        }
    }

    public function __isset($name) {
        if (!$this->__init) {
            $this->init_fields();
            if (!$this->__init) {
                throw new \BadMethodCallException("You must call parent::init_fields() in your implementation of init_fields().");
            }
        }

        return substr($name, 0, 1) != "_" && property_exists($this, $name);
    }
}
